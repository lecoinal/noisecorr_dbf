# Calculs de corrélations et de double-beamforming avec des données d'acquisition sismique autour de la faille de San Jacinto (Californie du Sud).

Données : 20 jours en continu, 1108 stations sur une zone de 1km/1km (réseau très dense) autour d'une faille. Volume 1.6TB.

The data from the San Jacinto Fault zone geophone array are provided by University of California San Diego, University of Southern California, and Nodal Seismic and are available in IRIS Data Management Center Web Services, at https://doi.org/10.7914/SN/ZG_2014.

Publication associated to dataset: https://doi.org/10.1093/gji/ggv142
```
Frank Vernon, Yehuda Ben-Zion, & Dan Hollis. (2014). 
Sage Brush Flats Nodal Experiment [Data set]. 
International Federation of Digital Seismograph Networks. 
https://doi.org/10.7914/SN/ZG_2014
```

Example to get 5-min from one station:
```
> wget "http://service.iris.edu/fdsnws/dataselect/1/query?net=ZG&sta=R3010&start=2014-06-01T06:30:00.000&end=2014-06-01T06:35:00.000" -O /tmp/data.mseed
> msi -tg /tmp/data.mseed
   Source                Start sample             End sample        Gap  Hz  Samples
ZG_R3010__DPZ     2014,152,06:30:00.000000 2014,152,06:35:00.000000  ==  500 150001
Total: 1 trace(s) with 1 segment(s)
```

---

[PREPROCESS](./PREPROCESS) : Codes for processing raw data (Python, python-obspy, python-h5py)

[CORR](./CORR) : Codes for computing high frequency cross-correlations (Fortran, FFTW, HDF5)

[CORRAVG](./CORRAVG) : Tools and codes for computing monthly means and global means (C, Fortran, HDF5 and h5tools)

[DBF](./DBF) : Code for computing Double BeamForming (Fortran, HDF5, ORDERPACK lib) 

---

[Wiki projet correlation et double beamforming sur les données: San Jacinto Fault zone dense geophone array](https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto). 

NB: Il s'agit de l'ancien wiki, à l'époque ou les codes étaient hébergés sur le SVN de la forge de l'OSUG. Dorénavant les codes ont migré sur ce repository gitlab.

:warning: Toutes les données jacinto ont été migrées de /summer vers les ressources backup de mantis. Sur cet ancien wiki sanjacinto, il faut donc remplacer tous les chemins `/summer/jacinto` par `/mantis/home/lecoinal/jacinto`. Les logs de la migration (iquest) indiquent:

```
# ssh_tmux cargo
> iinit --ttl 8640

> iput -rv /summer/jacinto /mantis/home/lecoinal/.

> irepl -rv /mantis/home/lecoinal/jacinto -R backup
# replication sur ressources backup, il y aura automatiquement 2 replicas sur ces ressources backup (donc momentanément 3 réplicas, un sur ressources normales, et 2 sur ressources backup)

> itrim -rv /mantis/home/lecoinal/jacinto
# suppression du replica le plus ancien, celui sur ressources normales.
# il faut conserver 2 replicas sur les ressources backup, c'est le mode par défaut, ne pas chercher à forcer avec itrim -N 1 (déprécié)

> iquest "SELECT SUM(DATA_SIZE), COUNT(DATA_ID)  WHERE COLL_NAME LIKE '/mantis/home/lecoinal/jacinto%'"
DATA_SIZE = 17245994313138
DATA_ID = 59978
------------------------------------------------------------
> cat /summer/jacinto/0-USAGE/Usage-2025.01.17-12H00.txt
[...]
Total disk usage : 8.45 To
Total number of files : 29979

# Or 29979 * 2 = 59958 = 59978 - 20
# En effet il s'agit des 10 fichiers présents dans 0-USAGE qui ont été migrés aussi, en 2 exemplaires (ou replicas)
```

---

# Discussion

For a Discussion about Big Data Processing Challenges in the context of massive San Jacinto dataset (*PREPROCESS*, *CORR*, and *CORRAVG* procedures):

see [annex_sjprocess.tex](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/iworms/-/blob/master/paper/annex_sjprocess.tex) (subsection *Data preprocessing* and subsection *Computing correlations*)


---


# Publications

[A methodological approach towards high-resolution surface wave imaging of the San Jacinto Fault Zone using ambient-noise recordings at a spatially dense array, Roux et al](https://hal.archives-ouvertes.fr/hal-01614804) [DOI:10.1093/gji/ggw193](https://dx.doi.org/10.1093/gji/ggw193)
