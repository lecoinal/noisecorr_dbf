#!/bin/bash
#OAR --project imagin
#OAR -l /nodes=1/cpu=1/core=1,walltime=10:00:00
#OAR -n compute_mean
#OAR -p network_address='luke4'

#  =======================================================================================
#   ***  Shell wrapper to compute global mean from daily correlations in the CIMENT grid ***
#    
#  =======================================================================================
#               
#  ---------------------------------------------------------------------------------------

set -e

source /applis/ciment/guix-start.bash
# Suppose that GUIX profile "corravg" was created with :
# guix install -p $GUIX_USER_PROFILE_DIR/corravg gcc-toolchain gfortran-toolchain hdf5 hdf5:fortran zlib
# Or with a GUIX manifest file
# guix package -m manifest_corravg.scm -p $GUIX_USER_PROFILE_DIR/corravg

refresh_guix corravg

YEAR=$2
JDAY1=$3
JDAY2=$4
FPCID=$5

# example : oarsub -S "./submit.sh 0 2014 132 150 EXP003"

F90EXE=corrglobalmean

cat $OAR_NODE_FILE
NBCORE=$(cat $OAR_NODE_FILE | wc -l)
echo "NBCORE : $NBCORE"

echo $YEAR $JDAY1 $JDAY2 $FPCID

case "$CLUSTER_NAME" in
  luke)
    if [[ "$HOSTNAME" == "luke4" ]] ; then 
      echo "$HOSTNAME ; working in MD1200 ; reading input from MD1200 "
      IDIR="/scratch_md1200/$USER/" # 40TB
    fi
    TMPDIR="${IDIR}/oar.SJmean.$OAR_JOB_ID";
    ;;
esac

mkdir -p $TMPDIR
cp $F90EXE $TMPDIR
cp stalist $TMPDIR

cat namelist.mean > $TMPDIR/namelist

cd $TMPDIR

for JD in $(seq $JDAY1 $JDAY2) ; do
  JDAY=$(printf "%03d" $JD)
  if [ -f "$IDIR/SAN_JACINTO/CORRELATION/${FPCID}/DAILYMEAN/SG.DAILYMEAN.${YEAR}${JDAY}.000000.h5" ] ; then
    ln -sf $IDIR/SAN_JACINTO/CORRELATION/${FPCID}/DAILYMEAN/SG.DAILYMEAN.${YEAR}${JDAY}.000000.h5 .
  else
    ln -sf $SHARED_SCRATCH_DIR/$USER/SAN_JACINTO/CORRELATION/${FPCID}/DAILYMEAN/SG.DAILYMEAN.${YEAR}${JDAY}.000000.h5 .
  fi
done

echo "Compute mean:                      $YEAR $JDAY1 - $JDAY2      Date and Time: $(date +%F" "%T"."%N)"

./$F90EXE -jday1 $JDAY1 -jday2 $JDAY2 -n $(wc -l stalist | awk '{print $1}')

echo "Move Results:                      $YEAR $JDAY1 - $JDAY2      Date and Time: $(date +%F" "%T"."%N)"

#mkdir -p ${IDIR}/SAN_JACINTO/CORRELATION/${FPCID}/GLOBALMEAN
#mv cmean.h5 ${IDIR}/SAN_JACINTO/CORRELATION/${FPCID}/GLOBALMEAN/SG.GLOBALMEAN.${YEAR}.${JDAY1}.${JDAY2}.000000.h5
mkdir -p ${FPCID}/GLOBALMEAN
mv cmean.h5 ${FPCID}/GLOBALMEAN/SG.GLOBALMEAN.${YEAR}.${JDAY1}.${JDAY2}.000000.h5

echo "End job:                           $YEAR $JDAY1 - $JDAY2      Date and Time: $(date +%F" "%T"."%N)"
