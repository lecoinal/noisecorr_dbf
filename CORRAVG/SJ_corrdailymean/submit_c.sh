#!/bin/bash

#  =======================================================================================
#   ***  Shell wrapper to compute daily mean from high frequency correlations in the CIMENT grid ***
#    
#  =======================================================================================
#               
#  ---------------------------------------------------------------------------------------

set -e

source /applis/ciment/guix-start.bash
# Suppose that GUIX profile "corravg" was created with :
# guix install -p $GUIX_USER_PROFILE_DIR/corravg gcc-toolchain gfortran-toolchain hdf5 hdf5:fortran zlib
# Or with a GUIX manifest file
# guix package -m manifest_corravg.scm -p $GUIX_USER_PROFILE_DIR/corravg

refresh_guix corravg

YEAR=$2   # 2014   # $1
JDAY=$3   # 145    # $2
FPCID=$4    # Final preprocessing cigri campaign id

CEXE=corrdailymeanc

cat $OAR_NODE_FILE
NBCORE=$(cat $OAR_NODE_FILE | wc -l)
echo "NBCORE : $NBCORE"

echo $YEAR $JDAY $FPCID

case "$CLUSTER_NAME" in
  luke)
    if [[ "$HOSTNAME" == "luke4" ]] ; then 
      echo "$HOSTNAME ; working in MD1200 ; reading input from MD1200 "
      IDIR="/scratch_md1200/$USER/" # 40TB
    else   # luke1 or luke2
      echo "$HOSTNAME ; working in nfs_scratch ; reading input from nfs_scratch"
      IDIR="$SHARED_SCRATCH_DIR/$USER/"
    fi
    TMPDIR="${IDIR}/oar.SJmean.$OAR_JOB_ID";
    ;;
esac

mkdir -p $TMPDIR
cp $CEXE $TMPDIR

cd $TMPDIR

if [ -f "$IDIR/SAN_JACINTO/CORRELATION/${FPCID}/SG.${YEAR}${JDAY}.000000.h5" ] ; then
  ln -sf $IDIR/SAN_JACINTO/CORRELATION/${FPCID}/SG.${YEAR}${JDAY}.000000.h5 .
else
  ln -sf /nfs_scratch/$USER/SAN_JACINTO/CORRELATION/${FPCID}/SG.${YEAR}${JDAY}.000000.h5 .
fi

echo -n "nbpair: "
h5ls SG.${YEAR}${JDAY}.000000.h5 | wc -l

echo "Compute mean:                      $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"

./$CEXE -f SG.${YEAR}${JDAY}.000000.h5

echo "Move Results:                      $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"

mkdir -p ${IDIR}/SAN_JACINTO/CORRELATION/${FPCID}-DAILYMEAN/
mv cmean.h5 ${IDIR}/SAN_JACINTO/CORRELATION/${FPCID}-DAILYMEAN/SG.DAILYMEAN.${YEAR}${JDAY}.000000.h5

echo "End job:                           $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"
