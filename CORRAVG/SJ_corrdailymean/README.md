[Averaging correlations (daily and global means)](https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Computing_daily_and_monthly_means)

---

```
> source /applis/ciment/guix-start.bash
```

We use the [GUIX manifest file manifest_corravg.scm](./manifest_corravg.scm)

```
> guix package -m manifest_corravg.scm -p $GUIX_USER_PROFILE_DIR/corravg
```

And then for each session:
```
> source /applis/ciment/guix-start.bash
> refresh_guix corravg
```

And then compile the C Code or Fortran Code:
```
> make -f Makefile_c
# or
> make
```
