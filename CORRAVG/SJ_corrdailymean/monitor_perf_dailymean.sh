#!/bin/bash

for f in $(ls *out) ; do 
#  echo $f
  node=$(head -1 $f)
  jid=$(echo $f | awk -F. '{print $((NF-1))}')
  Inittime=$(grep "Compute mean" $f | sed -e "s/2014-11-01/2014-10-32/" | awk '{print $((NF-1)) " " $NF}' | sed -e "s/-/ /g" -e "s/:/ /g" )
  Endtime=$(grep "End job" $f | sed -e "s/2014-11-01/2014-10-32/" | awk '{print $((NF-1)) " " $NF}' | sed -e "s/-/ /g" -e "s/:/ /g" )

  j0=$(echo $Inittime | awk '{print $3}')
  t0=$(echo $Inittime | awk -v var=$j0 '{print ($3-var)*86400+$4*3600+$5*60+$6}')
  t1=$(echo $Endtime  | awk -v var=$j0 '{print ($3-var)*86400+$4*3600+$5*60+$6}')
  dt=$(echo "$t0 $t1" | awk '{print $2-$1}')
  nbc=$(grep nbpair $f | awk -F= '{print $2}')
  nbcore=$(grep -cx $node $f)
  fpcid=$(grep -m 1 2014 $f | awk '{print $NF}')
  jd=$(grep -m 1 2014 $f | awk '{print $((NF-1))}')
  echo "$node $nbcore $nbc $dt $fpcid $jd" | sed -e "s/luke//"
done



