//###############################################################################
//#    This file is part of noisecorr_dbf : a C code for computing              #
//#    daily mean starting from 10-min cross-correlations                       #
//#    on the dataset from San Jacinto Fault zone                               #
//#    dense array of geophones                                                 #
//#                                                                             #
//#    Copyright (C) 2015 Albanne Lecointre                                     #
//#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
//#                                                                             #
//#    This program is free software: you can redistribute it and/or modify     #
//#    it under the terms of the GNU General Public License as published by     #
//#    the Free Software Foundation, either version 3 of the License, or        #
//#    (at your option) any later version.                                      #
//#                                                                             #
//#    This program is distributed in the hope that it will be useful,          #
//#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
//#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
//#    GNU General Public License for more details.                             #
//#                                                                             #
//#    You should have received a copy of the GNU General Public License        #
//#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
//###############################################################################


//  =======================================================================================
//                       ***  C program corrdailymeanc.c  ***
//   To compute daily mean starting from 10-min correlations
//    
//  =======================================================================================
//   History :  1.0  : 09/2015  : A. Lecointre : Initial C version
//               
//  ---------------------------------------------------------------------------------------
//
//  ---------------------------------------------------------------------------------------
//   methodology   : use H5Literate to iterate over the total number correlation pairs
//                 : this C program replaces Fortran corrdailymean.f90 program as the 
//   Fortran program becomes very long when nb pairs > 200,000
//
//   https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Computing_daily_and_monthly_means
//
//  ---------------------------------------------------------------------------------------

#include <hdf5.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#define nn_npc 401   //                  ! Correlation time length   ! INTEGER
#define nn_seg 24    //                  ! The total number of nn_unitseg segment in one day (if 10min segment : 144)

int progressCount = 0; // , oldProgressCount = 0;
//time_t progressDate = -1.0;
  hid_t outfile_id;

herr_t average(hid_t loc_id, const char *name, const H5L_info_t *info,
            void *progress) {

    herr_t          status;
    H5O_info_t      infobuf;
    hid_t           dset_id;
    float           buf[nn_seg][nn_npc];

    /*
     * Get type of the object and display its name and type.
     * The name of the object is passed to this function by
     * the Library.
     */
    status = H5Oget_info_by_name (loc_id, name, &infobuf, H5P_DEFAULT);
    switch (infobuf.type) {
        case H5O_TYPE_GROUP:
            printf ("  Group(%d): %s\n", progressCount+1, name);
            break;
        case H5O_TYPE_DATASET:
            printf ("  Dataset: %s\n", name);
            break;
        case H5O_TYPE_NAMED_DATATYPE:
            printf ("  Datatype: %s\n", name);
            break;
        default:
            printf ( "  Unknown: %s\n", name);
    }
   char corrname[99];
   sprintf(corrname, "%s/EPZ_EPZ/corr", name);
   dset_id = H5Dopen2(loc_id, corrname, H5P_DEFAULT);
   status = H5Dread(dset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, 
                    buf);
   status = H5Dclose(dset_id);

   int i,j;
   double cm[nn_npc] = { 0 };
   float fcm[nn_npc] = { 0 };
   for (i=0; i<nn_seg; i++)
   {
         for (j=0; j<nn_npc; j++)
         {
	       cm[j] += buf[i][j];
         }
   }
   for (j=0; j<nn_npc; j++)
   {
           fcm[j] = (float)(cm[j]/nn_seg);
   }
   hid_t       grpsta_id,grpchan_id;
   hid_t       dataset_id, dataspace_id;  /* identifiers */
   hsize_t     dims[1];
   grpsta_id = H5Gcreate2(outfile_id, name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
   grpchan_id = H5Gcreate2(grpsta_id, "EPZ_EPZ", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
   status = H5Gclose(grpsta_id);
   dims[0] = nn_npc; 
   dataspace_id = H5Screate_simple(1, dims, NULL);
   dataset_id = H5Dcreate2(grpchan_id, "corr", H5T_NATIVE_FLOAT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
   status = H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, fcm);
   status = H5Dclose(dataset_id);
   status = H5Sclose(dataspace_id);
   status = H5Gclose(grpchan_id);

   if ( progressCount % 100 == 0 ) {
//     double date;
//     time(&date);
//     if (progressCount > 0) {
//       printf("Total elapsed time to compute %8d mean %20.6f s",
//              progressCount - oldProgressCount, date - progressDate);
//     }
//     oldProgressCount = progressCount;
//     progressDate = date;

//  time_t timer;
//  struct tm y2k;
//  double seconds;
//  y2k.tm_hour = 0;   y2k.tm_min = 0; y2k.tm_sec = 0;
//  y2k.tm_year = 114; y2k.tm_mon = 0; y2k.tm_mday = 1;
//  time(&timer);  /* get current time; same as: timer = time(NULL)  */
//  seconds = difftime(timer,mktime(&y2k));
//  printf ("Total elapsed time to compute %8d mean %20.6f s\n",progressCount,seconds);

  clock_t t;
  t = clock();
  printf ("Total elapsed time to compute %8d mean %20.6f s (%ld clicks)\n",progressCount,((float)t)/CLOCKS_PER_SEC,t);

   }
   progressCount++;
   return 0;

}

int main(int argc, char **argv) {

  int maxlag; // in nb of samples
  int c, npc, npt;
  char *cn_filein;

  while ((c = getopt (argc, argv, "f:")) != -1)
    switch (c)
      {
      case 'f':
        cn_filein = optarg;
        break;
      case '?':
        if (optopt == 'c')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        return 1;
      default:
        abort ();
      }


  hid_t infile_id;
  infile_id = H5Fopen (cn_filein, H5F_ACC_RDONLY, H5P_DEFAULT);
  outfile_id = H5Fcreate("cmean.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
//  double progress[2] = { 0.0, -1.0 };
//  herr_t error = H5Literate (infile_id, H5_INDEX_NAME, H5_ITER_NATIVE, NULL, average, progress);
  herr_t error = H5Literate (infile_id, H5_INDEX_NAME, H5_ITER_NATIVE, NULL, average, NULL);
  error = H5Fclose (infile_id);
  error = H5Fclose (outfile_id);
  return 0;
}

