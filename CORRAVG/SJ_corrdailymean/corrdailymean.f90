!###############################################################################
!#    This file is part of noisecorr_dbf : a Fortran code for computing        #
!#    daily mean starting from 10-min cross-correlations                       #
!#    on the dataset from San Jacinto Fault zone                               #
!#    dense array of geophones                                                 #
!#                                                                             #
!#    Copyright (C) 2015 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################

!  =======================================================================================
!                       ***  Fortran program corrdailymean.f90  ***
!   To compute daily mean starting from 10-min correlations
!    
!  =======================================================================================
!   History :  1.0  : 09/2015  : A. Lecointre : Initial Fortran version
!               
!  ---------------------------------------------------------------------------------------
!
!  ---------------------------------------------------------------------------------------
!   methodology   : use h5gn_members_f to know the number of correlation pairs
!                 : and then iterate over the total nb of correlations and find them with
!   h5gget_obj_info_idx_f
!   Caution  : After ~ 200,000 members, h5gget_obj_info_idx_f becomes very very long, and 
!   then this program can't be used and should be replaced by C program using H5Literate: 
!   see corrdailymeanc.c
!
!   https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Computing_daily_and_monthly_means
!
!  ---------------------------------------------------------------------------------------

PROGRAM CORRDAILYMEAN

  USE HDF5 ! HDF5 module
  USE H5LT

  IMPLICIT NONE

  LOGICAL :: l_exist_namelist

  ! Output management

  ! Input parameters
  INTEGER :: nn_samprate   ! Input dataset frequency
  INTEGER :: nn_unitseg    ! length in sec of a unitary segment 1200 sec (24000 points at 20Hz)
  INTEGER :: nn_lag        ! the lag in sec for the correlation
  INTEGER :: maxlag      ! in nb of samples (5min*60*sampling rate) : 6000

  INTEGER :: npc         ! = 2*maxlag+1 : total nb of points for the output correlation : 12001
  INTEGER :: npt

  CHARACTER(LEN=128) :: cn_filein
  CHARACTER(LEN=11), DIMENSION(:), ALLOCATABLE :: corrname
  CHARACTER(LEN=20) :: cbuf

  ! Manipulation des fichiers et des datasets HDF5
  ! Input
  INTEGER(HID_T) :: infile_id      ! Input File identifier: this HDF5 file contains all the 10min corr
  INTEGER(HID_T) :: dset_id        ! Input and Output Dataset identifiers (released immediately)
  INTEGER(HSIZE_T), DIMENSION(2) :: indims
  ! Output
  INTEGER(HID_T) :: outfile_id     ! Output File identifier: the HDF5 File containing all the correlations for all the pairs
  INTEGER(HSIZE_T), DIMENSION(1) :: outdims ! The total current dimension for output dataset
  INTEGER(HID_T) :: grpsta_id
  INTEGER(HID_T) :: grpchan_id

  ! Iterators
  INTEGER    :: jk

  INTEGER   :: error ! Error flag

  INTEGER :: obj_type            ! Type of the object
  INTEGER :: nbpair

  ! Buffers for input and output datasets
  REAL(KIND=4), DIMENSION(:,:), ALLOCATABLE :: buf
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: cm

  ! Declare NAMELIST
!  NAMELIST /namio/ nn_scaleoffset 
  NAMELIST /namparam/ cn_filein,nn_samprate,nn_lag,nn_unitseg

  ! Read namelist
  INQUIRE(file='namelist', exist=l_exist_namelist)
  IF (.NOT. l_exist_namelist) THEN
     PRINT *,'The namelist does not exist'
     STOP
  ENDIF
  OPEN(11,file='namelist')
!  PRINT *,'Reading namelist namio'
!    REWIND 11
!    READ(11,namio)
!    PRINT *,'nn_scaleoffset = ',nn_scaleoffset
  PRINT *,'Reading namelist namparam'
    REWIND 11
    READ(11,namparam)
    PRINT *,'cn_filein: ',TRIM(cn_filein)
    PRINT *,'nn_samprate: ',nn_samprate,' Hz'
    PRINT *,'nn_unitseg: ',nn_unitseg,' sec'
    PRINT *,'nn_lag: ',nn_lag,' sec'
  CLOSE(11)

  ! Compute some useful parameters
  maxlag = nn_lag*nn_samprate  ! as a number of samples : 200
  npc = 2*maxlag+1             ! final length of the correlation vector : 401
  npt = 86400/nn_unitseg
  outdims = (/npc/)    ! 86400/600=144
  indims = (/npc,npt/)
  
  ! Read one time the entire file to get the total number of correlations (= the total nb of pairs)
  CALL h5open_f(error)
  CALL h5fopen_f(TRIM(cn_filein), H5F_ACC_RDONLY_F, infile_id, error)
  CALL h5gn_members_f(infile_id, "/", nbpair, error)
  print *,'nbpair=',nbpair

  ! Create output file
  CALL h5fcreate_f('cmean.h5', H5F_ACC_TRUNC_F, outfile_id, error)

  ! Allocate array
  ALLOCATE(corrname(nbpair)) 

  ! Browse the input file to know the station pair names
  ! Read all the input correlations and compute daily mean
  ALLOCATE(buf(npc,npt)) ! Fortran order : first dim is contiguous
  ALLOCATE(cm(npc)) ! Fortran order : first dim is contiguous

  DO jk = 1, nbpair ! Loop on correlations
    CALL h5gget_obj_info_idx_f(infile_id, "/", jk-1, cbuf, obj_type, error)
    corrname(jk)=TRIM(cbuf)
    print *,'corrname(',jk,')=',corrname(jk)
    buf(:,:)=0.0
    cm(:)=0.d0

    CALL h5ltread_dataset_float_f(infile_id, "/"//TRIM(corrname(jk))//"/EPZ_EPZ/corr", buf, indims, error)

    cm(:) = DBLE(SUM (buf, 2)) / DBLE(npt)

    CALL create_or_open_group(outfile_id, TRIM(corrname(jk)), grpsta_id )
    CALL create_or_open_group(grpsta_id,  "EPZ_EPZ", grpchan_id)
    CALL h5gclose_f( grpsta_id, error)
    CALL h5ltmake_dataset_float_f(grpchan_id, 'corr', 1, outdims, REAL(cm(:)), error)
    CALL h5gclose_f( grpchan_id, error)
  ENDDO

  ! Close the input daily file
  CALL h5fclose_f(infile_id, error)

  ! Close the 1day output file
  CALL h5fclose_f(outfile_id, error)

  CALL h5close_f(error)

END PROGRAM CORRDAILYMEAN

SUBROUTINE create_or_open_group(parent_id,gr_name,gr_id)

  USE HDF5 ! HDF5 module

  IMPLICIT NONE

  INTEGER(HID_T), INTENT(in)     :: parent_id
  CHARACTER(LEN=*), INTENT(in)   :: gr_name
  INTEGER(HID_T), INTENT(out)    :: gr_id
  LOGICAL                        :: link_exists
  INTEGER                        :: error

  ! Output file : create group if necessary, or simply open it
  CALL h5lexists_f(parent_id, gr_name, link_exists, error, H5P_DEFAULT_F)
  IF ( link_exists ) THEN
    CALL h5gopen_f(parent_id, gr_name, gr_id, error)
  ELSE
    CALL h5gcreate_f(parent_id, gr_name, gr_id, error)
  ENDIF

END SUBROUTINE create_or_open_group
