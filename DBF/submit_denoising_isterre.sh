#!/bin/bash
#OAR --project iste-equ-ondes
#OAR -l /nodes=1/cpu=1/core=1,walltime=5:00:00
#OAR -n denoising
#OAR -p network_address='ist-calcul1.ujf-grenoble.fr'

#  =======================================================================================
#   ***  Shell wrapper to submit denoising.f90 in the ISTERRE OAR resources ***
#    
#  =======================================================================================
#   History :  1.0  : 12/2016  : A. Lecointre : Initial version
#               
#  ---------------------------------------------------------------------------------------
#  $Id$
#  ---------------------------------------------------------------------------------------

set -e

#########################################################

getParam() {
   pair=$(grep "^ *$1 *= *" namelist | sed 's/!.*$//')
   if [[ -z "${pair}" ]] ; then
      echo 0
   else
      echo $(echo ${pair} | awk -F = '{ print $2 }' | awk '{ print $1 }')
   fi
}

#####################################################################

source /soft/env.bash

module load intel-devel/15.0.1 hdf5/1.8.14_intel-15.0.1 szip/2.1_intel-15.0.1 zlib/1.2.8_intel-15.0.1 fftw/3.3.4_intel-15.0.1

#h5pfc -pg -g -O2 -c   -o dbfmodule.o ../dbfmodule.f90  -I/soft/x86_64/intel_15.0.1/fftw_3.3.4/include -lfftw3

#h5pfc -pg -g -O2 -o denoising dbfmodule.o denoising.f90  -I/soft/x86_64/intel_15.0.1/fftw_3.3.4/include -lfftw3 -L/soft/x86_64/intel_15.0.1/hdf5_1.8.14/lib -L/soft/x86_64/intel_15.0.1/szip_2.1/lib -L/soft/x86_64/intel_15.0.1/zlib_1.2.8/lib -L/soft/x86_64/intel_15.0.1/fftw_3.3.4/lib

TMPDIR=$SHARED_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID
mkdir -p $TMPDIR

cp denoising name.txt reseau_64.h5 $TMPDIR/.
cp namelist.denoising $TMPDIR/namelist

cd $TMPDIR

freq_name=$(getParam cn_freq_name)
inputfile=$(getParam cn_inputfile)
ln -sf /media/luke4_scratch_md1200/bouep/${freq_name}-DAILYMEAN/${inputfile} . 

time ./denoising



