#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import scipy.io as sio
import numpy as np

number=64
mat_contents = sio.loadmat('/data/ondes/rouxphi/yehuda/june_2014/reseau_'+str(number)+'.mat')
x = mat_contents['x']
y = mat_contents['y']
z = mat_contents['z']
reseau = mat_contents['reseau']
all_stations = mat_contents['all_stations']

# >>> reseau.shape
# (1, 1)

val=reseau[0,0]

# >>> val.dtype
# dtype([('name', 'O'), ('x', 'O'), ('y', 'O'), ('z', 'O')])

reseauname=val['name']
# reseauname[0,0] : tableau (64,5)
# reseauname[0,1107] : tableau (64,5)
# >>> a=reseauname[0,1107]
# >>> a.shape
# (64,)
# >>> aa=reseauname[0,1107][0]
# >>> ab=reseauname[0,1107][63]

reseaux=val['x']
reseauy=val['y']
reseauz=val['z']
# >>> reseaux.shape
# (1108, 64)

import h5py
import numpy as np
f = h5py.File("reseau_64.h5", "w")
dset = f.create_dataset("all_stations", (1108,), dtype='S5')
for ista in range(1108):
	dset[ista] = str(all_stations[ista])

dset = f.create_dataset("x", (1108,), dtype='f8')
dset[:] = x[:,0]
dset = f.create_dataset("y", (1108,), dtype='f8')
dset[:] = y[:,0]
dset = f.create_dataset("z", (1108,), dtype='f8')
dset[:] = z[:,0]
dset = f.create_dataset("reseaux", (1108,64), dtype='f8')
dset[:,:] = reseaux[:,:]
dset = f.create_dataset("reseauy", (1108,64), dtype='f8')
dset[:,:] = reseauy[:,:]
dset = f.create_dataset("reseauz", (1108,64), dtype='f8')
dset[:,:] = reseauz[:,:]
#dset = f.create_dataset("reseauname", (1108,64), dtype='S5')
dset = f.create_dataset("reseauname", (1108,64), dtype='a5')
fascii = open('reseauname.txt', 'wb')
for ista in range(1108):
#	dset = f.create_dataset("reseauname"+str(all_stations[ista]), (64,), dtype='S5')
#	aa=reseauname[0,ista]
#	for index in range(64):
#		dset[index]=str(aa[index])
	aa=reseauname[0,ista]
	for index in range(63):
		dset[ista,index]=str(aa[index])
		fascii.write(str(aa[index])+" ")
        index=63
        dset[ista,index]=str(aa[index])
        fascii.write(str(aa[index]))
	fascii.write("\n")
f.close()
fascii.close()

fascii = open('name.txt', 'wb')
for ista in range(1108):
        fascii.write(str(all_stations[ista]))
	fascii.write("\n")
fascii.close()
