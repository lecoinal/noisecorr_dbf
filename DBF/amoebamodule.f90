!###############################################################################
!#    This file is part of noisecorr_dbf : a Fortran code for computing        #
!#    double beamforming on the dataset from San Jacinto Fault zone            #
!#    dense array of geophones                                                 #
!###############################################################################
!
! https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Double_beamforming

MODULE amoebamodule

!  IMPLICIT NONE

  CONTAINS

!! from http://jean-pierre.moreau.pagesperso-orange.fr/Fortran/tamoeba_f90.txt
SUBROUTINE AMOEBA(FUNC,P,Y,MP,NP,NDIM,FTOL,ITER)
!-------------------------------------------------------------------
! Multidimensional minimization of the function FUNC(X) where X is
! an NDIM-dimensional vector, by the downhill simplex method of
! Nelder and Mead. Input is a matrix P whose NDIM+1 rows are NDIM-
! dimensional vectors which are the vertices of the starting simplex
! (Logical dimensions of P are P(NDIM+1,NDIM); physical dimensions
! are input as P(NP,NP)). Also input is the vector Y of length NDIM
! +1, whose components must be pre-initialized to the values of FUNC
! evaluated at the NDIM+1 vertices (rows) of P; and FTOL the fractio-
! nal convergence tolerance to be achieved in the function value. On
! output, P and Y will have been reset to NDIM+1 new points all within
! FTOL of a minimum function value, and ITER gives the number of ite-
! rations taken.
!--------------------------------------------------------------------
IMPLICIT REAL*8 (A-H,O-Z)
PARAMETER(NMAX=20,ALPHA=1.d0,BETA=0.5d0,GAMMA=2.d0,ITMAX=500)
! Expected maximum number of dimensions, three parameters which define
! the expansions and contractions, and maximum allowed number of
! iterations.
!REAL*8 A-H ! ,O-Z
DIMENSION P(MP,NP), Y(MP), PR(NMAX), PRR(NMAX), PBAR(NMAX)
!  real ( kind = 8 ), external :: FUNC
  real*8, external :: FUNC
  MPTS=NDIM+1
  ITER=0
1 ILO=1
  IF(Y(1).GT.Y(2)) THEN
    IHI=1
    INHI=2
  ELSE
    IHI=2
    INHI=1
  ENDIF
  DO I=1, MPTS
    IF(Y(I).LT.Y(ILO)) ILO=I
    IF(Y(I).GT.Y(IHI)) THEN
      INHI=IHI
      IHI=I
    ELSE IF (Y(I).GT.Y(INHI)) THEN
      IF(I.NE.IHI) INHI=I
    END IF
  END DO
! Compute the fractional range from highest to lowest and return if
! satisfactory.
  RTOL=2.d0*ABS(Y(IHI)-Y(ILO))/(ABS(Y(IHI))+ABS(Y(ILO)))
  IF(RTOL.LT.FTOL) RETURN
  IF(ITER.EQ.ITMAX) Pause ' Amoeba exceeding maximum iterations.'
  ITER=ITER+1
  DO J=1, NDIM
    PBAR(J)=0.d0
  END DO
  DO I=1, MPTS
    IF(I.NE.IHI) THEN
      DO J=1,NDIM
        PBAR(J)=PBAR(J) + P(I,J)
      END DO
    END IF   
  END DO
  DO J=1, NDIM
    PBAR(J)=PBAR(J)/NDIM
    PR(J)=(1.d0+ALPHA)*PBAR(J) - ALPHA*P(IHI,J)
  END DO
  YPR=FUNC(PR)
  IF(YPR.LE.Y(ILO)) THEN
    DO J=1,NDIM
      PRR(J)=GAMMA*PR(J) + (1.d0-GAMMA)*PBAR(J)
    END DO
    YPRR=FUNC(PRR)
    IF(YPRR.LT.Y(ILO)) THEN
      DO J=1, NDIM
        P(IHI,J)=PRR(J)
      END DO
      Y(IHI)=YPRR
    ELSE
      DO J=1, NDIM
        P(IHI,J)=PR(J)
      END DO
      Y(IHI)=YPR	  
    END IF
  ELSE IF(YPR.GE.Y(INHI)) THEN
    IF(YPR.LT.Y(IHI)) THEN
      DO J=1, NDIM
        P(IHI,J)=PR(J)
      END DO
      Y(IHI)=YPR
    END IF
    DO J=1, NDIM
      PRR(J)=BETA*P(IHI,J) + (1.d0-BETA)*PBAR(J)
    END DO
    YPRR=FUNC(PRR)
    IF(YPRR.LT.Y(IHI)) THEN
      DO J=1, NDIM
        P(IHI,J)=PRR(J)
      END DO
      Y(IHI)=YPRR
    ELSE
      DO I=1, MPTS
        IF(I.NE.ILO) THEN
          DO J=1,NDIM
            PR(J)=0.5d0*(P(I,J) + P(ILO,J))
	    P(I,J)=PR(J)
	  END DO
          Y(I)=FUNC(PR)
	END IF
      END DO
    END IF
  ELSE
    DO J=1, NDIM
      P(IHI,J)=PR(J)
    END DO
    Y(IHI)=YPR
  END IF
  GO TO 1
  END





END MODULE amoebamodule
