#!/bin/bash
#OAR --project imagin
#OAR -l /nodes=1/cpu=1/core=1,walltime=5:00:00
#OAR -n denoising
#OAR -p network_address='luke4'

#  =======================================================================================
#   ***  Shell wrapper to submit denoising.f90 in the CIMENT OAR resources ***
#    
#  =======================================================================================
#   History :  1.0  : 12/2016  : A. Lecointre : Initial version
#               
#  ---------------------------------------------------------------------------------------

set -e

#########################################################

getParam() {
   pair=$(grep "^ *$1 *= *" namelist | sed 's/!.*$//')
   if [[ -z "${pair}" ]] ; then
      echo 0
   else
      echo $(echo ${pair} | awk -F = '{ print $2 }' | awk '{ print $1 }')
   fi
}

#####################################################################

source /applis/site/guix-start.bash
# Suppose that GUIX profile "dbf" was created with :
# guix install -p $GUIX_USER_PROFILE_DIR/dbf gfortran-toolchain fftw hdf5 hdf5:fortran zlib
# Or with a GUIX manifest file
# guix package -m manifest_dbf.scm -p $GUIX_USER_PROFILE_DIR/dbf
refresh_guix dbf

TMPDIR=$SHARED_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID
mkdir -p $TMPDIR

cp denoising $TMPDIR/.
cp namelist.denoising $TMPDIR/namelist

cd $TMPDIR

freq_name=$(getParam cn_freq_name)
inputfile=$(getParam cn_inputfile)
ln -sf /scratch_md1200/bouep/${freq_name}-DAILYMEAN/${inputfile} . 
ln -sf /scratch_md1200/lecointre/SAN_JACINTO/GRID/reseau_64.h5 .
ln -sf /scratch_md1200/lecointre/SAN_JACINTO/GRID/name.txt .

time ./denoising



