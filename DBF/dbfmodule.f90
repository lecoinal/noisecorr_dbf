!###############################################################################
!#    This file is part of noisecorr_dbf : a Fortran code for computing        #
!#    double beamforming on the dataset from San Jacinto Fault zone            #
!#    dense array of geophones                                                 #
!#                                                                             #
!#    Copyright (C) 2015 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################
!
! https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Double_beamforming

MODULE dbfmodule

  IMPLICIT NONE

  CONTAINS

SUBROUTINE dist_wh(dist,lat1,lat2,lon1,lon2)
  IMPLICIT NONE
  REAL(KIND=8), INTENT(out) :: dist
  REAL(KIND=8), INTENT(in) :: lat1,lat2,lon1,lon2
  REAL(KIND=8), PARAMETER :: rT = 6378000.d0  ! Rayon terrestre
  REAL(KIND=8) :: pi_over_180
  pi_over_180=4.d0 * DATAN(1.d0) / 180.d0
  dist = rT * DACOS( DSIN(lat1 * pi_over_180) * DSIN(lat2 * pi_over_180) + &
          DCOS(lat1 * pi_over_180) * DCOS(lat2 * pi_over_180) * DCOS((lon2-lon1) * pi_over_180) )
END SUBROUTINE dist_wh
  
SUBROUTINE azimuth_and_dist(dist,af,dlat1,dlat2,dlon1,dlon2)
  IMPLICIT NONE
  REAL(KIND=8), INTENT(in) :: dlat1,dlat2,dlon1,dlon2
  REAL(KIND=8), INTENT(out) :: dist,af
  REAL(KIND=8), PARAMETER :: pi = 4.d0 * ATAN(1.d0)
  REAL(KIND=8), PARAMETER :: pi_over_180 = 4.d0 * ATAN(1.d0) / 180.d0
  REAL(KIND=8), PARAMETER :: A = 6378137.d0
  REAL(KIND=8), PARAMETER :: E = 0.081819191d0  
  REAL(KIND=8) :: PHI1,PHI2,XLAM1,XLAM2,DLAM2,CTA12,A12,DPHI2,DLAM,EE,EPS,lat1,lat2,lon1,lon2,xnu1,xnu2,TPSI2,PSI2
  REAL(KIND=8) :: SIG,SSIG,G2,G,H2,H,TERM1,TERM2,TERM3,TERM4,somme
  REAL(KIND=8), DIMENSION(6) :: dd2
  REAL(KIND=8), PARAMETER :: epsilon = 2.d0**(-52.d0)
  INTEGER :: i1,i2,i3,bigbrnch

  EE = E**2.d0
  EPS = EE/(1.d0-EE)
  lat1 = dlat1 * pi_over_180
  lat2 = dlat2 * pi_over_180
  lon1 = dlon1 * pi_over_180
  lon2 = dlon2 * pi_over_180
  IF (lat1==0.d0) lat1=lat1*epsilon
  IF (lat2==0.d0) lat2=lat2*epsilon
  ! COMPUTE THE RADIUS OF CURVATURE IN THE PRIME VERTICAL FOR EACH POINT
  xnu1=A/SQRT(1.d0-(E*SIN(lat1))**2.d0)
  xnu2=A/SQRT(1.d0-(E*SIN(lat2))**2.d0)
  ! wiggle lines of constant lat to prevent numerical probs.
  PHI1=lat1
  PHI2=lat2
  XLAM1=lon1
  XLAM2=lon2
  IF (PHI1==PHI2) PHI2=PHI2+10.d0**(-14.d0)
  IF (XLAM1==XLAM2) XLAM2=XLAM2+10.d0**(-14.d0)
  ! COMPUTE THE AZIMUTH.  A12 IS THE AZIMUTH AT POINT 1 OF THE NORMAL SECTION CONTAINING THE POINT 2
  TPSI2=(1.d0-EE)*TAN(PHI2) + EE * xnu1*SIN(PHI1) / (xnu2*COS(PHI2))
  PSI2=ATAN(TPSI2)
  ! SOME FORM OF ANGLE DIFFERENCE COMPUTED HERE
  DPHI2=PHI2-PSI2
  DLAM=XLAM2-XLAM1
  CTA12 = ( COS(PHI1)*TPSI2 - SIN(PHI1)*COS(DLAM) ) / SIN(DLAM)
  A12 = ATAN(1.d0/CTA12)
  ! GET THE QUADRANT RIGHT
  IF (ABS(DLAM) < pi) THEN
    i1=1
  ELSE
    i1=0
  ENDIF ! i1 = (ABS(DLAM) < pi)
  IF (DLAM >= pi) THEN
    i2=1
  ELSE
    i2=0
  ENDIF ! i2 = (DLAM >= pi)
  IF (DLAM <= -pi) THEN
    i3=1
  ELSE
    i3=0
  ENDIF ! i3 = (DLAM <= -pi)
  DLAM2 = DBLE(i1) * DLAM + DBLE(i2) * (-2.d0*pi+DLAM) + DBLE(i3) * (2.d0*pi+DLAM)
  IF (A12<-pi) THEN
    i1=1
  ELSE
    i1=0
  ENDIF ! i1 = (A12<-pi)
  IF (A12>=pi) THEN
    i2=1
  ELSE
    i2=0
  ENDIF ! i2 = (A12>=pi)
  A12 = A12+DBLE(i1)*2.d0*pi-DBLE(i2)*2.d0*pi
  IF ( (A12 * DLAM2) < 0.d0 ) THEN
    i3=1
  ELSE
    i3=0
  ENDIF ! i3 = ( sign(A12) ~= sign(DLAM2) )
  A12 = A12 + SIGN(pi,-A12)*DBLE(i3) ! A12=A12+pi*sign(-A12).*( sign(A12) ~= sign(DLAM2) );
  af=A12*180.d0/pi
  SSIG=SIN(DLAM)*COS(PSI2)/SIN(A12)
  ! At this point we are OK if the angle < 90...but otherwise
  ! we get the wrong branch of asin! 
  ! This fudge will correct every case on a sphere, and *almost*
  ! every case on an ellipsoid (wrong hnadling will be when
  ! angle is almost exactly 90 degrees)
  dd2(1)=COS(lon1)*COS(lat1)
  dd2(2)=COS(lon2)*COS(lat2)
  dd2(3)=SIN(lon1)*COS(lat1)
  dd2(4)=SIN(lon2)*COS(lat2)
  dd2(5)=SIN(lat1)
  dd2(6)=SIN(lat2)
  somme=0.d0
  DO i1=1,5
    somme = somme + (dd2(i1+1)-dd2(i1))**2.d0
  ENDDO
!  IF ( ABS(somme-2.d0) < 2.d0 ) THEN  ! B=0
!    print *,'dist: Warning...point(s) too close to 90 degrees apart'
!  ENDIF
  IF ( somme > 2.d0 ) THEN
    bigbrnch = 1
  ELSE
    bigbrnch = 0
  ENDIF
  SIG=ASIN(SSIG)*(-bigbrnch+1) + (pi-ASIN(SSIG))*bigbrnch
  ! COMPUTE RANGE
  G2=EPS*((SIN(PHI1))**2.d0)
  G=SQRT(G2)
  H2=EPS*((COS(PHI1)*COS(A12))**2.d0)
  H=SQRT(H2)
  TERM1=-SIG*SIG*H2*(1.d0-H2)/6.d0
  TERM2=(SIG**3.d0)*G*H*(1.d0-2.d0*H2)/8.d0
  TERM3=(SIG**4.d0)*(H2*(4.d0-7.d0*H2)-3.d0*G2*(1.d0-7.d0*H2))/120.d0
  TERM4=-(SIG**5.d0)*G*H/48.d0
  dist = xnu1*SIG*(1.d0+TERM1+TERM2+TERM3+TERM4) 
END SUBROUTINE azimuth_and_dist

SUBROUTINE azimuth(af,dlat1,dlat2,dlon1,dlon2)
  IMPLICIT NONE
  REAL(KIND=8), INTENT(in) :: dlat1,dlat2,dlon1,dlon2
  REAL(KIND=8), INTENT(out) :: af
  REAL(KIND=8), PARAMETER :: pi = 4.d0 * ATAN(1.d0)
  REAL(KIND=8), PARAMETER :: pi_over_180 = 4.d0 * ATAN(1.d0) / 180.d0
  REAL(KIND=8), PARAMETER :: A = 6378137.d0
  REAL(KIND=8), PARAMETER :: E = 0.081819191d0  
  REAL(KIND=8) :: DLAM2,CTA12,A12,DPHI2,DLAM,EE,EPS,lat1,lat2,lon1,lon2,xnu1,xnu2,TPSI2,PSI2
  REAL(KIND=8), PARAMETER :: epsilon = 2.d0**(-52.d0)
  INTEGER :: i1,i2,i3

  EE = E**2.d0
  EPS = EE/(1.d0-EE)
  lat1 = dlat1 * pi_over_180
  lat2 = dlat2 * pi_over_180
  lon1 = dlon1 * pi_over_180
  lon2 = dlon2 * pi_over_180
  IF (lat1==0.d0) lat1=lat1*epsilon
  IF (lat2==0.d0) lat2=lat2*epsilon
  ! COMPUTE THE RADIUS OF CURVATURE IN THE PRIME VERTICAL FOR EACH POINT
  xnu1=A/SQRT(1.d0-(E*SIN(lat1))**2.d0)
  xnu2=A/SQRT(1.d0-(E*SIN(lat2))**2.d0)
  ! wiggle lines of constant lat to prevent numerical probs.
  IF (lat1==lat2) lat2=lat2+10.d0**(-14.d0)
  IF (lon1==lon2) lon2=lon2+10.d0**(-14.d0)
  ! COMPUTE THE AZIMUTH.  A12 IS THE AZIMUTH AT POINT 1 OF THE NORMAL SECTION CONTAINING THE POINT 2
  TPSI2=(1.d0-EE)*TAN(lat2) + EE * xnu1*SIN(lat1) / (xnu2*COS(lat2))
  PSI2=ATAN(TPSI2)
  ! SOME FORM OF ANGLE DIFFERENCE COMPUTED HERE
  DPHI2=lat2-PSI2
  DLAM=lon2-lon1
  CTA12 = ( COS(lat1)*TPSI2 - SIN(lat1)*COS(DLAM) ) / SIN(DLAM)
  A12 = ATAN(1.d0/CTA12)
  ! GET THE QUADRANT RIGHT
  IF (ABS(DLAM) < pi) THEN
    i1=1
  ELSE
    i1=0
  ENDIF ! i1 = (ABS(DLAM) < pi)
  IF (DLAM >= pi) THEN
    i2=1
  ELSE
    i2=0
  ENDIF ! i2 = (DLAM >= pi)
  IF (DLAM <= -pi) THEN
    i3=1
  ELSE
    i3=0
  ENDIF ! i3 = (DLAM <= -pi)
  DLAM2 = DBLE(i1) * DLAM + DBLE(i2) * (-2.d0*pi+DLAM) + DBLE(i3) * (2.d0*pi+DLAM)
  IF (A12<-pi) THEN
    i1=1
  ELSE
    i1=0
  ENDIF ! i1 = (A12<-pi)
  IF (A12>=pi) THEN
    i2=1
  ELSE
    i2=0
  ENDIF ! i2 = (A12>=pi)
  A12 = A12+DBLE(i1)*2.d0*pi-DBLE(i2)*2.d0*pi
  IF ( (A12 * DLAM2) < 0.d0 ) THEN
    i3=1
  ELSE
    i3=0
  ENDIF ! i3 = ( sign(A12) ~= sign(DLAM2) )
  A12 = A12 + SIGN(pi,-A12)*DBLE(i3) ! A12=A12+pi*sign(-A12).*( sign(A12) ~= sign(DLAM2) );
  af=A12*180.d0/pi
END SUBROUTINE azimuth

SUBROUTINE linspace(x, x_start, x_end, x_len)
  IMPLICIT NONE
  INTEGER, INTENT(in) :: x_len
  REAL(KIND=8), DIMENSION(x_len), INTENT(out) :: x
  REAL(KIND=8), INTENT(in) :: x_start,x_end
  REAL(KIND=8) :: dx
  INTEGER :: ii
 
  dx = ( x_end - x_start ) / ( x_len - 1 )
  x = (/( x_start + ((ii-1)*dx) , ii=1,x_len )/)

END SUBROUTINE linspace

SUBROUTINE read_one_trace(file_id,dsetname,rank,offset,count,nb,buf)
  USE HDF5
  IMPLICIT NONE
  INTEGER(HID_T), INTENT(in)     :: file_id
  CHARACTER(LEN=*), INTENT(in)   :: dsetname
  INTEGER, INTENT(in)            :: rank,nb
  INTEGER(HSIZE_T), DIMENSION(rank), INTENT(in) :: offset
  INTEGER(HSIZE_T), DIMENSION(rank), INTENT(in) :: count
  REAL(KIND=8), DIMENSION(nb), INTENT(out) :: buf
  INTEGER(HID_T) :: dset_id,space_id,memspace_id
  INTEGER                        :: error
  INTEGER(HSIZE_T), DIMENSION(1) :: memcount
  INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: memoffset = (/0/) 
  INTEGER(HSIZE_T), DIMENSION(1) :: indims

  memcount=(/nb/)
  indims = (/nb/)

  CALL h5dopen_f(file_id, dsetname, dset_id, error)
  CALL h5dget_space_f(dset_id, space_id, error)
  CALL h5sselect_hyperslab_f (space_id, H5S_SELECT_SET_F, offset, count, error)
  CALL h5screate_simple_f(1, memcount, memspace_id, error)
  CALL h5sselect_hyperslab_f(memspace_id, H5S_SELECT_SET_F, memoffset, memcount, error)
  buf(:)=0.d0
  CALL H5dread_f(dset_id, H5T_NATIVE_DOUBLE, buf, indims, error, memspace_id, space_id)
  CALL h5sclose_f(space_id, error)
  CALL h5sclose_f(memspace_id, error)
  CALL h5dclose_f(dset_id, error)

END SUBROUTINE read_one_trace

SUBROUTINE sort_string(str1,str2,strsort)

  IMPLICIT NONE

  CHARACTER(LEN = *), INTENT(in)  :: str1,str2
  CHARACTER(LEN = *), INTENT(out) :: strsort

  IF ( LGT(str1,str2) ) THEN
    strsort=str2//'_'//str1
  ELSE
    strsort=str1//'_'//str2
  ENDIF

END SUBROUTINE sort_string

SUBROUTINE compute_hilbert(hilbert,nn,rdata)
  USE, intrinsic :: iso_c_binding
  implicit none
  include 'fftw3.f03'

  INTEGER, INTENT(in) :: nn
  REAL(C_DOUBLE), DIMENSION(nn), INTENT(in) :: rdata
  COMPLEX(C_DOUBLE_COMPLEX), DIMENSION(nn), INTENT(out) :: hilbert

  INTEGER :: rank,ii
  COMPLEX(C_DOUBLE_COMPLEX), DIMENSION(:), ALLOCATABLE :: zout,ffthw
  COMPLEX(C_DOUBLE_COMPLEX), DIMENSION(:), ALLOCATABLE :: zin
  REAL(C_DOUBLE), DIMENSION(:), ALLOCATABLE :: din
  INTEGER(KIND=8) :: plan_forward,plan_backward
  REAL(C_DOUBLE), DIMENSION(:), ALLOCATABLE :: hw ! weight for the fft

!  nn=4
!  rdata(:) = (/1.d0, 2.d0, 3.d0, 4.d0/)

! Create the plan for FFT forward
  ! forward real-to-complex
  rank = 1
  ALLOCATE(din(nn),zout(nn/2+1),hw(nn/2+1),ffthw(nn/2+1))
  din(:)=0.d0
  zout(:)=(0.d0,0.d0)
  CALL dfftw_plan_dft_r2c(plan_forward, rank, nn, &
                          din, zout, FFTW_ESTIMATE)

  CALL dfftw_execute_dft_r2c(plan_forward, rdata, zout)
  CALL dfftw_destroy_plan(plan_forward)

  hw(:)=1.d0
  hw(2:nn/2)=2.d0
  hw(nn/2+1)=1.d0

  DO ii=1,nn/2+1
    ffthw(ii)=hw(ii)*zout(ii)
  ENDDO

  DEALLOCATE(zout)

  ! Create the plan for FFT backward
  ! backward complex-to-complex
  ALLOCATE(zout(nn),zin(nn))
  zout(:)=(0.d0,0.d0)
  zin(:)=0.d0
  call dfftw_plan_dft_1d(plan_backward,nn,zout,zin,FFTW_BACKWARD,FFTW_ESTIMATE)

  zout(nn/2:nn)=(0.d0,0.d0)
  zout(1:nn/2+1)=ffthw(:)

  call dfftw_execute_dft(plan_backward,zout,hilbert)
  CALL dfftw_destroy_plan(plan_backward)
  hilbert(:)=hilbert(:)/DBLE(nn)

END SUBROUTINE compute_hilbert
SUBROUTINE tukeywin(res,N,alpha)

  IMPLICIT NONE

  INTEGER, INTENT(in) :: N
  REAL(KIND=8), INTENT(in) :: alpha
  REAL(KIND=8), DIMENSION(N), INTENT(out) :: res
  REAL(KIND=8) :: pi
  INTEGER :: ii

  pi = 4.d0*DATAN(1.d0)
  DO ii=1,floor(0.5*alpha*(N-1)+1)
    res(ii)=0.5d0*(1.d0+COS(pi*(2.d0*(ii-1)/(alpha*(N-1))-1.d0)))
  ENDDO
  DO ii=ceiling(0.5*alpha*(N-1)+1),floor((N-1)*(1-0.5*alpha)+1)
    res(ii)=1.d0
  ENDDO
  DO ii=ceiling((N-1)*(1-0.5*alpha)+1),N
    res(ii)=0.5d0*(1.d0+COS(pi*(2.d0*(ii-1)/(alpha*(N-1))-2.d0/alpha+1.d0)))
  ENDDO

END SUBROUTINE tukeywin

SUBROUTINE lenteur(nl,npt,cpt_valid_corr,Fs,surech,  &
                    diff_distance,veclenteur,reseau_xcorr_trie,  &
                    reseau_maxi_signal_moyen,IIIJJJ,maxloc_reseau_maxi_env)
  IMPLICIT NONE
  INTEGER, INTENT(in) :: nl,npt,cpt_valid_corr,Fs,surech
  REAL(KIND=8), DIMENSION(cpt_valid_corr), INTENT(in) :: diff_distance
  REAL(KIND=8), DIMENSION(nl), INTENT(in) :: veclenteur
  REAL(KIND=8), DIMENSION(npt,cpt_valid_corr), INTENT(in) :: reseau_xcorr_trie
  INTEGER :: dtll,kk,ll,itmp
  REAL(KIND=8), DIMENSION(npt,nl) :: reseau_maxi  ! en fait on ne veut que le IIIJJJ2 -> signal_moyen
  REAL(KIND=8), DIMENSION(npt), INTENT(out) :: reseau_maxi_signal_moyen
  INTEGER, DIMENSION(2), INTENT(out) :: IIIJJJ
  REAL(KIND=8), DIMENSION(npt,nl) :: reseau_maxi_env ! en fait on n'en veut qu'un
  REAL(KIND=4), INTENT(out) :: maxloc_reseau_maxi_env ! en fait on n'en veut qu'un
  REAL(KIND=8), DIMENSION(npt) :: somme
  REAL(KIND=8) :: tmp
  COMPLEX(KIND=8), DIMENSION(npt) :: hilbert_reseau_maxi_kk
  itmp=Fs*surech
  DO kk=1,nl
    somme(:)=0.d0
    tmp=veclenteur(kk)*DBLE(itmp)
    DO ll=1,cpt_valid_corr
      dtll=NINT(diff_distance(ll)*veclenteur(kk)*Fs*surech)  ! preciser DBLE pr Fs et surech !!!
      somme(:)=somme(:)+CSHIFT(reseau_xcorr_trie(:,ll),dtll)   ! reseau_xcorr_trie doit etre remis a jour a chaque iteration de compteur 
    ENDDO
    reseau_maxi(:,kk)=somme(:)/cpt_valid_corr   ! preciser DBLE !!!
    CALL compute_hilbert(hilbert_reseau_maxi_kk(:),npt,reseau_maxi(:,kk))
    DO ll=1,npt
      reseau_maxi_env(ll,kk)=ABS(hilbert_reseau_maxi_kk(ll))
    ENDDO
  ENDDO
  IIIJJJ(:) = MAXLOC(reseau_maxi_env) ! elt1 : IIIpos / elt2 : JJJpos
  maxloc_reseau_maxi_env = REAL(reseau_maxi_env(IIIJJJ(1),IIIJJJ(2)))
  reseau_maxi_signal_moyen(:)=reseau_maxi(:,IIIJJJ(2))
END SUBROUTINE lenteur

SUBROUTINE extraction(nil,npt,Fs,surech,cpt_valid_corr, & 
                      IIIJJJ,index,diff_distance,maxloclenteur,signal_moyen, & 
                      reseau_xcorr_trie)
  IMPLICIT NONE
  INTEGER, INTENT(in) :: nil,npt,Fs,surech,cpt_valid_corr
  INTEGER, DIMENSION(2), INTENT(in) :: IIIJJJ
  INTEGER, DIMENSION(nil), INTENT(in) :: index
  REAL(KIND=8), INTENT(in) :: maxloclenteur
  REAL(KIND=8), DIMENSION(cpt_valid_corr), INTENT(in) :: diff_distance
  REAL(KIND=8), DIMENSION(npt), INTENT(in) :: signal_moyen
  REAL(KIND=8), DIMENSION(npt,cpt_valid_corr), INTENT(inout) :: reseau_xcorr_trie
  INTEGER, DIMENSION(nil) :: IIIpos_plus_index
  INTEGER, DIMENSION(1) :: iloc
  INTEGER :: good_len_index,ll,dtll
  INTEGER, DIMENSION(:), ALLOCATABLE :: goodindex
  INTEGER, DIMENSION(:), ALLOCATABLE :: IIIpos_plus_goodindex
  REAL(KIND=8), DIMENSION(npt) :: toto,tototmpll
  IIIpos_plus_index(:)=IIIJJJ(1)+index(:)
  IF ( MINVAL(ABS(IIIpos_plus_index(:)-1)) == 0 ) THEN
    iloc=MINLOC(ABS(IIIpos_plus_index(:)-1))
    good_len_index=nil-iloc(1)+1
    ALLOCATE(goodindex(good_len_index))
    goodindex(:)=index(iloc(1):nil)
  ELSEIF ( MINVAL(ABS(IIIpos_plus_index(:)-npt)) == 0 ) THEN
    iloc=MINLOC(ABS(IIIpos_plus_index(:)-npt))
    good_len_index=iloc(1)
    ALLOCATE(goodindex(good_len_index))
    goodindex(:)=index(1:iloc(1))
  ELSE
    good_len_index=nil
    ALLOCATE(goodindex(good_len_index))
    goodindex(:)=index(:)
  ENDIF
  ALLOCATE(IIIpos_plus_goodindex(good_len_index))
  IIIpos_plus_goodindex(:)=IIIJJJ(1)+goodindex(:)
  DEALLOCATE(goodindex)
  
  toto(:)=0.d0
  CALL tukeywin(toto(IIIpos_plus_goodindex(1):IIIpos_plus_goodindex(good_len_index)),good_len_index,0.5d0)
  DEALLOCATE(IIIpos_plus_goodindex)
  
  DO ll=1,npt
    toto(ll)=toto(ll)*signal_moyen(ll)
  ENDDO

  DO ll=1,cpt_valid_corr
    dtll=NINT(diff_distance(ll)*maxloclenteur*Fs*surech)
    tototmpll(:)=CSHIFT(toto(:),-dtll)
    reseau_xcorr_trie(:,ll)=reseau_xcorr_trie(:,ll)-tototmpll(:)
  ENDDO
END SUBROUTINE extraction



!SUBROUTINE lenteur0(nl,npt,cpt_valid_corr,Fs,surech,  &
!                     diff_distance,lenteur_pos,reseau_xcorr_trie,  &
!                     dt,reseau_maxi_pos,IIIJJJpos,reseau_maxi_pos_env)
!  IMPLICIT NONE
!  INTEGER, INTENT(in) :: nl,npt,cpt_valid_corr,Fs,surech
!  REAL(KIND=8), DIMENSION(cpt_valid_corr), INTENT(in) :: diff_distance
!  REAL(KIND=8), DIMENSION(nl), INTENT(in) :: lenteur_pos
!  REAL(KIND=8), DIMENSION(npt,cpt_valid_corr), INTENT(in) :: reseau_xcorr_trie
!  INTEGER, DIMENSION(cpt_valid_corr), INTENT(out) :: dt
!  REAL(KIND=8), DIMENSION(npt,nl), INTENT(out) :: reseau_maxi_pos  ! en fait on ne veut que le IIIJJJpos2 -> signal_moyen
!  INTEGER, DIMENSION(2), INTENT(out) :: IIIJJJpos
!  REAL(KIND=8), DIMENSION(npt,nl), INTENT(out) :: reseau_maxi_pos_env ! en fait on n'en veut qu'un
!  INTEGER :: kk,ic,ll
!!  REAL(KIND=8), DIMENSION(npt) :: somme
!  REAL(KIND=8), DIMENSION(npt,cpt_valid_corr) :: reseau_xcorr_svd_DBF_pos
!!  COMPLEX(KIND=8), DIMENSION(npt,nl) :: hilbert_reseau_maxi_pos
!!  DO kk=1,nl
!!    print *,'kk,lenteur_pos(kk)',kk,lenteur_pos(kk)
!!    DO ic=1,cpt_valid_corr
!!      print *,'ic,diff_distance(ic)',ic,diff_distance(ic)
!!print *,kk,ic,diff_distance(ic) ! ,lenteur_pos(kk),Fs,surech
!!      dt(ic)=NINT(diff_distance(ic)*lenteur_pos(kk)*Fs*surech)  ! voir si res plus proche de matlab avec dble(Fs) et dble(surech) ?
!!    ENDDO
!!print *,shape(reseau_xcorr_svd_DBF_pos)
!!print *,shape(reseau_xcorr_trie)
!!print *,shape(dt)
!!    DO ll=1,cpt_valid_corr
!!      reseau_xcorr_svd_DBF_pos(:,ll)=CSHIFT(reseau_xcorr_trie(:,ll),dt(ll))  ! reseau_xcorr_trie doit etre remis a jour a chaque iteration de compteur
!!    ENDDO
!!    somme(:)=0.d0
!!    DO ll=1,cpt_valid_corr
!!      somme(:)=somme(:)+reseau_xcorr_svd_DBF_pos(:,ll)
!!    ENDDO
!!    reseau_maxi_pos(:,kk)=somme(:)/cpt_valid_corr
!!  ENDDO
!!  DO kk=1,nl
!!    CALL compute_hilbert(hilbert_reseau_maxi_pos(:,kk),npt,reseau_maxi_pos(:,kk))
!!    DO ll=1,npt
!!      reseau_maxi_pos_env(ll,kk)=ABS(hilbert_reseau_maxi_pos(ll,kk))
!!    ENDDO
!!  ENDDO
!!  IIIJJJpos(:) = MAXLOC(reseau_maxi_pos_env) ! elt1 : IIIpos / elt2 : JJJpos
!END SUBROUTINE lenteur0
!
!SUBROUTINE read_one_ctrace(file_id,dsetname,offset,count,nb,buf)
!!SUBROUTINE read_one_ctrace(file_id,dataset) ! ,offset,count,nb,buf)
!  USE HDF5
!  USE ISO_C_BINDING
!  IMPLICIT NONE
!  INTEGER(HID_T), INTENT(in)     :: file_id
!  CHARACTER(LEN=*), INTENT(in)   :: dsetname
!  INTEGER, INTENT(in)            :: nb
!  INTEGER(HSIZE_T), DIMENSION(2), INTENT(in) :: offset
!  INTEGER(HSIZE_T), DIMENSION(2), INTENT(in) :: count
!  CHARACTER(LEN=*), INTENT(out) :: buf
!  INTEGER(HID_T) :: dset_id,space_id,memspace_id
!  INTEGER                        :: error
!  INTEGER(HSIZE_T), DIMENSION(1) :: memcount
!  INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: memoffset = (/0/)
!  INTEGER(HSIZE_T), DIMENSION(1) :: indims
!  INTEGER(HID_T)  ::  filetype,memtype
!  INTEGER(SIZE_T) :: size
!  INTEGER(SIZE_T)  , PARAMETER :: sdim      = 5
!!  CHARACTER(LEN=sdim), TARGET :: rdata
!!  TYPE(C_PTR) :: f_ptr
!
!  memcount=(/nb/)
!  indims = (/nb/)
!
!  buf='CCCCC'
!
!  CALL h5dopen_f(file_id, dsetname, dset_id, error)
!  CALL h5dget_space_f(dset_id, space_id, error)
!  CALL h5sselect_hyperslab_f (space_id, H5S_SELECT_SET_F, offset, count, error)
!  CALL h5screate_simple_f(1, memcount, memspace_id, error)
!  CALL h5sselect_hyperslab_f(memspace_id, H5S_SELECT_SET_F, memoffset, memcount, error)
!
!  ! Get the datatype and its size.
!  CALL H5Dget_type_f(dset_id, filetype, error)
!  CALL H5Tget_size_f(filetype, size, error)
!print *,'filetype,size',filetype,size
!
!  ! Create the memory datatype.
!  CALL H5Tcopy_f(H5T_NATIVE_CHARACTER, memtype, error)
!  CALL H5Tset_size_f(memtype, sdim, error)
!print *,'memtype,sdim',memtype, sdim
!
!!  f_ptr = C_LOC(rdata(1:1))
!  CALL H5Dread_f(dset_id, memtype, buf, indims, error)
!!  CALL H5dread_f(dset_id, H5T_STRING_F, buf, indims, error, memspace_id, space_id)
!print *,'error:',error
!print *,'buf:',buf
!  CALL h5sclose_f(space_id, error)
!  CALL h5sclose_f(memspace_id, error)
!  CALL h5dclose_f(dset_id, error)
!
!
!
!
!!  INTEGER(HID_T), INTENT(in)     :: file_id
!!  CHARACTER(LEN=*), INTENT(in)   :: dataset
!!!  CHARACTER(LEN=12), PARAMETER :: filename  = "reseau_64.h5"
!!!  CHARACTER(LEN=10) , PARAMETER :: dataset   = "reseauname"
!!!  INTEGER          , PARAMETER :: dim0      = 1108, dim1 = 64
!!  INTEGER          , PARAMETER :: dim0      = 64, dim1 = 1108
!!  INTEGER(SIZE_T)  , PARAMETER :: sdim      = 6
!!
!!  INTEGER(HID_T)  :: file, filetype, memtype, space, dset ! Handles
!!  INTEGER :: hdferr
!!
!!  INTEGER(HSIZE_T), DIMENSION(1:2) :: dims = (/dim0,dim1/)
!!  INTEGER(HSIZE_T), DIMENSION(1:2) :: maxdims
!!
!!!  CHARACTER(LEN=sdim), DIMENSION(1:dim0), TARGET :: &
!!!       wdata = (/"Parting", "is such", "sweet  ", "sorrow."/)
!!  CHARACTER(LEN=sdim), DIMENSION(:,:), ALLOCATABLE, TARGET :: rdata
!!  INTEGER :: i, j, len
!!  INTEGER(SIZE_T) :: size
!!  TYPE(C_PTR) :: f_ptr
!!  !
!!!  ! Initialize FORTRAN interface.
!!!  !
!! ! CALL h5open_f(hdferr)
!!  ! Open file and dataset.
!!  !
!!!  CALL h5fopen_f(filename, H5F_ACC_RDONLY_F, file, hdferr)
!!  CALL h5dopen_f(file_id, dataset, dset, hdferr)
!!  !
!!  ! Get the datatype and its size.
!!  !
!!  CALL H5Dget_type_f(dset, filetype, hdferr)
!!  CALL H5Tget_size_f(filetype, size, hdferr)
!!
!!print *,'size',size
!!print *,'sdim',sdim
!!
!!  ! Make sure the declared length is large enough
!!  IF(size.GT.sdim)THEN
!!     PRINT*,'ERROR: Character LEN is to small'
!!     STOP
!!  ENDIF
!!  !
!!  ! Get dataspace.
!!  !
!!  CALL H5Dget_space_f(dset, space, hdferr)
!!  CALL H5Sget_simple_extent_dims_f(space, dims, maxdims, hdferr)
!!
!!  ALLOCATE(rdata(1:dims(1),1:dims(2)))
!!  !
!!  ! Create the memory datatype.
!!  !
!!  CALL H5Tcopy_f(H5T_FORTRAN_S1, memtype, hdferr)
!!  CALL H5Tset_size_f(memtype, sdim, hdferr)
!!  !
!!  ! Read the data.
!!  !
!!  f_ptr = C_LOC(rdata(1,1)(1:1))
!!  CALL H5Dread_f(dset, memtype, f_ptr, hdferr, space)
!!  !
!!  ! Output the data to the screen.
!!  !
!!  DO i = 1, dims(1)
!!     DO j = 1, dims(2)
!!     WRITE(*,'(A,"(",I0,"): ", A)') DATASET, i, TRIM(rdata(i,j))
!!     ENDDO
!!  END DO
!!  !
!!  ! Close and release resources.
!!  !
!!  CALL H5Dclose_f(dset, hdferr)
!!  CALL H5Sclose_f(space, hdferr)
!!  CALL H5Tclose_f(memtype, hdferr)
!!
!!  DEALLOCATE(rdata)
!!
!!
!END SUBROUTINE read_one_ctrace

END MODULE dbfmodule
