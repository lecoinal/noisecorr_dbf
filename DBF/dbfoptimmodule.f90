!###############################################################################
!#    This file is part of noisecorr_dbf : a Fortran code for computing        #
!#    double beamforming on the dataset from San Jacinto Fault zone            #
!#    dense array of geophones                                                 #
!#                                                                             #
!#    Copyright (C) 2015 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################
!
! https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Double_beamforming

MODULE dbfoptimmodule

  IMPLICIT NONE

  CONTAINS

REAL(KIND=8) FUNCTION beamforming_slowness(xx)
  use glovarmodule , only : reseau_xcorr_svd,distance_elements_1d,distance,nn_Fs,nn_taille_reseau2,nn_npt,lisnan
  use dbfmodule
  implicit none
  REAL(KIND=8) :: xx,somme
  REAL(KIND=8), DIMENSION(nn_npt,nn_taille_reseau2) :: reseau_xcorr_svd_DBF_pos
  REAL(KIND=8), DIMENSION(nn_npt) :: reseau_maxi_pos
  COMPLEX(KIND=8), DIMENSION(nn_npt) :: hilbert_reseau_maxi_pos
  INTEGER :: dtll,ll,ic,compteur

  DO ll=1,nn_taille_reseau2
    dtll = NINT( (distance_elements_1d(ll)-distance) * xx * DBLE(nn_Fs) )
    reseau_xcorr_svd_DBF_pos(:,ll) = CSHIFT(reseau_xcorr_svd(:,ll),dtll)
  ENDDO
  DO ic=1,nn_npt
    somme=0.d0
    compteur=0
    DO ll=1,nn_taille_reseau2
      IF ( .NOT. lisnan(ll) ) THEN
        somme=somme+reseau_xcorr_svd_DBF_pos(ic,ll)
        compteur=compteur+1
      ENDIF
    ENDDO
    reseau_maxi_pos(ic) = somme/DBLE(compteur)
  ENDDO
  CALL compute_hilbert(hilbert_reseau_maxi_pos(:),nn_npt,reseau_maxi_pos(:))
  DO ll=1,nn_npt
    reseau_maxi_pos(ll)=ABS(hilbert_reseau_maxi_pos(ll))
  ENDDO
  beamforming_slowness = 1.d0 - MAXVAL(reseau_maxi_pos)
  return
end FUNCTION beamforming_slowness

FUNCTION beamforming_signal(xx) RESULT(signal_moyen)
  use glovarmodule , only : reseau_xcorr_svd,distance_elements_1d,distance,nn_Fs,nn_taille_reseau2,nn_npt,lisnan
  IMPLICIT NONE
  REAL(KIND=8) :: xx,somme
  REAL(KIND=8), DIMENSION(nn_npt,nn_taille_reseau2) :: reseau_xcorr_svd_DBF_pos
  REAL(KIND=8), DIMENSION(nn_npt) :: signal_moyen
  INTEGER :: ll,dtll,compteur,ic

  DO ll=1,nn_taille_reseau2
    dtll = NINT( (distance_elements_1d(ll)-distance) * xx * DBLE(nn_Fs) )
    reseau_xcorr_svd_DBF_pos(:,ll) = CSHIFT(reseau_xcorr_svd(:,ll),dtll)
  ENDDO
  DO ic=1,nn_npt
    somme=0.d0
    compteur=0
    DO ll=1,nn_taille_reseau2
      IF ( .NOT. lisnan(ll) ) THEN
        somme=somme+reseau_xcorr_svd_DBF_pos(ic,ll)
        compteur=compteur+1
      ENDIF
    ENDDO
    signal_moyen(ic) = somme/DBLE(compteur)
  ENDDO
END FUNCTION beamforming_signal

REAL(KIND=8) FUNCTION beamforming_azimuth_slowness(xxx)
!REAL FUNCTION beamforming_azimuth_slowness(xxx)
  use glovarmodule , only : pi_over_180,reseau_xcorr_svd,position1,position2,nn_taille_reseau,nn_taille_reseau2,nn_Fs,nn_npt,lisnan 
  use dbfmodule
  implicit none
  REAL(KIND=8), DIMENSION(2) :: xxx
  REAL(KIND=8), DIMENSION(nn_taille_reseau) :: dt1,dt2
  REAL(KIND=8), DIMENSION(nn_npt) :: reseau_maxi_DBF,reseau_maxi_DBF_env
  COMPLEX(KIND=8), DIMENSION(nn_npt) :: hilbert_reseau_maxi_DBF
  INTEGER, DIMENSION(nn_taille_reseau2) :: dtdif
  INTEGER :: ll,pp,ic,compteur
  DO ll=1,nn_taille_reseau 
    dt1(ll) = xxx(2) * ( position1(ll,1)*SIN(pi_over_180 * xxx(1))+position1(ll,2)*COS(pi_over_180 * xxx(1)) ) * DBLE(nn_Fs)
    dt2(ll) = xxx(2) * ( position2(ll,1)*SIN(pi_over_180 * xxx(1))+position2(ll,2)*COS(pi_over_180 * xxx(1)) ) * DBLE(nn_Fs)
  ENDDO
  DO pp=1,nn_taille_reseau
    DO ll=1,nn_taille_reseau
      dtdif((pp-1)*nn_taille_reseau+ll) = NINT(dt1(ll)-dt2(pp))
    ENDDO
  ENDDO
  reseau_maxi_DBF(:)=0.d0
  compteur=0
  DO pp=1,nn_taille_reseau2
    IF ( .NOT. lisnan(pp) ) THEN
      reseau_maxi_DBF(:) = reseau_maxi_DBF(:) + CSHIFT( reseau_xcorr_svd(:,pp) , dtdif(pp) )
      compteur=compteur+1
    ENDIF
  ENDDO
  DO ic=1,nn_npt
    reseau_maxi_DBF(ic) = reseau_maxi_DBF(ic) / DBLE(compteur)
  ENDDO
  CALL compute_hilbert(hilbert_reseau_maxi_DBF(:),nn_npt,reseau_maxi_DBF(:))
  DO ic=1,nn_npt
    reseau_maxi_DBF_env(ic)=ABS(hilbert_reseau_maxi_DBF(ic))
  ENDDO
  beamforming_azimuth_slowness = 1.d0 - MAXVAL(reseau_maxi_DBF_env)
return
END FUNCTION beamforming_azimuth_slowness


FUNCTION beamforming_signal_slowness_azimuth(xxx) RESULT(signal_moyen)
  use glovarmodule , only : pi_over_180,reseau_xcorr_svd,position1,position2,nn_taille_reseau,nn_taille_reseau2,nn_Fs,nn_npt,lisnan 
  IMPLICIT NONE
  REAL(KIND=8), DIMENSION(2) :: xxx
  REAL(KIND=8), DIMENSION(nn_taille_reseau) :: dt1,dt2
  REAL(KIND=8), DIMENSION(nn_npt) :: signal_moyen
  INTEGER, DIMENSION(nn_taille_reseau2) :: dtdif
  INTEGER :: ll,pp,ic,compteur
  DO ll=1,nn_taille_reseau 
    dt1(ll) = xxx(2) * ( position1(ll,1)*SIN(pi_over_180 * xxx(1))+position1(ll,2)*COS(pi_over_180 * xxx(1)) ) * DBLE(nn_Fs)
    dt2(ll) = xxx(2) * ( position2(ll,1)*SIN(pi_over_180 * xxx(1))+position2(ll,2)*COS(pi_over_180 * xxx(1)) ) * DBLE(nn_Fs)
  ENDDO
  DO pp=1,nn_taille_reseau
    DO ll=1,nn_taille_reseau
      dtdif((pp-1)*nn_taille_reseau+ll) = NINT(dt1(ll)-dt2(pp))
    ENDDO
  ENDDO
  signal_moyen(:)=0.d0
  compteur=0
  DO pp=1,nn_taille_reseau2
    IF ( .NOT. lisnan(pp) ) THEN
      signal_moyen(:) = signal_moyen(:) + CSHIFT(reseau_xcorr_svd(:,pp),dtdif(pp))
      compteur=compteur+1
    ENDIF
  ENDDO
  signal_moyen(:) = signal_moyen(:) / DBLE(compteur)
END FUNCTION beamforming_signal_slowness_azimuth

REAL(KIND=8) FUNCTION beamforming_amplitude(xxx)
  use glovarmodule , only : reseau_xcorr_svd_trie,toto_svd_trie,N,nn_taille_reseau2,nn_npt,lisnan_trie,llpowtestm1
  implicit none
  REAL(KIND=8), DIMENSION(N) :: xxx
  INTEGER :: test,ll,it
  REAL(KIND=8) :: amplitudell
  beamforming_amplitude = 0.d0
  DO ll=1,nn_taille_reseau2
    IF ( .NOT. lisnan_trie(ll) ) THEN
      amplitudell = 1.d0
      DO test=1,N
        amplitudell=amplitudell+xxx(test)*llpowtestm1(test,ll)
      ENDDO
      DO it=1,nn_npt
        beamforming_amplitude = beamforming_amplitude + &
                ( reseau_xcorr_svd_trie(it,ll) - (toto_svd_trie(it,ll) * amplitudell) )**2.d0
      ENDDO
    ENDIF
  ENDDO
  return
END FUNCTION beamforming_amplitude

SUBROUTINE prepare_llpowtestm1(N)
  use glovarmodule , only : nn_taille_reseau2,llpowtestm1
  IMPLICIT NONE
  INTEGER, INTENT(in) :: N
  INTEGER :: ll,it
  DO ll=1,nn_taille_reseau2
    DO it=1,N
      llpowtestm1(it,ll) = ll**(it-1)
    ENDDO
  ENDDO
END SUBROUTINE prepare_llpowtestm1

FUNCTION beamforming_signal_amplitude(xxx) RESULT(valeur)
  use glovarmodule , only : toto_svd_trie,N,nn_taille_reseau2,nn_npt,lisnan_trie,llpowtestm1
  IMPLICIT NONE
  INTEGER :: ll,it
  REAL(KIND=8) :: amplitudell
  REAL(KIND=8), DIMENSION(N) :: xxx
  REAL(KIND=8), DIMENSION(nn_npt,nn_taille_reseau2) :: valeur
  DO ll=1,nn_taille_reseau2
    IF ( .NOT. lisnan_trie(ll) ) THEN
      amplitudell = 1.d0
      DO it=1,N
        amplitudell=amplitudell+xxx(it)*llpowtestm1(it,ll)
      ENDDO
      DO it=1,nn_npt
        valeur(it,ll) = toto_svd_trie(it,ll) * amplitudell
      ENDDO
    ENDIF
  ENDDO
END FUNCTION beamforming_signal_amplitude

SUBROUTINE prepare_x_ampl_powicoeffm1(x_ampl_powicoeffm1)
  use glovarmodule , only : nn_taille_reseau2,Ncoeffs
  use dbfmodule
  IMPLICIT NONE
  REAL(KIND=8), DIMENSION(Ncoeffs,nn_taille_reseau2), INTENT(out) :: x_ampl_powicoeffm1
  REAL(KIND=8), DIMENSION(nn_taille_reseau2) :: x_ampl
  INTEGER :: icoeff,ia
  CALL linspace(x_ampl,0.d0,1.d0,nn_taille_reseau2)
  DO ia = 1,nn_taille_reseau2
    DO icoeff = 1,Ncoeffs
      x_ampl_powicoeffm1(icoeff,ia) = (x_ampl(ia))**DBLE(icoeff-1)
    ENDDO
  ENDDO
END SUBROUTINE prepare_x_ampl_powicoeffm1

SUBROUTINE generate_MaxMincoeffs()
  use glovarmodule , only : Ncoeffs,Max_coeffs,Min_coeffs
  IMPLICIT NONE
  Max_coeffs(:)=3
  Min_coeffs(:)=-3
END SUBROUTINE generate_MaxMincoeffs

SUBROUTINE prepare_deltacoeff_Tcoeffs(deltacoeff_Tcoeffs)
  use glovarmodule , only : Ncoeffs,NSA,Max_coeffs,Min_coeffs
  use dbfmodule
  IMPLICIT NONE
  REAL(KIND=8), DIMENSION(NSA) :: T_coeffs
  REAL(KIND=8), PARAMETER :: x_start = 0.5d0 , x_end = 1.d0/30.d0
  REAL(KIND=8), DIMENSION(Ncoeffs,NSA), INTENT(out) :: deltacoeff_Tcoeffs
  INTEGER :: isa
  CALL linspace(T_coeffs,x_start,x_end,NSA)
  DO isa=1,NSA
    deltacoeff_Tcoeffs(:,isa) = DBLE(Max_coeffs(:) - Min_coeffs(:))*T_coeffs(isa)
  ENDDO 
END SUBROUTINE prepare_deltacoeff_Tcoeffs

SUBROUTINE fn_optimize_amplitude_new(reseau_xcorr,wavelet,x_ampl_powicoeffm1,deltacoeff_Tcoeffs)
  use glovarmodule , only : nn_npt,nn_taille_reseau2,lisnan_trie,Ncoeffs,NSA,Max_coeffs,Min_coeffs
  use dbfmodule
  IMPLICIT NONE
  REAL(KIND=8), DIMENSION(nn_npt,nn_taille_reseau2), INTENT(in) :: reseau_xcorr
  REAL(KIND=8), DIMENSION(nn_npt,nn_taille_reseau2), INTENT(inout) :: wavelet
  REAL(KIND=8), DIMENSION(Ncoeffs,nn_taille_reseau2), INTENT(in) :: x_ampl_powicoeffm1
  REAL(KIND=8), DIMENSION(Ncoeffs,NSA) :: deltacoeff_Tcoeffs
  REAL(KIND=8), DIMENSION(nn_npt,nn_taille_reseau2) :: copy_reseau_xcorr
  INTEGER :: i_restart,isaplot,ia,icoeff,it,isa,idx,iisa
  REAL(KIND=8), DIMENSION(NSA) :: T_SA,likelihood_SA_ampl,RMS_SA_ampl
  LOGICAL, DIMENSION(NSA) :: RMS_SA_ampl_mask
  INTEGER, DIMENSION(Ncoeffs,NSA) :: coeff
  INTEGER, DIMENSION(Ncoeffs) :: coeff_cand
  REAL(KIND=8),DIMENSION(Ncoeffs) :: Coeffs_true
  REAL(KIND=8) :: amplitudeia,rand,RMS_cand,likelihood_cand,test_likelihood,somme,min_RMS_SA_ampl,T0,Tmin

  ! optimizing wavelet amplitude
  RMS_SA_ampl_mask(:)=.false.
  coeff(1:Ncoeffs,2:NSA)=0
  i_restart = 500
  isaplot = 1000

  coeff(1,1) = 1
  coeff(2:Ncoeffs,1) = 0

! inutile de passer par une copy, masker reseau_xcorr_svd_trie en amont dans le
! main !!!
  DO ia=1,nn_taille_reseau2
    IF ( lisnan_trie(ia) ) THEN
      copy_reseau_xcorr(:,ia)=0.d0
    ELSE
      copy_reseau_xcorr(:,ia)=reseau_xcorr(:,ia)
    ENDIF
  ENDDO
  somme=0.d0
  DO ia = 1,nn_taille_reseau2
    amplitudeia=0.d0
!!!DIR$ SIMD
    DO icoeff = 1,Ncoeffs
      amplitudeia = amplitudeia + coeff(icoeff,1) * x_ampl_powicoeffm1(icoeff,ia)
    ENDDO
!!!DIR$ SIMD
    DO it=1,nn_npt
      somme = somme + ( ( copy_reseau_xcorr(it,ia) - amplitudeia * wavelet(it,ia) )**2.d0 )
    ENDDO
  ENDDO
  RMS_SA_ampl_mask(1) = .true.
  RMS_SA_ampl(1) = somme**0.25d0
  T0 = 0.1d0 * RMS_SA_ampl(1)
  Tmin = RMS_SA_ampl(1)/35.d0
  CALL linspace(T_SA,log(T0),log(Tmin),NSA)
  DO isa=1,NSA
    T_SA(isa) = 1.d0 / ( 2.d0 * EXP(2.d0*T_SA(isa)) )
  ENDDO

  likelihood_SA_ampl(1) = EXP( (-RMS_SA_ampl(1)**2.d0) * T_SA(1) )

  DO isa=2,NSA
    IF ( MODULO(isa,i_restart) .EQ. 0 ) THEN
      min_RMS_SA_ampl = MINVAL(RMS_SA_ampl(1:NSA-1),mask=RMS_SA_ampl_mask(1:NSA-1))
      idx=0
      iisa=NSA
      DO WHILE ( idx==0 ) ! iisa=NSA,1,-1
        IF ( RMS_SA_ampl(iisa) == min_RMS_SA_ampl ) idx=iisa
        iisa=iisa-1
      ENDDO
      likelihood_SA_ampl(isa) = EXP( (-RMS_SA_ampl(idx)**2.d0) * T_SA(isa) )
      coeff(:,isa) = coeff(:,idx)
      RMS_SA_ampl(isa) = RMS_SA_ampl(idx)
      RMS_SA_ampl_mask(isa) = RMS_SA_ampl_mask(idx)
    ELSE
      coeff_cand(:) = generate_params(Ncoeffs,coeff(:,isa-1),deltacoeff_Tcoeffs(:,isa),Max_coeffs(:),Min_coeffs(:))
      somme=0.d0
      DO ia = 1,nn_taille_reseau2
        amplitudeia=0.d0
!!!DIR$ SIMD
        DO icoeff = 1,Ncoeffs
          amplitudeia = amplitudeia + coeff_cand(icoeff) * x_ampl_powicoeffm1(icoeff,ia)
        ENDDO
!!!DIR$ SIMD
        DO it=1,nn_npt
          somme = somme + ( ( copy_reseau_xcorr(it,ia) - amplitudeia * wavelet(it,ia) )**2.d0 )
        ENDDO
      ENDDO
      RMS_cand = somme**0.25d0
      likelihood_cand = EXP( (-RMS_cand**2.d0) * T_SA(isa) ) 
      test_likelihood = MIN( 1.d0 , likelihood_cand/likelihood_SA_ampl(isa-1) )
      CALL RANDOM_NUMBER(rand)
      IF ( rand .LT. test_likelihood ) THEN
        likelihood_SA_ampl(isa) = likelihood_cand
        coeff(:,isa) = coeff_cand(:)
        RMS_SA_ampl(isa) = RMS_cand
        RMS_SA_ampl_mask(isa) = .true.
      ELSE
        coeff(:,isa) = coeff(:,isa-1)
        RMS_SA_ampl(isa) = RMS_SA_ampl(isa-1)
        RMS_SA_ampl_mask(isa) = RMS_SA_ampl_mask(isa-1)
        likelihood_SA_ampl(isa) = EXP( (-RMS_SA_ampl(isa)**2.d0) * T_SA(isa) )
      ENDIF
    ENDIF 
  ENDDO
  min_RMS_SA_ampl = MINVAL(RMS_SA_ampl(1:NSA-1),mask=RMS_SA_ampl_mask(1:NSA-1))
  idx=0
  iisa=NSA
  DO WHILE ( idx==0 ) ! iisa=NSA,1,-1
    IF ( (RMS_SA_ampl(iisa) == min_RMS_SA_ampl) .AND. (likelihood_SA_ampl(iisa) .NE. 0.d0) ) idx=iisa
    iisa=iisa-1
  ENDDO
  Coeffs_true(:) = coeff(:,idx)
 
  DO ia = 1,nn_taille_reseau2
    amplitudeia=0.d0
!!!DIR$ SIMD
    DO icoeff = 1,Ncoeffs
      amplitudeia = amplitudeia + Coeffs_true(icoeff) * x_ampl_powicoeffm1(icoeff,ia)
    ENDDO
    wavelet(:,ia) = amplitudeia * wavelet(:,ia)
  ENDDO
END SUBROUTINE fn_optimize_amplitude_new

FUNCTION generate_params(Nc,X,delta_candidate,MAX_candidate,MIN_candidate) RESULT(candidate)
  IMPLICIT NONE
  INTEGER :: Nc
  INTEGER, DIMENSION(Nc) :: X,MAX_candidate,MIN_candidate
  REAL(KIND=8), DIMENSION(Nc) :: delta_candidate,candidate
  INTEGER :: kk
  REAL(KIND=8) :: rand,maxi,mini
  DO kk=1,Nc
    maxi = DBLE(MIN(DBLE(X(kk)) + delta_candidate(kk) , DBLE(MAX_candidate(kk))))
    mini = DBLE(MAX(DBLE(X(kk)) - delta_candidate(kk) , DBLE(MIN_candidate(kk))))
    CALL RANDOM_NUMBER(rand)
    candidate(kk) = mini + (maxi-mini)*rand
  ENDDO
END FUNCTION generate_params

SUBROUTINE hanning_window(w,n) 
  IMPLICIT NONE
  INTEGER, INTENT(in) :: n
  REAL(KIND=8), DIMENSION(n), INTENT(out) :: w
  REAL(KIND=8) :: pin
  INTEGER :: i
  pin=8.d0*ATAN(1.d0)/DBLE(n-1) 
  DO i=1,n
    w(i)=0.5d0*(1.d0-COS(pin*DBLE(i-1))) 
  ENDDO
END SUBROUTINE hanning_window

END MODULE dbfoptimmodule
