!###############################################################################
!#    This file is part of noisecorr_dbf : a Fortran code for computing        #
!#    double beamforming on the dataset from San Jacinto Fault zone            #
!#    dense array of geophones                                                 #
!#                                                                             #
!#    Copyright (C) 2015 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################
!
! https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Double_beamforming

PROGRAM DBFOPTIM_UNO
  USE HDF5
  USE H5LT      ! Lite Interface HDF5
  USE m_mrgrnk  ! Sorting and Ranking ORDERPACK lib (licence ?)
  USE dbfmodule
  USE dbfoptimmodule
  USE glovarmodule
  IMPLICIT NONE

  REAL(KIND=8) :: Freq_centrale
  REAL(KIND=8), DIMENSION(2) :: Freq_interval
  CHARACTER(LEN=256) :: cn_freq_name 
  REAL(KIND=8), DIMENSION(2) :: interval_distance
  REAL(KIND=8) :: angle_main,interval_angle
  INTEGER :: nn_ibeg,nn_iend,nn_istp,nn_jend,nn_jstp,nn_threshold,nn_max_compte
  REAL(KIND=8) :: dn_maxi_compteur,dn_maxi_value
  REAL(KIND=8) :: dn_Lb,dn_Ub,dn_reqmin
  REAL(KIND=8) :: dn_istep
  INTEGER :: nn_konvge,nn_kcount

  REAL(KIND=8) :: d_inv_nn_taille_reseau

  REAL(KIND=8), DIMENSION(4) :: step4

  REAL(KIND=8), DIMENSION(2) :: bandwidth
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: time_corr
  INTEGER :: ii,jj,kk,ll,ic
  INTEGER :: error
  INTEGER(HID_T) :: file_id,infile_id      ! Input File identifier
  INTEGER(HSIZE_T), DIMENSION(1) :: startin1
  INTEGER(HSIZE_T), DIMENSION(1) :: countin1
  INTEGER(HSIZE_T), DIMENSION(3) :: startin3
  INTEGER(HSIZE_T), DIMENSION(3) :: countin3
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: xx,yy,reseau1xx,reseau1yy,reseau2xx,reseau2yy
  CHARACTER(len=5), DIMENSION(:), ALLOCATABLE :: reseau1name,reseau2name
  REAL(KIND=8),DIMENSION(:), ALLOCATABLE :: distance1,AF1,distance2,AF2
  REAL(KIND=8), DIMENSION(1) :: pos_center_lon1,pos_center_lat1,pos_center_lon2,pos_center_lat2
  CHARACTER(len=5), DIMENSION(:), ALLOCATABLE :: name
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: distance_elements
  integer(HSIZE_T), dimension(1) :: dims ! size of the corr_sp buffer
  integer(HSIZE_T), dimension(2) :: dims2
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: r12,r21
  INTEGER, DIMENSION(:), ALLOCATABLE :: B,JJ1,JJ2,idx_sort
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: toto1tmp
  INTEGER :: compteur,compte
  INTEGER, DIMENSION(1) :: tmparr
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: signal_moyen,signal_moyen_env,signal_moyen1
  COMPLEX(KIND=8), DIMENSION(:), ALLOCATABLE :: hilbert_signal_moyen
  INTEGER :: index_length
  REAL(KIND=8), DIMENSION(1) :: JJJ
  INTEGER, DIMENSION(1) :: III
  INTEGER, DIMENSION(:), ALLOCATABLE :: index
  REAL(KIND=8) :: AF
  REAL(KIND=4) :: t0,t1
  INTEGER, DIMENSION(8) :: values
  LOGICAL :: l_exist_namelist

  REAL(KIND=8) :: maxabs
  REAL(KIND=8) :: eps,t,fx,x_x,maxi_value
  real ( kind = 8 ) :: local_min,ynewlo
  REAL(KIND=8), DIMENSION(4) :: start4,xxxamp
  INTEGER :: icount, numres, ifault
  INTEGER, DIMENSION(:), ALLOCATABLE :: III_plus_index ! [2*index_length+1]
  INTEGER, DIMENSION(1) :: iloc
  INTEGER :: good_len_index
  INTEGER, DIMENSION(:), ALLOCATABLE :: goodindex
  INTEGER, DIMENSION(:), ALLOCATABLE :: III_plus_goodindex
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: toto
  REAL(KIND=8) :: dtll
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE  :: toto_svd
  REAL(KIND=8) :: energie1
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: hanningwindow
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: x_ampl_powicoeffm1

  ! Outputs
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: temps_extrait,vitesse_extrait,distance_reseaux
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: maximum_extrait,lenteur_extrait,energie_extrait
  REAL(KIND=4), DIMENSION(:,:), ALLOCATABLE :: position_reseaux

  ! Declare NAMELIST
  NAMELIST /naminput/ Freq_centrale,nn_Fs,nn_npt,cn_freq_name,Freq_interval
  NAMELIST /namparam/ nn_taille_reseau,angle_main,interval_angle,interval_distance
  NAMELIST /namloop/ nn_ibeg,nn_iend,nn_istp,nn_jend,nn_jstp,dn_maxi_compteur,dn_maxi_value,nn_threshold,nn_max_compte
  NAMELIST /namlocmin/ dn_Lb,dn_Ub,dn_reqmin,dn_istep,nn_konvge,nn_kcount

  ! Read namelist
  INQUIRE(file='namelist', exist=l_exist_namelist)
  IF (.NOT. l_exist_namelist) THEN
     PRINT *,'The namelist does not exist'
     STOP
  ENDIF
  OPEN(11,file='namelist')
    REWIND 11
    READ(11,naminput)
    REWIND 11
    READ(11,namparam)
    REWIND 11
    READ(11,namloop)
    REWIND 11
    READ(11,namlocmin)
  CLOSE(11)

  nn_taille_reseau2=nn_taille_reseau*nn_taille_reseau
  d_inv_nn_taille_reseau = 1.d0 / DBLE(nn_taille_reseau)

  ALLOCATE(time_corr(nn_npt))
  time_corr = (/((DBLE(ii-201)/DBLE(nn_Fs)),ii=1,nn_npt)/)

  bandwidth=Freq_interval

  compte=1
          
  eps = 10.0D+00 * sqrt ( epsilon ( eps ) )
  t = eps
  N=4 ! the number of variables of the function for nelmin minimisation
  ALLOCATE(llpowtestm1(N,nn_taille_reseau2))
  CALL prepare_llpowtestm1(N)

  CALL h5open_f(error)

  ALLOCATE(signal_moyen(nn_npt),hilbert_signal_moyen(nn_npt),signal_moyen_env(nn_npt))
  ALLOCATE(signal_moyen1(nn_npt))
  index_length=NINT(DBLE(nn_Fs)/DBLE((bandwidth(2)-bandwidth(1))))
  ALLOCATE(index(2*index_length+1))
  ALLOCATE(III_plus_index(2*index_length+1))
  index = (/( ii , ii=-index_length,index_length )/)
  ALLOCATE(reseau1xx(nn_taille_reseau),reseau1yy(nn_taille_reseau),reseau1name(nn_taille_reseau))
  ALLOCATE(reseau2xx(nn_taille_reseau),reseau2yy(nn_taille_reseau),reseau2name(nn_taille_reseau))
  ALLOCATE(xx(1108),yy(1108),name(1108))
  ALLOCATE(distance1(1108),AF1(1108),JJ1(1108),position1(nn_taille_reseau,2))
  ALLOCATE(distance2(1108),AF2(1108),JJ2(1108),position2(nn_taille_reseau,2))
  ALLOCATE(r12(nn_npt),r21(nn_npt))
  ALLOCATE(distance_elements(nn_taille_reseau,nn_taille_reseau))
  ALLOCATE(distance_elements_1d(nn_taille_reseau2))
  ALLOCATE(idx_sort(nn_taille_reseau2),B(nn_taille_reseau2))
  ALLOCATE(reseau_xcorr_svd(nn_npt,nn_taille_reseau2))
  ALLOCATE(reseau_xcorr_svd_trie(nn_npt,nn_taille_reseau2))
  ALLOCATE(toto1tmp(nn_npt,nn_taille_reseau2))
  ALLOCATE(lisnan(nn_taille_reseau2))
  ALLOCATE(lisnan_trie(nn_taille_reseau2))
  ALLOCATE(toto(nn_npt),toto_svd(nn_npt,nn_taille_reseau2),toto_svd_trie(nn_npt,nn_taille_reseau2))
  ALLOCATE(hanningwindow(nn_npt))
  CALL hanning_window(hanningwindow,nn_npt)
  DO ic=1,nn_npt
    hanningwindow(ic)=hanningwindow(ic)**0.05d0
  ENDDO
  ALLOCATE(temps_extrait(nn_max_compte),vitesse_extrait(nn_max_compte),distance_reseaux(nn_max_compte), &
           maximum_extrait(nn_max_compte),lenteur_extrait(nn_max_compte),energie_extrait(nn_max_compte))
  ALLOCATE(position_reseaux(4,nn_max_compte))

  CALL h5fopen_f("reseau_64.h5", H5F_ACC_RDONLY_F, infile_id, error)
  startin1=(/0/)
  countin1=(/1108/)
  CALL read_one_trace(infile_id,'x',1,startin1,countin1,1108,xx)
  CALL read_one_trace(infile_id,'y',1,startin1,countin1,1108,yy)
  CALL h5fclose_f(infile_id, error)
  OPEN(10, file="name.txt")
  DO kk=1,1108
    READ(10,*) name(kk)
  ENDDO
  CLOSE(10)

  CALL h5fopen_f(TRIM(cn_freq_name)//'_denoised.h5', H5F_ACC_RDONLY_F, file_id, error)

  countin1=(/1/)
  countin3=(/nn_npt,1,1/)

  write (*,'(A52)') 'Sp       AF  distance Elapsed_time compteur'

  DO ii=nn_ibeg,nn_iend,nn_istp

    pos_center_lon1(1) = xx(ii)
    pos_center_lat1(1) = yy(ii)

    DO kk=1,1108
      CALL dist_wh(distance1(kk),yy(kk),pos_center_lat1(1),xx(kk),pos_center_lon1(1))
      CALL azimuth(AF1(kk)      ,yy(kk),pos_center_lat1(1),xx(kk),pos_center_lon1(1))
    ENDDO

      ! Ranking from ORDERPACK2.0
!!! Note: Taken from ORDERPACK 2.0
!!! http://www.fortran-2000.com/ downloaded 2015-06-25
    CALL mrgrnk (distance1, JJ1)
    DO kk=1,nn_taille_reseau
      reseau1xx(kk)=xx(JJ1(kk))
      reseau1yy(kk)=yy(JJ1(kk))
      reseau1name(kk)=name(JJ1(kk))
      position1(kk,1)=distance1(JJ1(kk))*COS(pi_over_180*AF1(JJ1(kk)))
      position1(kk,2)=distance1(JJ1(kk))*SIN(pi_over_180*AF1(JJ1(kk)))
    ENDDO

    ! On remet a jour pos_center_lon/lat qui doivent etre les corrdonnees du centre du sous reseau (et pas les coordonnees de la station "centrale"
    pos_center_lon1(1) = SUM(reseau1xx(:)) * d_inv_nn_taille_reseau
    pos_center_lat1(1) = SUM(reseau1yy(:)) * d_inv_nn_taille_reseau

    DO jj=ii,nn_jend,nn_jstp

      pos_center_lon2(1)=xx(jj)
      pos_center_lat2(1)=yy(jj)
      CALL dist_wh(distance,pos_center_lat1(1),pos_center_lat2(1),pos_center_lon1(1),pos_center_lon2(1))
      CALL azimuth(AF,      pos_center_lat1(1),pos_center_lat2(1),pos_center_lon1(1),pos_center_lon2(1))

#ifdef lk_applycriteria
      IF ( (distance>=interval_distance(1)) .AND. & 
           (distance<=interval_distance(2)) .AND. &
           (ABS(AF-angle_main)<=interval_angle) ) THEN
#endif      

        CALL date_and_time(values=values)
        t0 = REAL(values(5))*3600.0+REAL(values(6))*60.0+REAL(values(7))+0.001*REAL(values(8))

        DO kk=1,1108
          CALL dist_wh(distance2(kk),yy(kk),pos_center_lat2(1),xx(kk),pos_center_lon2(1))
          CALL azimuth(AF2(kk)      ,yy(kk),pos_center_lat2(1),xx(kk),pos_center_lon2(1))
        ENDDO
        CALL mrgrnk (distance2, JJ2)
        DO kk=1,nn_taille_reseau
          reseau2xx(kk)=xx(JJ2(kk))
          reseau2yy(kk)=yy(JJ2(kk))
          reseau2name(kk)=name(JJ2(kk))
          position2(kk,1)=distance2(JJ2(kk))*COS(pi_over_180*AF2(JJ2(kk)))
          position2(kk,2)=distance2(JJ2(kk))*SIN(pi_over_180*AF2(JJ2(kk)))
        ENDDO

        ! On remet a jour pos_center_lon/lat qui doivent etre les corrdonnees du centre du sous reseau (et pas les coordonnees de la station "centrale"
        pos_center_lon2(1) = SUM(reseau2xx(:)) * d_inv_nn_taille_reseau
        pos_center_lat2(1) = SUM(reseau2yy(:)) * d_inv_nn_taille_reseau
        ! On remet a jour distance et AF avec ces centres de sous reseaux
        CALL dist_wh(distance,pos_center_lat1(1),pos_center_lat2(1),pos_center_lon1(1),pos_center_lon2(1))
        CALL azimuth(AF,      pos_center_lat1(1),pos_center_lat2(1),pos_center_lon1(1),pos_center_lon2(1))

        DO kk=1,nn_taille_reseau
          DO ll=1,nn_taille_reseau
            CALL dist_wh(distance_elements(kk,ll),reseau1yy(kk),reseau2yy(ll),reseau1xx(kk),reseau2xx(ll))
! read the good 625 reseau_xcorr_mask : here we read only 2 of them
            startin3=(/0,JJ1(kk)-1,JJ2(ll)-1/)
            CALL read_one_trace(file_id,'/reseau_xcorr_mask',3,startin3,countin3,nn_npt,r12)
            startin3=(/0,JJ2(ll)-1,JJ1(kk)-1/)
            CALL read_one_trace(file_id,'/reseau_xcorr_mask',3,startin3,countin3,nn_npt,r21)
            IF ( isnan(r12(1)) ) THEN ! test nan que pour ic=1
              reseau_xcorr_svd(:,(ll-1)*nn_taille_reseau+kk) = r21(:)
            ELSEIF ( isnan(r21(1)) ) THEN
              reseau_xcorr_svd(:,(ll-1)*nn_taille_reseau+kk) = r12(:)
            ELSE
              reseau_xcorr_svd(:,(ll-1)*nn_taille_reseau+kk) = 0.5d0 * ( r12(:) + r21(:) )
            ENDIF
          ENDDO
        ENDDO
!on aurait pas pu normaliser des ici ?
!c'est bizarre lordre des indices kk,ll ?
!JJ1 est légèrement différent entre matlab et fortran (précision sur tri des distance), donc il peut y avoir des permutations dans reseau_xcorr

        distance_elements_1d(:)=reshape(distance_elements,(/nn_taille_reseau2/))
        CALL mrgrnk (distance_elements_1d, idx_sort) ! idx_sort=JJ
        CALL mrgrnk (idx_sort, B)

        lisnan(:)=isnan(reseau_xcorr_svd(1,:))

! on fait le calcul avec les nan, ca restera nan...
        DO ic=1,nn_taille_reseau2
          IF ( .NOT. lisnan(ic) ) THEN
            maxabs=MAXVAL(ABS(reseau_xcorr_svd(:,ic)))
            reseau_xcorr_svd(:,ic) = reseau_xcorr_svd(:,ic) / maxabs
          ENDIF
        ENDDO

        compteur=0

        maxi_value=dn_maxi_value
!write(*,'(A99)'),'compte temps   vitesse                position_reseaux               distance    maximum    lenteur'
        DO WHILE ( (maxi_value .GT. dn_maxi_compteur) .AND. (compteur .LE. nn_threshold) ) 
!CALL date_and_time(values=values)
!t0 = REAL(values(5))*3600.0+REAL(values(6))*60.0+REAL(values(7))+0.001*REAL(values(8))

! qu'est ce qu'on choisit comme eps et t (tol abs et relative) au regard de fminbnd matlab ?
          fx = local_min ( dn_Lb, dn_Ub, eps, t, beamforming_slowness, x_x )

          signal_moyen(:) = beamforming_signal(x_x)
          CALL compute_hilbert(hilbert_signal_moyen(:),nn_npt,signal_moyen(:))
          DO ll=1,nn_npt
            signal_moyen_env(ll)=ABS(hilbert_signal_moyen(ll))
          ENDDO
          III = MAXLOC(signal_moyen_env)
          JJJ = signal_moyen_env(III)

          III_plus_index(:)=III(1)+index(:)
          IF ( MINVAL(ABS(III_plus_index(:)-1)) == 0 ) THEN
            iloc=MINLOC(ABS(III_plus_index(:)-1))
            good_len_index=2*index_length+1-iloc(1)+1
            ALLOCATE(goodindex(good_len_index))
            goodindex(:)=index(iloc(1):2*index_length+1)
          ELSEIF ( MINVAL(ABS(III_plus_index(:)-nn_npt)) == 0 ) THEN
            iloc=MINLOC(ABS(III_plus_index(:)-nn_npt))
            good_len_index=iloc(1)
            ALLOCATE(goodindex(good_len_index))
            goodindex(:)=index(1:iloc(1))
          ELSE
            good_len_index=2*index_length+1
            ALLOCATE(goodindex(good_len_index))
            goodindex(:)=index(:)
          ENDIF
          ALLOCATE(III_plus_goodindex(good_len_index))
          III_plus_goodindex(:)=III(1)+goodindex(:)
          DEALLOCATE(goodindex)
          toto(:)=0.d0
          CALL tukeywin(toto(III_plus_goodindex(1):III_plus_goodindex(good_len_index)),good_len_index,0.5d0)
          DEALLOCATE(III_plus_goodindex)
          DO ll=1,nn_npt
            toto(ll)=toto(ll)*signal_moyen(ll)
          ENDDO

!            %%%reconstruction de la wavelet

          DO ll=1,nn_taille_reseau2
            dtll=(distance_elements_1d(ll)-distance)*x_x*DBLE(nn_Fs)
            toto_svd(:,ll) = CSHIFT(toto(:),NINT(-dtll))
          ENDDO
          DO ll=1,nn_taille_reseau2
            toto_svd_trie(:,ll)=toto_svd(:,idx_sort(ll))
            reseau_xcorr_svd_trie(:,ll)=reseau_xcorr_svd(:,idx_sort(ll))
            lisnan_trie(ll)=lisnan(idx_sort(ll))
! reseau_xcorr_svd_trie et toto_svd_trie ne seront plus utilisé apres fnoptimize_amplitude_new donc on ourrait masker les nan par zero maintenant !
          ENDDO

!            %optimisation amplitude : technique 1

          start4 = (/ 0.d0 , 0.d0 , 0.d0 , 0.d0 /)  ! may be overwritten by nelmin
          step4 = (/dn_istep,dn_istep,dn_istep,dn_istep/)
          CALL nelmin(beamforming_amplitude , N , start4 , xxxamp , ynewlo , &
                      dn_reqmin, step4,  nn_konvge, nn_kcount, icount, numres, ifault ) 

          toto1tmp(:,:) = beamforming_signal_amplitude(xxxamp)

          energie1=0.d0
          DO ll=1,nn_taille_reseau2
            if ( .not. lisnan(ll) ) then  ! on ne met a jour reseau_xcorr_svd que si dif de nan
              DO ic=1,nn_npt
                reseau_xcorr_svd(ic,ll) = ( reseau_xcorr_svd(ic,ll)-toto1tmp(ic,B(ll)) ) * hanningwindow(ic)
                energie1 = energie1 + reseau_xcorr_svd(ic,ll)**2.d0
              ENDDO
            endif
          ENDDO

          temps_extrait(compte)=REAL(time_corr(III(1)))
          vitesse_extrait(compte)=REAL(distance/time_corr(III(1)))
          position_reseaux(:,compte)=REAL((/pos_center_lon1(1),pos_center_lon2(1),pos_center_lat1(1),pos_center_lat2(1)/))
          distance_reseaux(compte)=REAL(distance)
          maximum_extrait(compte)=REAL(JJJ(1))
          lenteur_extrait(compte)=REAL(x_x)
          energie_extrait(compte)=REAL(energie1)

!write (*,130),compte,temps_extrait(compte),vitesse_extrait(compte),position_reseaux(:,compte),distance_reseaux(compte),maximum_extrait(compte),lenteur_extrait(compte)
!130 format (I6,1X,F5.2,2X,F8.1,2X,6(F9.4,2X),F9.5,2X,F8.2)

          compte=compte+1
          compteur=compteur+1
          maxi_value=JJJ(1)

!CALL date_and_time(values=values)
!t1 = REAL(values(5))*3600.0+REAL(values(6))*60.0+REAL(values(7))+0.001*REAL(values(8)) - t0
!print *,'Elapsed time: ',t1,' sec'

        ENDDO ! END WHILE DO LOOP
        CALL date_and_time(values=values)
        t1 = REAL(values(5))*3600.0+REAL(values(6))*60.0+REAL(values(7))+0.001*REAL(values(8)) - t0
        write (*,'(I4,A3,I4,1X,F9.3,1X,F8.3,8X,F6.3,8X,I1)') ii,' - ',jj,AF,distance,t1,compteur
#ifdef lk_applycriteria
      ENDIF ! End test sur distance et angle
#endif
    ENDDO ! End jj loop
  ENDDO   ! End ii loop   

  CALL h5fclose_f(file_id, error)

  CALL h5fcreate_f(TRIM(cn_freq_name)//"_JD150_new_processed.h5",H5F_ACC_TRUNC_F,file_id,error)
  dims=(/1/)
  tmparr = (/compte/)
  CALL h5ltmake_dataset_int_f(file_id,"compte",1,dims,tmparr,error)
  tmparr = (/nn_taille_reseau/)
  CALL h5ltmake_dataset_int_f(file_id,"number",1,dims,tmparr,error)
  dims = (/compte-1/)
  CALL h5ltmake_dataset_float_f(file_id,"vitesse_extrait",1,dims,vitesse_extrait(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"temps_extrait",1,dims,temps_extrait(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"lenteur_extrait",1,dims,lenteur_extrait(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"distance_reseaux",1,dims,distance_reseaux(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"maximum_extrait",1,dims,maximum_extrait(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"energie_extrait",1,dims,energie_extrait(1:compte-1),error)
  dims2=(/4,compte-1/)
  CALL h5ltmake_dataset_float_f(file_id,"position_reseaux",2,dims2,position_reseaux(:,1:compte-1),error)
  CALL h5fclose_f(file_id,error)

  CALL h5close_f(error)

END PROGRAM DBFOPTIM_UNO
