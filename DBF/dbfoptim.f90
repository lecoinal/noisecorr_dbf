!###############################################################################
!#    This file is part of noisecorr_dbf : a Fortran code for computing        #
!#    double beamforming on the dataset from San Jacinto Fault zone            #
!#    dense array of geophones                                                 #
!#                                                                             #
!#    Copyright (C) 2015 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################
!
! https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Double_beamforming

PROGRAM DBFOPTIM
  USE HDF5
  USE H5LT      ! Lite Interface HDF5
  USE m_mrgrnk  ! Sorting and Ranking ORDERPACK lib (licence ?)
  USE dbfmodule
  USE dbfoptimmodule
  USE glovarmodule
#ifdef lk_amoeba
  USE amoebamodule
#endif
!  USE plotmodule
  IMPLICIT NONE

  REAL(KIND=8) :: Freq_centrale
  REAL(KIND=8), DIMENSION(2) :: Freq_interval
  CHARACTER(LEN=6) :: cn_freq_name 
  REAL(KIND=8), DIMENSION(2) :: interval_distance
  REAL(KIND=8) :: angle_main,interval_angle
  INTEGER :: nn_ibeg,nn_iend,nn_istp,nn_jend,nn_jstp,nn_threshold,nn_max_compte
  REAL(KIND=8) :: dn_maxi_compteur,dn_maxi_value
  REAL(KIND=8) :: dn_Lb,dn_Ub,dn_reqmin
  REAL(KIND=8) :: dn_istep
  INTEGER :: nn_konvge,nn_kcount

  REAL(KIND=8), DIMENSION(2) :: step2
  REAL(KIND=8), DIMENSION(4) :: step4

  REAL(KIND=8), DIMENSION(2) :: bandwidth
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: time_corr
  INTEGER :: ii,jj,kk,ll,ic,it
  INTEGER :: surech,error
  INTEGER(HID_T) :: file_id,infile_id,tmpfile_id      ! Input File identifier
  INTEGER(HSIZE_T), DIMENSION(1) :: startin1
  INTEGER(HSIZE_T), DIMENSION(1) :: countin1
  INTEGER(HSIZE_T), DIMENSION(3) :: startin3
  INTEGER(HSIZE_T), DIMENSION(3) :: countin3
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: xx,yy,reseau1xx,reseau1yy,reseau2xx,reseau2yy
  CHARACTER(len=5), DIMENSION(:), ALLOCATABLE :: reseau1name,reseau2name
  REAL(KIND=8),DIMENSION(:), ALLOCATABLE :: distance1,AF1,distance2,AF2
  REAL(KIND=8), DIMENSION(1) :: pos_center_lon1,pos_center_lat1,pos_center_lon2,pos_center_lat2
  CHARACTER(len=5), DIMENSION(:), ALLOCATABLE :: name
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: distance_elements
  integer(HSIZE_T), dimension(1) :: dims ! size of the corr_sp buffer
  integer(HSIZE_T), dimension(2) :: dims2
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: r12,r21
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: reseau_xcorr_svd1,reseau_xcorr_svd2
!  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: reseau_xcorr_svd0
  INTEGER, DIMENSION(:), ALLOCATABLE :: B,JJ1,JJ2,idx_sort
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: toto1tmp
  INTEGER :: compteur,compte
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: signal_moyen,signal_moyen_env,signal_moyen1,signal_moyen2
!  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: signal_moyen0
  COMPLEX(KIND=8), DIMENSION(:), ALLOCATABLE :: hilbert_signal_moyen
  INTEGER :: index_length
  REAL(KIND=8), DIMENSION(1) :: JJJ,JJJ1,JJJ2
  INTEGER, DIMENSION(1) :: III,III1,III2
  INTEGER, DIMENSION(:), ALLOCATABLE :: index
  REAL(KIND=8) :: AF,tmp
  REAL(KIND=4) :: t0,t1
  INTEGER, DIMENSION(8) :: values
  LOGICAL :: l_exist_namelist

  REAL(KIND=8) :: maxabs ,energie
  REAL(KIND=8) :: eps,t,fx,x_x,maxi_value
  real ( kind = 8 ) :: local_min,ynewlo
  REAL(KIND=8), DIMENSION(2) :: start,xxx1,xxx2,xxx
  REAL(KIND=8), DIMENSION(4) :: start4,xxxamp
  INTEGER :: icount, numres, ifault
  INTEGER, DIMENSION(:), ALLOCATABLE :: III_plus_index ! [2*index_length+1]
  INTEGER, DIMENSION(1) :: iloc
  INTEGER :: good_len_index,pp
  INTEGER, DIMENSION(:), ALLOCATABLE :: goodindex
  INTEGER, DIMENSION(:), ALLOCATABLE :: III_plus_goodindex
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: toto
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE  :: dt1,dt2
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE  :: toto_svd
  REAL(KIND=8) :: energie1,energie2
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: hanningwindow
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: x_ampl_powicoeffm1
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: deltacoeff_Tcoeffs

#ifdef lk_amoeba
  REAL(KIND=8), DIMENSION(3,2) :: P
  REAL(KIND=8), DIMENSION(2) :: PT
  REAL(KIND=8), DIMENSION(3) :: Y
  REAL(KIND=8), DIMENSION(5,4) :: P4
  REAL(KIND=8), DIMENSION(4) :: PT4
  REAL(KIND=8), DIMENSION(5) :: Y4
  INTEGER :: I,J
#endif

  ! Outputs
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: temps_extrait,vitesse_extrait,distance_reseaux,maximum_extrait,lenteur_extrait,azimuthal_extrait
  REAL(KIND=4), DIMENSION(:,:), ALLOCATABLE :: position_reseaux

  ! Declare NAMELIST
  NAMELIST /naminput/ Freq_centrale,nn_Fs,nn_npt,cn_freq_name,Freq_interval
  NAMELIST /namparam/ nn_taille_reseau,angle_main,interval_angle,interval_distance
  NAMELIST /namloop/ nn_ibeg,nn_iend,nn_istp,nn_jend,nn_jstp,dn_maxi_compteur,dn_maxi_value,nn_threshold,nn_max_compte
  NAMELIST /namlocmin/ dn_Lb,dn_Ub,dn_reqmin,dn_istep,nn_konvge,nn_kcount

  ! Read namelist
  INQUIRE(file='namelist', exist=l_exist_namelist)
  IF (.NOT. l_exist_namelist) THEN
     PRINT *,'The namelist does not exist'
     STOP
  ENDIF
  OPEN(11,file='namelist')
    REWIND 11
    READ(11,naminput)
    REWIND 11
    READ(11,namparam)
    REWIND 11
    READ(11,namloop)
    REWIND 11
    READ(11,namlocmin)
  CLOSE(11)

  nn_taille_reseau2=nn_taille_reseau*nn_taille_reseau

  ALLOCATE(time_corr(nn_npt))
  time_corr = (/((DBLE(ii-201)/DBLE(nn_Fs)),ii=1,nn_npt)/)

  bandwidth=Freq_interval

  surech=1
  DO WHILE ( DBLE(surech*nn_Fs)/Freq_centrale/20.d0 < 1.d0 )
    surech=surech+1
  ENDDO

  IF (surech .NE. 1 ) THEN
    PRINT *,"surech (= ",surech,") different from 1 is not supported"
    STOP
  ENDIF

  compte=1
          
  eps = 10.0D+00 * sqrt ( epsilon ( eps ) )
  t = eps
  N=4 ! the number of variables of the function for nelmin minimisation
  ALLOCATE(llpowtestm1(N,nn_taille_reseau2))
  CALL prepare_llpowtestm1(N)

  ALLOCATE(x_ampl_powicoeffm1(Ncoeffs,nn_taille_reseau2))
  CALL prepare_x_ampl_powicoeffm1(x_ampl_powicoeffm1)

  CALL generate_MaxMincoeffs()

  CALL h5open_f(error)

!  ALLOCATE(signal_moyen0(nn_npt))
  ALLOCATE(signal_moyen(nn_npt),hilbert_signal_moyen(nn_npt),signal_moyen_env(nn_npt))
  ALLOCATE(signal_moyen1(nn_npt),signal_moyen2(nn_npt))
  index_length=NINT(DBLE(nn_Fs)/DBLE((bandwidth(2)-bandwidth(1))))
  ALLOCATE(index(2*index_length+1))
  ALLOCATE(III_plus_index(2*index_length+1))
  index = (/( ii , ii=-index_length,index_length )/)
  ALLOCATE(reseau1xx(nn_taille_reseau),reseau1yy(nn_taille_reseau),reseau1name(nn_taille_reseau))
  ALLOCATE(reseau2xx(nn_taille_reseau),reseau2yy(nn_taille_reseau),reseau2name(nn_taille_reseau))
  ALLOCATE(xx(1108),yy(1108),name(1108))
  ALLOCATE(distance1(1108),AF1(1108),JJ1(1108),position1(nn_taille_reseau,2))
  ALLOCATE(distance2(1108),AF2(1108),JJ2(1108),position2(nn_taille_reseau,2))
  ALLOCATE(r12(nn_npt),r21(nn_npt))
  ALLOCATE(distance_elements(nn_taille_reseau,nn_taille_reseau))
  ALLOCATE(distance_elements_1d(nn_taille_reseau2))
  ALLOCATE(idx_sort(nn_taille_reseau2),B(nn_taille_reseau2))
  ALLOCATE(reseau_xcorr_svd(nn_npt,nn_taille_reseau2))
!  ALLOCATE(reseau_xcorr_svd0(nn_npt,nn_taille_reseau2))
  ALLOCATE(reseau_xcorr_svd1(nn_npt,nn_taille_reseau2))
  ALLOCATE(reseau_xcorr_svd2(nn_npt,nn_taille_reseau2))
  ALLOCATE(reseau_xcorr_svd_trie(nn_npt,nn_taille_reseau2))
  ALLOCATE(toto1tmp(nn_npt,nn_taille_reseau2))
  ALLOCATE(lisnan(nn_taille_reseau2))
  ALLOCATE(lisnan_trie(nn_taille_reseau2))
  ALLOCATE(toto(nn_npt),toto_svd(nn_npt,nn_taille_reseau2),toto_svd_trie(nn_npt,nn_taille_reseau2))
  ALLOCATE(dt1(nn_taille_reseau),dt2(nn_taille_reseau))
  ALLOCATE(hanningwindow(nn_npt))
  CALL hanning_window(hanningwindow,nn_npt)
  DO ic=1,nn_npt
    hanningwindow(ic)=hanningwindow(ic)**0.05d0
  ENDDO
  ALLOCATE(temps_extrait(nn_max_compte),vitesse_extrait(nn_max_compte),distance_reseaux(nn_max_compte), &
           maximum_extrait(nn_max_compte),lenteur_extrait(nn_max_compte),azimuthal_extrait(nn_max_compte))
  ALLOCATE(position_reseaux(4,nn_max_compte))

  CALL h5fopen_f("reseau_64.h5", H5F_ACC_RDONLY_F, infile_id, error)
  startin1=(/0/)
  countin1=(/1108/)
  CALL read_one_trace(infile_id,'x',1,startin1,countin1,1108,xx)
  CALL read_one_trace(infile_id,'y',1,startin1,countin1,1108,yy)
  CALL h5fclose_f(infile_id, error)
  OPEN(10, file="name.txt")
  DO kk=1,1108
    READ(10,*) name(kk)
  ENDDO
  CLOSE(10)

  CALL h5fopen_f(cn_Freq_name//'_denoised.mat', H5F_ACC_RDONLY_F, file_id, error)

  countin1=(/1/)
  countin3=(/nn_npt,1,1/)

  DO ii=nn_ibeg,nn_iend,nn_istp

    pos_center_lon1(1) = xx(ii)
    pos_center_lat1(1) = yy(ii)

    DO kk=1,1108
      CALL dist_wh(distance1(kk),yy(kk),pos_center_lat1(1),xx(kk),pos_center_lon1(1))
      CALL azimuth(AF1(kk)      ,yy(kk),pos_center_lat1(1),xx(kk),pos_center_lon1(1))
!      CALL azimuth_and_dist(distance1(kk),AF1(kk),yy(kk),pos_center_lat1(1),xx(kk),pos_center_lon1(1))
    ENDDO

      ! Ranking from ORDERPACK2.0
!!! Note: Taken from ORDERPACK 2.0
!!! http://www.fortran-2000.com/ downloaded 2015-06-25
    CALL mrgrnk (distance1, JJ1)
    DO kk=1,nn_taille_reseau
      reseau1xx(kk)=xx(JJ1(kk))
      reseau1yy(kk)=yy(JJ1(kk))
      reseau1name(kk)=name(JJ1(kk))
      position1(kk,1)=distance1(JJ1(kk))*COS(pi_over_180*AF1(JJ1(kk)))
      position1(kk,2)=distance1(JJ1(kk))*SIN(pi_over_180*AF1(JJ1(kk)))
    ENDDO

    DO jj=ii,nn_jend,nn_jstp

      pos_center_lon2(1)=xx(jj)
      pos_center_lat2(1)=yy(jj)
      CALL dist_wh(distance,pos_center_lat1(1),pos_center_lat2(1),pos_center_lon1(1),pos_center_lon2(1))
      CALL azimuth(AF,      pos_center_lat1(1),pos_center_lat2(1),pos_center_lon1(1),pos_center_lon2(1))
!      CALL azimuth_and_dist(distance,AF,pos_center_lat1(1),pos_center_lat2(1),pos_center_lon1(1),pos_center_lon2(1))

      IF ( (distance>=interval_distance(1)) .AND. & 
           (distance<=interval_distance(2)) .AND. &
           (ABS(AF-angle_main)<=interval_angle) ) THEN
        DO kk=1,1108
          CALL dist_wh(distance2(kk),yy(kk),pos_center_lat2(1),xx(kk),pos_center_lon2(1))
          CALL azimuth(AF2(kk)      ,yy(kk),pos_center_lat2(1),xx(kk),pos_center_lon2(1))
!          CALL azimuth_and_dist(distance2(kk),AF2(kk),yy(kk),pos_center_lat2(1),xx(kk),pos_center_lon2(1))
        ENDDO
        CALL mrgrnk (distance2, JJ2)
        DO kk=1,nn_taille_reseau
          reseau2xx(kk)=xx(JJ2(kk))
          reseau2yy(kk)=yy(JJ2(kk))
          reseau2name(kk)=name(JJ2(kk))
          position2(kk,1)=distance2(JJ2(kk))*COS(pi_over_180*AF2(JJ2(kk)))
          position2(kk,2)=distance2(JJ2(kk))*SIN(pi_over_180*AF2(JJ2(kk)))
        ENDDO
write (*,'(A5,I4,A3,I4,A8,F7.3,A5,F8.3)'),'Sp = ',ii,' - ',jj,' , AF = ',AF,' d = ',distance

!        distance_elements(:,:)=0.d0  ! cette init est inutile
!        reseau_xcorr(:,:,:)=0.d0  ! cette init est inutile
        DO kk=1,nn_taille_reseau
          DO ll=1,nn_taille_reseau
            CALL dist_wh(distance_elements(kk,ll),reseau1yy(kk),reseau2yy(ll),reseau1xx(kk),reseau2xx(ll))
!            CALL azimuth_and_dist(distance_elements(kk,ll),tmp,reseau1yy(kk),reseau2yy(ll),reseau1xx(kk),reseau2xx(ll))           
! read the good 625 reseau_xcorr_mask : here we read only 2 of them
            startin3=(/0,JJ1(kk)-1,JJ2(ll)-1/)
            CALL read_one_trace(file_id,'/reseau_xcorr_mask',3,startin3,countin3,nn_npt,r12)
            startin3=(/0,JJ2(ll)-1,JJ1(kk)-1/)
            CALL read_one_trace(file_id,'/reseau_xcorr_mask',3,startin3,countin3,nn_npt,r21)
            IF ( isnan(r12(1)) ) THEN ! test nan que pour ic=1
!              reseau_xcorr(:,kk,ll) = r21(:)
              reseau_xcorr_svd(:,(ll-1)*nn_taille_reseau+kk) = r21(:)
            ELSEIF ( isnan(r21(1)) ) THEN
!              reseau_xcorr(:,kk,ll) = r12(:)
              reseau_xcorr_svd(:,(ll-1)*nn_taille_reseau+kk) = r12(:)
            ELSE
!              reseau_xcorr(:,kk,ll) = 0.5d0 * ( r12(:) + r21(:) )
              reseau_xcorr_svd(:,(ll-1)*nn_taille_reseau+kk) = 0.5d0 * ( r12(:) + r21(:) )
            ENDIF
          ENDDO
        ENDDO
!on aurait pas pu normaliser des ici ?
!c'est bizarre lordre des indices kk,ll ?
!JJ1 est légèrement différent entre matlab et fortran (précision sur tri des distance), donc il peut y avoir des permutations dans reseau_xcorr

        distance_elements_1d(:)=reshape(distance_elements,(/nn_taille_reseau2/))
        CALL mrgrnk (distance_elements_1d, idx_sort) ! idx_sort=JJ
        CALL mrgrnk (idx_sort, B)

        lisnan(:)=isnan(reseau_xcorr_svd(1,:))

! on fait le calcul avec les nan, ca restera nan...
        DO ic=1,nn_taille_reseau2
          IF ( .NOT. lisnan(ic) ) THEN
            maxabs=MAXVAL(ABS(reseau_xcorr_svd(:,ic)))
            reseau_xcorr_svd(:,ic) = reseau_xcorr_svd(:,ic) / maxabs
          ENDIF
        ENDDO

! ce calcul de l'energie est inutile
        energie=0.d0
        DO ic=1,nn_taille_reseau2
          IF ( .NOT. lisnan(ic) ) THEN
            DO it=1,nn_npt
              energie = energie + reseau_xcorr_svd(it,ic)**2.d0
            ENDDO
          ENDIF
        ENDDO

print *,'Iteration = 0 - Energie = ',energie


!%        figure(600);clf
!%        imagesc(time_corr,1:size(reseau_xcorr_svd,2),reseau_xcorr_svd(:,JJ)')
!%        %caxis([-max(abs(caxis))*0.6 max(abs(caxis))*0.6])
!%        caxis([-1 1])
!%        colorbar
!%        set(gca,'ydir','normal')
!%        title([num2str(ii) ' - ' num2str(jj) ' - Iteration = 0 - Energie = ' num2str(energie)])
!%        pause(0.1)

!         DO ll=1,nn_taille_reseau2
!           reseau_xcorr_svd0(:,ll)=reseau_xcorr_svd(:,idx_sort(ll))
!         ENDDO
!         CALL plot_xy(nn_npt,nn_taille_reseau2,REAL(reseau_xcorr_svd0),'fig600.ps','reseau_xcorr_svd0')
!         !CALL plot_xy(10,10,REAL(reseau_xcorr_svd0(1:10,1:10)),'fig600.ps','reseau_xcorr_svd0')

!========================================================================!

        compteur=0
        NSA = 8000 ! IF ( compteur .LE. 1 
        if (allocated(deltacoeff_Tcoeffs) .eqv. .true.) deallocate(deltacoeff_Tcoeffs)
        ALLOCATE(deltacoeff_Tcoeffs(Ncoeffs,NSA))
        CALL prepare_deltacoeff_Tcoeffs(deltacoeff_Tcoeffs)

        maxi_value=dn_maxi_value
write(*,'(A109)'),'compte temps   vitesse                position_reseaux               distance    maximum    lenteur   azimuth'
        DO WHILE ( (maxi_value .GT. dn_maxi_compteur) .AND. (compteur .LE. nn_threshold) ) 
CALL date_and_time(values=values)
t0 = REAL(values(5))*3600.0+REAL(values(6))*60.0+REAL(values(7))+0.001*REAL(values(8))

! qu'est ce qu'on choisit comme eps et t (tol abs et relative) au regard de fminbnd matlab ?
          fx = local_min ( dn_Lb, dn_Ub, eps, t, beamforming_slowness, x_x )

!print *,'x_x',x_x
!x_x = 0.001974954155291d0
!print *,'x_x',x_x


!          signal_moyen0(:) = beamforming_signal(x_x)
!! inutile ?
!          CALL compute_hilbert(hilbert_signal_moyen(:),nn_npt,signal_moyen(:))
!          DO ll=1,nn_npt
!            signal_moyen_env(ll)=ABS(hilbert_signal_moyen(ll))
!          ENDDO
!          III = MAXLOC(signal_moyen_env)
!          JJJ = signal_moyen_env(III)
!          CALL h5fcreate_f("file.h5",H5F_ACC_TRUNC_F,tmpfile_id,error)
!          dims = (/401/)
!          CALL h5ltmake_dataset_float_f(tmpfile_id,"signal_moyen",1,dims,REAL(signal_moyen(:)),error)
!          CALL h5fclose_f(tmpfile_id,error)
!! ce calcul signal_moyen et III JJJ est inutile car va etre ecrasée par les deux fminsearch ci dessous

!CALL h5fcreate_f("fffile.h5",H5F_ACC_TRUNC_F,tmpfile_id,error)
!dims2 = (/401,625/)
!CALL h5ltmake_dataset_float_f(tmpfile_id,"reseau_xcorr_svd",2,dims2,REAL(reseau_xcorr_svd(:,:)),error)
!CALL h5fclose_f(tmpfile_id,error)

!print *,'AF,xx,',AF,x_x

#ifdef lk_amoeba

!CALL AMOEBA(FUNC,P,Y,MP,NP,NDIM,FTOL,ITER)
!define NDIM+1 initial vertices (one by row) : starting point and +/- 1%
P(1,1)=AF+180.d0;  P(1,2)=x_x
!P(1,1)=AF+210.d0;  P(1,2)=x_x
P(2,1)=1.01d0*P(1,1); P(2,2)=1.01d0*P(1,2)
P(3,1)=0.99d0*P(1,1);  P(3,2)=0.99d0*P(1,2)
!Initialize Y to the values of FUNC evaluated 
!at the NDIM+1 vertices (rows) of P
DO I=1,3 ! NDIM+1
  PT(1)=P(I,1); PT(2)=P(I,2); 
  Y(I)=beamforming_azimuth_slowness(PT)
END DO
CALL AMOEBA(beamforming_azimuth_slowness,P,Y,3,2,2,dn_reqmin,icount)
xxx1(:) = P(1,:)
!  print *,' Number of iterations:', icount
!  print *,' Best NDIM+1 points:'
!  write(*,10) ((P(I,J),J=1,2),I=1,3)
!  print *,' Best NDIM+1 mimimum values:'
!  write(*,20) (Y(I),I=1,3) 
!10 format(2E16.8)
!20 format(E16.8)

#else

          start = (/ AF+180.d0 , x_x /)  ! may be overwritten by nelmin
! on teste de démarrer d'un autre azimuth
!print *,'x_x',x_x
!          start = (/ AF+210.d0 , x_x /)  ! may be overwritten by nelmin
          step2 = (/dn_istep,dn_istep/)
          CALL nelmin(beamforming_azimuth_slowness , 2 , start , xxx1 , ynewlo , &
                      dn_reqmin, step2,  nn_konvge, nn_kcount, icount, numres, ifault )
!print *,'ifault,xxx1,ynewlo',ifault,xxx1,ynewlo
!write (*,110),'ifault,xxx1,ynewlo ',ifault,xxx1,ynewlo 
!110 format (A19,I1,1X,F9.3,1X,F10.7,1X,F9.5)
!print *,'on refixe volontairement le pt trouve par matlab'
!xxx1 = (/217.3850532561265d0,0.0024186769560d0/)
!print *,'xxx1',xxx1,ynewlo
!print *,beamforming_azimuth_slowness(xxx1)
!print *,beamforming_azimuth_slowness((/217.3850532561265d0,0.0024186769560d0/))

#endif
          signal_moyen1(:) = beamforming_signal_slowness_azimuth(xxx1)
          CALL compute_hilbert(hilbert_signal_moyen(:),nn_npt,signal_moyen1(:))
          DO ll=1,nn_npt
            signal_moyen_env(ll)=ABS(hilbert_signal_moyen(ll))
          ENDDO
          III1 = MAXLOC(signal_moyen_env)
          JJJ1 = signal_moyen_env(III1)

#ifdef lk_amoeba

P(1,1)=AF;  P(1,2)=x_x
!P(1,1)=AF+30.d0;  P(1,2)=x_x
P(2,1)=1.01d0*P(1,1); P(2,2)=1.01d0*P(1,2)
P(3,1)=0.99d0*P(1,1);  P(3,2)=0.99d0*P(1,2)
DO I=1,3 ! NDIM+1
  PT(1)=P(I,1); PT(2)=P(I,2); 
  Y(I)=beamforming_azimuth_slowness(PT)
END DO
CALL AMOEBA(beamforming_azimuth_slowness,P,Y,3,2,2,dn_reqmin,icount)
xxx2(:) = P(1,:)
!  print *,' Number of iterations:', icount
!  print *,' Best NDIM+1 points:'
!  write(*,10) ((P(I,J),J=1,2),I=1,3)
!  print *,' Best NDIM+1 mimimum values:'
!  write(*,20) (Y(I),I=1,3) 

#else

          start = (/ AF , x_x /)  ! may be overwritten by nelmin
!          start = (/ 34.0d0 , x_x /)  ! may be overwritten by nelmin
!          start = (/ AF+30.d0 , x_x /)  ! may be overwritten by nelmin
          CALL nelmin(beamforming_azimuth_slowness , 2 , start , xxx2 , ynewlo , &
                      dn_reqmin, step2,  nn_konvge, nn_kcount, icount, numres, ifault ) 
!print *,'ifault,xxx2,ynewlo',ifault,xxx2,ynewlo
!write (*,110),'ifault,xxx2,ynewlo ',ifault,xxx2,ynewlo 
!print *,'on refixe volontairement le pt trouve par matlab'
!xxx2 = (/34.991571054617822d0 , 0.001947968950216d0/)
!print *,'xxx2',xxx2,ynewlo
!print *,beamforming_azimuth_slowness(xxx2)
!print *,beamforming_azimuth_slowness((/34.991571054617822d0,0.001947968950216d0/))

#endif

          signal_moyen2(:) = beamforming_signal_slowness_azimuth(xxx2)
          CALL compute_hilbert(hilbert_signal_moyen(:),nn_npt,signal_moyen2(:))
          DO ll=1,nn_npt
            signal_moyen_env(ll)=ABS(hilbert_signal_moyen(ll))
          ENDDO
          III2 = MAXLOC(signal_moyen_env)
          JJJ2 = signal_moyen_env(III2)
          IF ( JJJ1(1) > JJJ2(1) ) THEN
            signal_moyen(:)=signal_moyen1(:)
            JJJ = JJJ1
            III = III1
            xxx = xxx1
          ELSE
            signal_moyen(:)=signal_moyen2(:)
            JJJ = JJJ2
            III = III2
            xxx = xxx2
          ENDIF

          IF ( xxx(1) > 180.d0 ) THEN
            xxx(1)=xxx(1)-360.d0
          ELSEIF ( xxx(1) < -180.d0 ) THEN
            xxx(1)=xxx(1)+360.d0
          ENDIF

!          CALL h5fcreate_f("ffile.h5",H5F_ACC_TRUNC_F,tmpfile_id,error)
!          dims = (/401/)
!          CALL h5ltmake_dataset_float_f(tmpfile_id,"signal_moyen",1,dims,REAL(signal_moyen(:)),error)
!          CALL h5fclose_f(tmpfile_id,error)

!          ctitle  write(ctitle,'(I0.2)') xxx(1)-AF 1.d0/xxx(2)
!          CALL plot_t2(nn_npt,REAL(time_corr),REAL(signal_moyen0),REAL(signal_moyen),'fig1.ps') ! ,XLABEL='Time (days)',YLABEL='Wind Friction') ! ,TITLE=ctitle)
!            title(num2str([xxx(1)-AF 1/xxx(2)]))




          III_plus_index(:)=III(1)+index(:)
          IF ( MINVAL(ABS(III_plus_index(:)-1)) == 0 ) THEN
            iloc=MINLOC(ABS(III_plus_index(:)-1))
            good_len_index=2*index_length+1-iloc(1)+1
            ALLOCATE(goodindex(good_len_index))
            goodindex(:)=index(iloc(1):2*index_length+1)
          ELSEIF ( MINVAL(ABS(III_plus_index(:)-nn_npt)) == 0 ) THEN
            iloc=MINLOC(ABS(III_plus_index(:)-nn_npt))
            good_len_index=iloc(1)
            ALLOCATE(goodindex(good_len_index))
            goodindex(:)=index(1:iloc(1))
          ELSE
            good_len_index=2*index_length+1
            ALLOCATE(goodindex(good_len_index))
            goodindex(:)=index(:)
          ENDIF
          ALLOCATE(III_plus_goodindex(good_len_index))
          III_plus_goodindex(:)=III(1)+goodindex(:)
          DEALLOCATE(goodindex)
          toto(:)=0.d0
          CALL tukeywin(toto(III_plus_goodindex(1):III_plus_goodindex(good_len_index)),good_len_index,0.5d0)
          DEALLOCATE(III_plus_goodindex)
          DO ll=1,nn_npt
            toto(ll)=toto(ll)*signal_moyen(ll)
          ENDDO

!            %%%reconstruction de la wavelet
          DO ll=1,nn_taille_reseau
            dt1(ll) = xxx(2) * ( position1(ll,1)*SIN(pi_over_180 * xxx(1))+position1(ll,2)*COS(pi_over_180 * xxx(1)) ) * DBLE(nn_Fs)
            dt2(ll) = xxx(2) * ( position2(ll,1)*SIN(pi_over_180 * xxx(1))+position2(ll,2)*COS(pi_over_180 * xxx(1)) ) * DBLE(nn_Fs)
          ENDDO
          DO pp=1,nn_taille_reseau
            DO ll=1,nn_taille_reseau
              toto_svd(:,(pp-1)*nn_taille_reseau+ll) = CSHIFT(toto(:),NINT(-dt1(ll)+dt2(pp)))
            ENDDO
          ENDDO
!          CALL h5fcreate_f("file.h5",H5F_ACC_TRUNC_F,tmpfile_id,error)
!          dims2 = (/401,625/)
!          CALL h5ltmake_dataset_float_f(tmpfile_id,"toto",2,dims2,REAL(toto_svd(:,:)),error)
!          CALL h5fclose_f(tmpfile_id,error)
          DO ll=1,nn_taille_reseau2
            toto_svd_trie(:,ll)=toto_svd(:,idx_sort(ll))
            reseau_xcorr_svd_trie(:,ll)=reseau_xcorr_svd(:,idx_sort(ll))
            lisnan_trie(ll)=lisnan(idx_sort(ll))
! reseau_xcorr_svd_trie et toto_svd_trie ne seront plus utilisé apres fnoptimize_amplitude_new donc on ourrait masker les nan par zero maintenant !
          ENDDO

!            %optimisation amplitude : technique 1

#ifdef lk_amoeba

P4(1,:)=0.d0  ! [5,4]
P4(2,:)=-0.002d0  ! [5,4]
P4(3,:)=-0.001d0  ! [5,4]
P4(4,:)= 0.001d0  ! [5,4]
P4(5,:)= 0.002d0  ! [5,4]
DO I=1,5 ! NDIM+1
  PT4(1)=P4(I,1); PT4(2)=P4(I,2); PT4(3)=P4(I,3); PT4(4)=P4(I,4);
  Y4(I)=beamforming_amplitude(PT4)
END DO
CALL AMOEBA(beamforming_amplitude,P4,Y4,5,4,4,dn_reqmin,icount)
xxxamp(:) = P4(1,:)
!  print *,' Number of iterations:', icount
!  print *,' Best NDIM+1 points:'
!  write(*,30) ((P4(I,J),J=1,4),I=1,5)
!  print *,' Best NDIM+1 mimimum values:'
!  write(*,40) (Y4(I),I=1,5) 
!30 format(4E16.8)
!40 format(E16.8)

#else

          start4 = (/ 0.d0 , 0.d0 , 0.d0 , 0.d0 /)  ! may be overwritten by nelmin
          step4 = (/dn_istep,dn_istep,dn_istep,dn_istep/)
          CALL nelmin(beamforming_amplitude , N , start4 , xxxamp , ynewlo , &
                      dn_reqmin, step4,  nn_konvge, nn_kcount, icount, numres, ifault ) 
!print *,'xxxamp',xxxamp
!xxxamp=(/-0.085020662604183,0.000871517088001,-0.000001616796715,0.000000000549989/)
!print *,'xxxamp',xxxamp

#endif


          toto1tmp(:,:) = beamforming_signal_amplitude(xxxamp)

!CALL h5fcreate_f("file.h5",H5F_ACC_TRUNC_F,tmpfile_id,error)
!dims2 = (/401,625/)
!CALL h5ltmake_dataset_float_f(tmpfile_id,"toto1",2,dims2,REAL(toto1tmp(:,:)),error)
!CALL h5ltmake_dataset_float_f(tmpfile_id,"toto_svd_trie",2,dims2,REAL(toto_svd_trie(:,:)),error)
!CALL h5ltmake_dataset_float_f(tmpfile_id,"toto_svd",2,dims2,REAL(toto_svd(:,:)),error)
!dims = (/625/)
!CALL h5ltmake_dataset_int_f(tmpfile_id,"B",1,dims,B,error)
!CALL h5fclose_f(tmpfile_id,error)
!print *,'B'
!DO ll=1,625
!print *,B(ll)
!enddo

          IF ( compteur .EQ. 2 ) THEN
            DEALLOCATE(deltacoeff_Tcoeffs)
            NSA=5000 ! if compteur >=2
            ALLOCATE(deltacoeff_Tcoeffs(Ncoeffs,NSA))
            CALL prepare_deltacoeff_Tcoeffs(deltacoeff_Tcoeffs)
          ENDIF
!            %%%%optimization amplitude : technique 2
!          CALL fn_optimize_amplitude_new(reseau_xcorr_svd_trie,toto_svd_trie,x_ampl_powicoeffm1,deltacoeff_Tcoeffs)

          energie1=0.d0
!          energie2=0.d0
          DO ll=1,nn_taille_reseau2
            if ( .not. lisnan(ll) ) then  ! on ne met a jour reseau_xcorr_svd1/2 que si dif de nan
              DO ic=1,nn_npt
                reseau_xcorr_svd1(ic,ll) = ( reseau_xcorr_svd(ic,ll)-toto1tmp(ic,B(ll)) ) * hanningwindow(ic)
                energie1 = energie1 + reseau_xcorr_svd1(ic,ll)**2.d0
!                reseau_xcorr_svd2(ic,ll) = ( reseau_xcorr_svd(ic,ll)-toto_svd_trie(ic,B(ll)) ) * hanningwindow(ic)
!                energie2 = energie2 + reseau_xcorr_svd2(ic,ll)**2.d0
              ENDDO
            endif
          ENDDO
!          CALL h5fcreate_f("tfile.h5",H5F_ACC_TRUNC_F,tmpfile_id,error)
!          dims2 = (/401,625/)
!          CALL h5ltmake_dataset_float_f(tmpfile_id,"data",2,dims2,REAL(reseau_xcorr_svd1(:,:)),error)
!          dims = (/625/)
!          CALL h5ltmake_dataset_int_f(tmpfile_id,"JJ",1,dims,idx_sort(:),error)
!          CALL h5fclose_f(tmpfile_id,error)
write (*,120),'Iteration = ',compteur+1,' - 1/lenteur = ',1.d0/x_x,' - Energie = ',energie1
120 format (A12,I3,A15,F9.3,A13,F9.3)



!          IF ( energie1 .LE. energie2 ) THEN
            reseau_xcorr_svd(:,:) = reseau_xcorr_svd1(:,:)
!          ELSE
!            reseau_xcorr_svd(:,:) = reseau_xcorr_svd2(:,:)
!          ENDIF

          temps_extrait(compte)=REAL(time_corr(III(1)))
          vitesse_extrait(compte)=REAL(distance/time_corr(III(1)))
          position_reseaux(:,compte)=REAL((/pos_center_lon1(1),pos_center_lon2(1),pos_center_lat1(1),pos_center_lat2(1)/))
          distance_reseaux(compte)=REAL(distance)
          maximum_extrait(compte)=REAL(JJJ(1))
          lenteur_extrait(compte)=REAL(xxx(2))
          azimuthal_extrait(compte)=REAL(xxx(1))

write (*,130),compte,temps_extrait(compte),vitesse_extrait(compte),position_reseaux(:,compte),distance_reseaux(compte),maximum_extrait(compte),lenteur_extrait(compte),azimuthal_extrait(compte)
130 format (I6,1X,F5.2,2X,F8.1,2X,6(F9.4,2X),F9.5,2X,F8.2)

          compte=compte+1
          compteur=compteur+1
          maxi_value=JJJ(1)

CALL date_and_time(values=values)
t1 = REAL(values(5))*3600.0+REAL(values(6))*60.0+REAL(values(7))+0.001*REAL(values(8)) - t0
!print *,'NSA,compteur,elapsed time = ',NSA,compteur,t1

        ENDDO ! END WHILE DO LOOP
      ENDIF ! End test sur distance et angle
    ENDDO ! End jj loop
  ENDDO   ! End ii loop   

  CALL h5fclose_f(file_id, error)

  CALL h5fcreate_f(cn_freq_name//"_JD150_new_processed.h5",H5F_ACC_TRUNC_F,file_id,error)
  dims=(/1/)
  CALL h5ltmake_dataset_int_f(file_id,"compte",1,dims,(/compte/),error)
  CALL h5ltmake_dataset_int_f(file_id,"number",1,dims,(/nn_taille_reseau/),error)
  dims = (/compte-1/)
  CALL h5ltmake_dataset_float_f(file_id,"vitesse_extrait",1,dims,vitesse_extrait(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"temps_extrait",1,dims,temps_extrait(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"lenteur_extrait",1,dims,lenteur_extrait(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"azimuthal_extrait",1,dims,azimuthal_extrait(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"distance_reseaux",1,dims,distance_reseaux(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"maximum_extrait",1,dims,maximum_extrait(1:compte-1),error)
  dims2=(/4,compte-1/)
  CALL h5ltmake_dataset_float_f(file_id,"position_reseaux",2,dims2,position_reseaux(:,1:compte-1),error)
  CALL h5fclose_f(file_id,error)

  CALL h5close_f(error)

END PROGRAM DBFOPTIM
