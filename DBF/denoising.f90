!###############################################################################
!#    This file is part of noisecorr_dbf : a Fortran code for computing        #
!#    double beamforming on the dataset from San Jacinto Fault zone            #
!#    dense array of geophones                                                 #
!#                                                                             #
!#    Copyright (C) 2015 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################
!
! https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Double_beamforming

PROGRAM DENOISING
  USE HDF5
  USE H5LT      ! Lite Interface HDF5
  USE dbfmodule
  IMPLICIT NONE

  ! Instrumentation du code : analyse de performance
  INTEGER,DIMENSION(8) :: values
  REAL(KIND=4) :: elps            ! in sec

  ! Input and Output file

  INTEGER(HID_T) :: infile_id                 ! Input File identifier
  INTEGER(HSIZE_T), DIMENSION(1) :: startin1  ! Input file vector offset
  INTEGER(HSIZE_T), DIMENSION(1) :: countin1  !                   and size
  LOGICAL :: link_exists
  INTEGER :: error

  INTEGER(HID_T) :: outfile_id                ! Output File identifier
  INTEGER :: rank = 3                         ! Output Dataset rank ( in file )
  INTEGER(HSIZE_T), DIMENSION(1:3) :: dimsf   ! = (/401,1108,1108/) ! Dataset dimensions in outfile: Here first dim is contiguous
  INTEGER(HID_T) :: dataspace                 ! Dataspace identifier 
  INTEGER(HID_T) :: dset_id                   ! Dataset identifier 
  INTEGER(HSIZE_T), DIMENSION(1:3) :: count   ! = (/401,1108,1/)  ! Size of hyperslab
  INTEGER(HSIZE_T), DIMENSION(1:3) :: offset  ! = (/0,0,jj-1/)    ! Hyperslab offset
  INTEGER :: mrank = 2                        ! Output Dataset rank ( in memory )
  INTEGER(HSIZE_T), DIMENSION(1:2) :: dimsm   ! = (/401,1108/) ! Dataset dimensions in memory
  INTEGER(HID_T) :: memspace                  ! memspace identifier 
  INTEGER(HSIZE_T), DIMENSION(2) :: data_dims

  ! Some useful constants
  REAL(KIND=8), PARAMETER :: pi_over_180 = 4.d0 * DATAN(1.d0) / 180.d0
  REAL(KIND=8), PARAMETER :: pi = 4.d0 * DATAN(1.d0)
  COMPLEX(KIND=8),parameter :: cj=(0.d0,1.d0)

  CHARACTER(len=5), DIMENSION(:), ALLOCATABLE :: staname  ! Stations names
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: xx,yy        ! Stations positions
  CHARACTER(len=11) :: nom_paire
  REAL(KIND=8) :: distance_elements,angle
  REAL(KIND=8),DIMENSION(:,:), ALLOCATABLE :: position    ! Station positions on a grid

  INTEGER :: jt,jf,ii,pp,ji,jj   ! Loop index

  ! Temporary work array
  INTEGER :: nfreq
  REAL(KIND=8) :: maxabs,invmaxabs,increment,deltat,inv_norm_omega,tmp,sigma,inv_deux_sigma_sigma
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: hann025,hannwithzero,kx,kxkx,time_corr,freq_vect,corr
  REAL(KIND=8),DIMENSION(:,:), ALLOCATABLE :: reseau_xcorr,mask,reseau_xcorr_mask
  COMPLEX(KIND=8) :: cplxsomme,cplxtmp,beamforming
  COMPLEX(KIND=8),DIMENSION(:), ALLOCATABLE :: omega,reseau_xcorr_freq
  COMPLEX(KIND=8),DIMENSION(:,:), ALLOCATABLE :: reseau_xcorr_freq_mask,matrice_int

  ! Read from namelist
  LOGICAL :: l_exist_namelist
  REAL(KIND=8)              :: Freq_centrale ! 1.3
  CHARACTER(LEN=256)        :: cn_freq_name  ! EXPA013
  CHARACTER(LEN=256)        :: cn_inputfile
  REAL(KIND=8)              :: sigma_inv     ! 700
  REAL(KIND=8),DIMENSION(2) :: Freq_interval ! (/0.8667,1.7333/)
  REAL(KIND=8)              :: nn_Fs         ! 100
  INTEGER                   :: Nb_points     ! 151
  INTEGER                   :: nn_sta        ! 1108 ! The total number of stations
  INTEGER                   :: nn_npt        ! 401  ! Length of time correlation window [-2sec:+2sec 100Hz]

  ! Declare NAMELIST
  NAMELIST /naminput/ Freq_centrale,nn_Fs,nn_npt,nn_sta,cn_freq_name,cn_inputfile,Freq_interval
  NAMELIST /namparam/ sigma_inv,Nb_points

  ! Read namelist
  INQUIRE(file='namelist', exist=l_exist_namelist)
  IF (.NOT. l_exist_namelist) THEN
     PRINT *,'The namelist does not exist'
     STOP
  ENDIF
  OPEN(11,file='namelist')
    REWIND 11
    READ(11,naminput)
    REWIND 11
    READ(11,namparam)
  CLOSE(11)

  dimsf = (/nn_npt,nn_sta,nn_sta/)
  count = (/nn_npt,nn_sta,1/)
  dimsm = (/nn_npt,nn_sta/) 
  data_dims(1:2) = (/nn_npt, nn_sta/) 

  ALLOCATE(time_corr(nn_npt))
  ALLOCATE(xx(nn_sta),yy(nn_sta),staname(nn_sta),corr(nn_npt),reseau_xcorr(nn_npt,nn_sta),reseau_xcorr_mask(nn_npt,nn_sta))
  ALLOCATE(position(nn_sta,2),omega(nn_sta))

  CALL h5open_f(error)

  ! Read position and name of stations from reference file reseau_64.h5 and name.txt (from /data/ondes/rouxphi/yehuda/june_2014/reseau_64.mat)
  CALL h5fopen_f("reseau_64.h5", H5F_ACC_RDONLY_F, infile_id, error)
  startin1=(/0/)
  countin1=(/nn_sta/)
  CALL read_one_trace(infile_id,'x',1,startin1,countin1,nn_sta,xx)
  CALL read_one_trace(infile_id,'y',1,startin1,countin1,nn_sta,yy)
  CALL h5fclose_f(infile_id, error)
  OPEN(10, file="name.txt")
  DO jj=1,nn_sta
    READ(10,*) staname(jj)
  ENDDO
  CLOSE(10)

  ! Prepare mask and time_corr array
  ALLOCATE(kx(Nb_points),kxkx(Nb_points),mask(Nb_points,Nb_points))
  increment=1.d0/(100.d0*150.d0)
  kx = (/(-1.d0/200.d0+((ii-1)*increment),ii=1,Nb_points)/) 
  DO ii=1,Nb_points
    kxkx(ii)=kx(ii)*kx(ii)
  ENDDO
  sigma=1.d0/sigma_inv ! 700.d0
  inv_deux_sigma_sigma = ( sigma_inv * sigma_inv ) / 2.d0
  ALLOCATE(hann025(Nb_points),hannwithzero(Nb_points+2))
  CALL hanning_window(hannwithzero,Nb_points+2)
  DO ii=1,Nb_points
    hann025(ii)=hannwithzero(ii+1)**0.25d0  ! remove the zero at the begin and end of hanning window
  ENDDO
  DEALLOCATE(hannwithzero)
  DO ii=1,Nb_points
    DO pp=1,Nb_points
      tmp = -(kxkx(ii)+kxkx(pp)) * inv_deux_sigma_sigma
      mask(pp,ii)=(1.d0-EXP(tmp)) * (hann025(ii)*hann025(pp))
    ENDDO
  ENDDO
  time_corr = (/(-2.d0+((jt-1)*1.d0/nn_Fs),jt=1,nn_npt)/)
  deltat = time_corr(nn_npt)-time_corr(1)

  ! Define how many frequency bands and create matrice_int
  nfreq=0
  tmp = DBLE(Freq_interval(1))
  DO WHILE ( tmp .LE. DBLE(Freq_interval(2)) )
    tmp=tmp+1.d0/deltat
    nfreq=nfreq+1
  ENDDO
  ALLOCATE(freq_vect(nfreq),matrice_int(nn_npt,nfreq))
  tmp = DBLE(Freq_interval(1))
  nfreq=0
  DO WHILE ( tmp .LE. DBLE(Freq_interval(2)) )
    nfreq=nfreq+1
    freq_vect(nfreq)=tmp
    tmp=tmp+1.d0/deltat
  ENDDO
  DO jf=1,nfreq
    cplxtmp = 2.d0*pi*cj*freq_vect(jf)
    DO jt=1,nn_npt
      matrice_int(jt,jf)=EXP(cplxtmp * time_corr(jt))
    ENDDO
  ENDDO

  ALLOCATE(reseau_xcorr_freq(nn_sta))
  ALLOCATE(reseau_xcorr_freq_mask(nn_sta,nfreq))

  ! Open input file
  !CALL h5fopen_f("/media/luke4_scratch_md1200/bouep/"//TRIM(cn_freq_name)//"-DAILYMEAN/SG.DAILYMEAN.2014150.000000.h5", H5F_ACC_RDONLY_F, infile_id, error)
  CALL h5fopen_f(TRIM(cn_inputfile), H5F_ACC_RDONLY_F, infile_id, error)
  startin1=(/0/)
  countin1=(/nn_npt/)

  ! Create output file
  ! Create a new file using default properties.
  CALL h5fcreate_f(TRIM(cn_freq_name)//"_denoised.h5", H5F_ACC_TRUNC_F, outfile_id, error)
  ! Create the data space for the  dataset. 
  CALL h5screate_simple_f(rank, dimsf, dataspace, error)
  ! Create the dataset with default properties.
  CALL h5dcreate_f(outfile_id, "/reseau_xcorr_mask", H5T_NATIVE_DOUBLE, dataspace, dset_id, error)
  ! Close the dataspace.
  CALL h5sclose_f(dataspace, error)

  CALL date_and_time(VALUES=values)
  elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)

  DO jj=1,nn_sta

    ! Compute the position on the grid centered on jj reference station
    DO ji=1,nn_sta
      CALL azimuth_and_dist(distance_elements,angle,yy(ji),yy(jj),xx(ji),xx(jj))
      position(ji,1)=distance_elements*SIN(pi_over_180*angle)
      position(ji,2)=distance_elements*COS(pi_over_180*angle)
    ENDDO

    ! Read from input file the correlations with jj reference station
    DO ji=1,nn_sta  
      CALL sort_string(staname(ji),staname(jj),nom_paire)
      CALL h5lexists_f(infile_id, "/"//TRIM(nom_paire), link_exists, error, H5P_DEFAULT_F)
      IF ( link_exists ) THEN
        CALL read_one_trace(infile_id,"/"//TRIM(nom_paire)//"/EPZ_EPZ/corr",1,startin1,countin1,nn_npt,corr) 
        maxabs=MAXVAL(ABS(corr(:)))
        reseau_xcorr(:,ji)=corr(:)/maxabs
      ELSE
        reseau_xcorr(:,ji)=0.d0
      ENDIF
    ENDDO

    reseau_xcorr_freq_mask(:,:)=(0.d0,0.d0)

    DO jf=1,nfreq
      DO ji=1,nn_sta
        cplxsomme=(0.d0,0.d0)
        DO jt=1,nn_npt
          cplxsomme=cplxsomme+reseau_xcorr(jt,ji)*matrice_int(jt,jf)
        ENDDO
        reseau_xcorr_freq(ji)=cplxsomme
      ENDDO
!  normeL2 omega est constnte qqs jf,ii,pp ???
      cplxtmp = cj * 2  * pi * freq_vect(jf)
      DO ii=1,Nb_points
        DO pp=1,Nb_points
          DO ji=1,nn_sta
            omega(ji) = EXP( -cplxtmp * ( position(ji,1)*kx(ii) + position(ji,2)*kx(pp) ) )
          ENDDO
          inv_norm_omega = 1.d0 / NORM2((/NORM2(dble(omega(:))),NORM2(dimag(omega(:)))/))
          cplxsomme=(0.d0,0.d0)
          DO ji=1,nn_sta
            cplxsomme=cplxsomme+omega(ji) * inv_norm_omega * reseau_xcorr_freq(ji)
          ENDDO
          beamforming = cplxsomme * mask(ii,pp)
          reseau_xcorr_freq_mask(:,jf) = reseau_xcorr_freq_mask(:,jf) + CONJG(omega(:)) * inv_norm_omega * beamforming
        ENDDO
      ENDDO
      reseau_xcorr_freq_mask(:,jf) = reseau_xcorr_freq_mask(:,jf) / MAXVAL(ABS(reseau_xcorr_freq_mask(:,jf)))
    ENDDO

    ! Rebuild one slice of correlations with reference station
! matrice_int [401,4]
! reseau_xcorr_freq_mask [1108,4]
! reseau_xcorr_mask [401,1108]
    reseau_xcorr_mask(:,:) = MATMUL(CONJG(matrice_int(:,:)),TRANSPOSE(reseau_xcorr_freq_mask(:,:)))  ! dim a intervertir ???
    DO ji=1,nn_sta
      invmaxabs=1.d0/MAXVAL(ABS(reseau_xcorr_mask(:,ji)))
      DO jt=1,nn_npt
        reseau_xcorr_mask(jt,ji)=reseau_xcorr_mask(jt,ji)*invmaxabs
      ENDDO
    ENDDO

    ! Get dataset's dataspace identifier and select subset.
    CALL h5dget_space_f(dset_id, dataspace, error)
    offset = (/0,0,jj-1/) ! Hyperslab offset
    CALL h5sselect_hyperslab_f(dataspace, H5S_SELECT_SET_F, offset, count, error) 
    ! Create memory dataspace.
    CALL h5screate_simple_f(mrank, dimsm, memspace, error)
    ! Write subset to dataset  
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, reseau_xcorr_mask(:,:), data_dims, error, memspace, dataspace)
    CALL h5sclose_f(dataspace, error)
    CALL h5sclose_f(memspace, error)

    CALL date_and_time(VALUES=values)
    PRINT '("TOTAL        : Total Elapsed-time jj:",I4," : ",f20.6," s")', & 
            jj,values(5)*3600+values(6)*60+values(7)+0.001*values(8)-elps
    elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)

  ENDDO

  ! Close the dataspace and dataset and file.
  CALL h5dclose_f(dset_id, error)
  CALL h5fclose_f(outfile_id, error)

  CALL h5fclose_f(infile_id, error)

  CALL h5close_f(error)

END PROGRAM DENOISING

SUBROUTINE sort_string(str1,str2,strsort)

  IMPLICIT NONE

  CHARACTER(LEN = *), INTENT(in)  :: str1,str2
  CHARACTER(LEN = *), INTENT(out) :: strsort

  IF ( LGT(str1,str2) ) THEN
    strsort=str2//'_'//str1
  ELSE
    strsort=str1//'_'//str2
  ENDIF

END SUBROUTINE sort_string

SUBROUTINE hanning_window(w,n) 

    IMPLICIT NONE

    INTEGER, INTENT(in) :: n
    REAL(KIND=8), DIMENSION(n), INTENT(out) :: w

    REAL(KIND=8) :: pin
    INTEGER :: i

    pin=8.d0*ATAN(1.d0)/DBLE(n-1) 
    DO i=1,n
      w(i)=0.5d0*(1.d0-COS(pin*DBLE(i-1))) 
    ENDDO

END SUBROUTINE hanning_window
