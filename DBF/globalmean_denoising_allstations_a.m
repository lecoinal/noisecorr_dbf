clear all

addpath /data/ondes/rouxphi/yehuda/june_2014/

load EXPA_Frequence_plus.mat
choix_freq=1;
freq_name=['EXPA0' num2str(Freq_centrale(choix_freq)*10)];
    
% if Freq_centrale(choix_freq)<10
%     freq_name=['EXPA0' num2str(Freq_centrale(choix_freq)) ];
% else
%     freq_name=['EXPA' num2str(Freq_centrale(choix_freq)) ];
% end

load stations_SanJacinto_update.mat
all_stations=char(Name);
all_stations(:,4)=[];
clear Name

freq_name_bis='EXPA03';
eval(['load list_' freq_name_bis '_150.mat'])

first=list(1,1:5);
tag=1;
while isequal(list(tag,1:5),first)==1
    stations(tag,:)=list(tag,7:11);
    tag=tag+1;
end
clear first tag

for ii=1:length(stations)
    test1=1;
    num=1;
    while num>=0 && test1<=length(x)
        [I J]=find(isequal(stations(ii,:),all_stations(test1,:)));
        if isempty(I)==0
            num=-1;
        else
            test1=test1+1;
        end
        clear I J
    end
    match(ii)=test1;
end
I=find(match==(length(x)+1));
x(I)=[];
y(I)=[];
z(I)=[];
all_stations(I,:)=[];
name=char(all_stations);
clear I stations match list

eval(['load ' freq_name_bis '_JD150.mat DD_data Fs time_corr'])
%directory=['/data/ondes/rouxphi/yehuda/june_2014/' freq_name '/DAILYMEAN/'];
directory=['/media/luke4_scratch_md1200/bouep/' freq_name '-DAILYMEAN/'];
%directory=['/nfs_scratch/bouep/SJ_CORR/' freq_name '-DAILYMEAN/'];

affichage=0;

bad=zeros(size(all_stations,1));    
for qqq=1:1:size(all_stations,1)

    disp(qqq)
    
    XX=x(qqq);
    YY=y(qqq);
    
    pos_center_lon=XX;
    pos_center_lat=YY;
    
    for ii=1:size(all_stations,1)
        [distance_elements(ii),angle(ii)]=dist_wh([y(ii) pos_center_lat],[x(ii) pos_center_lon]);
    end
    position(:,1)=distance_elements.*sind(angle);
    position(:,2)=distance_elements.*cosd(angle);
    
    if affichage==1
        figure(1);clf
        plot(x,y,'ro')
        hold on
        plot(pos_center_lon,pos_center_lat,'ko')
        title(num2str(qqq))
        drawnow
        pause(.1)
    end
        
    eval(['fid=H5F.open(''' directory DD_data(1).name ''');'])
    for kk=1:size(all_stations,1)
        
        if str2num(name(kk,2:3))<str2num(name(qqq,2:3))
            nom_paire=[name(kk,:) '_' name(qqq,:)];
        elseif str2num(name(kk,2:3))>str2num(name(qqq,2:3))
            nom_paire=[name(qqq,:) '_' name(kk,:)];
        else
            if str2num(name(kk,4:5))<str2num(name(qqq,4:5))
                nom_paire=[name(kk,:) '_' name(qqq,:)];
            else
                nom_paire=[name(qqq,:) '_' name(kk,:)];
            end
        end
        
        eval(['bool=H5L.exists(fid,[''/'',nom_paire],''H5P_DEFAULT'');'])
        if bool==1
            eval(['corr=h5read(''' directory DD_data(1).name ''',[''/'',nom_paire,''/EPZ_EPZ/corr'']);'])
            corr=double(corr);
            corr=corr/max(abs(corr));
        else
            corr=zeros(length(time_corr),1);
            bad(kk,qqq)=1;
        end
        clear bool
        
        reseau_xcorr(:,kk)=corr;%double(corr);%double(0.5*(corr+flipud(corr)));%
    end
    H5F.close(fid)
    clear fid corr
    
    [II JJ]=sort(distance_elements);
    
    if affichage==1
        figure(2);clf
        imagesc(time_corr,1:size(all_stations,1),reseau_xcorr(:,JJ)')
        colorbar
        set(gca,'ydir','normal')
        title(num2str(qqq))
        drawnow
        pause(.1)
    end
    
    if qqq==1
        Nb_points=151;
        kx=linspace(-1/200,1/200,Nb_points);
        ky=kx;
        
        mask=zeros(length(kx),length(ky));
        sigma=1/700;
        mask=ones(length(kx),length(ky))-exp(-kx.^2/2/sigma^2)'*exp(-ky.^2/2/sigma^2);
        mask=mask.*((hanning(Nb_points).^0.25)*(hanning(Nb_points).^0.25)');
        
        bandwidth=Freq_interval(choix_freq,:);
        deltat=max(time_corr)-min(time_corr);
        
        freq_vect=min(bandwidth):1/deltat:max(bandwidth);
        matrice_int=exp(2*pi*1i*time_corr'*freq_vect);
    end
    
    reseau_xcorr_freq=reseau_xcorr'*matrice_int;
    
    beamforming_kx_ky=zeros(length(kx),length(ky),length(freq_vect));
    beamforming_kx_ky_mask=zeros(length(kx),length(ky),length(freq_vect));
    reseau_xcorr_freq_mask=zeros(size(reseau_xcorr_freq));

    for jj=1:length(freq_vect)
        
        for ii=1:length(kx)
            for pp=1:length(ky)
                omega=exp(-1i*2*pi*freq_vect(jj)*(position(:,1)*kx(ii)+position(:,2)*ky(pp)));
                omega=omega/norm(omega);
                
                beamforming_ky_ky(ii,pp,jj)=omega.'*reseau_xcorr_freq(:,jj);
            end
        end
        
        affichage=0;
        if affichage==1
            figure(10);clf
            imagesc(kx,ky,abs(beamforming_ky_ky(:,:,jj))')
            colorbar
            grid on
            title(num2str(freq_vect(jj)))
            drawnow
            pause(.1)
        end
        
        beamforming_kx_ky_mask(:,:,jj)=beamforming_ky_ky(:,:,jj).*mask;
        
        affichage=0;
        if affichage==1
            figure(11);clf
            imagesc(kx,ky,abs(beamforming_kx_ky_mask(:,:,jj))')
            colorbar
            grid on
            title(num2str(freq_vect(jj)))
            pause(1)
            drawnow
            pause(.1)
        end
        
        for ii=1:length(kx)
            for pp=1:length(ky)
                omega=exp(1i*2*pi*freq_vect(jj)*(position(:,1)*kx(ii)+position(:,2)*ky(pp)));
                omega=omega/norm(omega);
                
                reseau_xcorr_freq_mask(:,jj)=reseau_xcorr_freq_mask(:,jj)+omega*beamforming_kx_ky_mask(ii,pp,jj);
            end
        end
        reseau_xcorr_freq_mask(:,jj)=reseau_xcorr_freq_mask(:,jj)./max(abs(reseau_xcorr_freq_mask(:,jj)));
    end
    
    if qqq==1
        reseau_xcorr_mask=zeros(length(time_corr),size(all_stations,1),size(all_stations,1));
    end
    
    reseau_xcorr_mask(:,:,qqq)=real(conj(matrice_int)*reseau_xcorr_freq_mask.');
    reseau_xcorr_mask(:,:,qqq)=reseau_xcorr_mask(:,:,qqq)./(ones(length(time_corr),1)*max(abs(reseau_xcorr_mask(:,:,qqq)),[],1));
    
    affichage=0;
    if affichage==1
        figure(12);clf
        imagesc(time_corr,1:size(all_stations,1),reseau_xcorr_mask(:,JJ,qqq)')
        colorbar
        title(num2str(qqq))
        set(gca,'ydir','normal')
        drawnow
        pause(.1)
    end
    
    clear reseau_xcorr_freq reseau_xcorr_freq_mask reseau_xcorr beamforming* omega angle distance_elements position II JJ   
end

clear matrice_int
eval(['save ' freq_name '_denoised.mat -v7.3'])

