!###############################################################################
!#    This file is part of noisecorr_dbf : a Fortran code for computing        #
!#    double beamforming on the dataset from San Jacinto Fault zone            #
!#    dense array of geophones                                                 #
!#                                                                             #
!#    Copyright (C) 2015 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################
!
! https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Double_beamforming

MODULE glovarmodule

  IMPLICIT NONE

  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: reseau_xcorr_svd
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: reseau_xcorr_svd_trie
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: distance_elements_1d
  REAL(KIND=8) :: distance
  INTEGER :: nn_npt,nn_Fs,nn_taille_reseau,nn_taille_reseau2
  LOGICAL, DIMENSION(:), ALLOCATABLE :: lisnan
  LOGICAL, DIMENSION(:), ALLOCATABLE :: lisnan_trie
  REAL(KIND=8),DIMENSION(:,:), ALLOCATABLE :: position1,position2
  INTEGER :: N
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE  :: toto_svd_trie
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: llpowtestm1
  INTEGER, PARAMETER :: Ncoeffs=4  ! fn_optimize_amplitude_new
  INTEGER :: NSA ! fn_optimize_amplitude_new
  INTEGER, DIMENSION(Ncoeffs) :: Max_coeffs,Min_coeffs
  REAL(KIND=8), PARAMETER :: pi_over_180 = 4.d0 * DATAN(1.d0) / 180.d0

END MODULE glovarmodule
