!###############################################################################
!#    This file is part of noisecorr_dbf : a Fortran code for computing        #
!#    double beamforming on the dataset from San Jacinto Fault zone            #
!#    dense array of geophones                                                 #
!#                                                                             #
!#    Copyright (C) 2015 Albanne Lecointre                                     #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################
!
! https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Double_beamforming

PROGRAM DBF
  USE HDF5
  USE H5LT      ! Lite Interface HDF5
  USE m_mrgrnk  ! Sorting and Ranking ORDERPACK lib (licence ?)
  USE dbfmodule
  IMPLICIT NONE

  INTEGER :: nn_taille_reseau
  INTEGER :: nn_Fs,nn_npt
  REAL(KIND=8) :: Freq_centrale
  REAL(KIND=8), DIMENSION(2) :: Freq_interval
  CHARACTER(LEN=6) :: cn_freq_name 
  REAL(KIND=8), DIMENSION(2) :: interval_distance
  REAL(KIND=8) :: angle_main,interval_angle
  INTEGER :: nn_maxi_compteur,nn_ibeg,nn_iend,nn_istp,nn_jend,nn_jstp,nn_max_compte

  REAL(KIND=8), DIMENSION(2) :: bandwidth
  CHARACTER(LEN=2) :: cn_taille_reseau
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: time_corr
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: lenteur_pos,lenteur_neg
  INTEGER :: ii,jj,kk,ll,ic
  INTEGER :: surech,error
  INTEGER(HID_T) :: file_id,infile_id      ! Input File identifier
  INTEGER(HSIZE_T), DIMENSION(2) :: startin 
  INTEGER(HSIZE_T), DIMENSION(2) :: countin
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: reseauxii,reseauyii,reseauxjj,reseauyjj
  REAL(KIND=8) :: distance
  REAL(KIND=8), DIMENSION(1) :: pos_center_lon1,pos_center_lat1,pos_center_lon2,pos_center_lat2
  CHARACTER(len=5), DIMENSION(:), ALLOCATABLE :: name1,name2
  CHARACTER(len=11) :: nom_paire
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: distance_elements
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: corr_sp
  integer(HSIZE_T), dimension(1) :: dims ! size of the corr_sp buffer
  integer(HSIZE_T), dimension(2) :: dims2
  REAL(KIND=8), DIMENSION(:,:,:), ALLOCATABLE :: reseau_xcorr
  LOGICAL :: link_exists
  INTEGER :: cpt_valid_corr
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: distance_elements_1d
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: reseau_xcorr_2d
  INTEGER, DIMENSION(:), ALLOCATABLE :: idx_sort
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: distance_elements_trie
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: reseau_xcorr_trie
  INTEGER :: compteur,compte
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: diff_distance
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: reseau_maxi_pos,reseau_maxi_neg
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: signal_moyen
  INTEGER, DIMENSION(2) :: IIIJJJpos,IIIJJJneg
  INTEGER :: index_length
  INTEGER, DIMENSION(:), ALLOCATABLE :: index
  REAL(KIND=8) :: AF,t0,t1
  INTEGER, DIMENSION(8) :: values
  LOGICAL :: l_exist_namelist

  ! Outputs
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: temps_extrait,vitesse_extrait,distance_reseaux,maximum_extrait,lenteur_extrait
  REAL(KIND=4), DIMENSION(:,:), ALLOCATABLE :: position_reseaux

  ! Declare NAMELIST
  NAMELIST /naminput/ Freq_centrale,nn_Fs,nn_npt,cn_freq_name,Freq_interval
  NAMELIST /namparam/ nn_taille_reseau,angle_main,interval_angle,interval_distance
  NAMELIST /namloop/ nn_ibeg,nn_iend,nn_istp,nn_jend,nn_jstp,nn_maxi_compteur,nn_max_compte

  ! Read namelist
  INQUIRE(file='namelist', exist=l_exist_namelist)
  IF (.NOT. l_exist_namelist) THEN
     PRINT *,'The namelist does not exist'
     STOP
  ENDIF
  OPEN(11,file='namelist')
    REWIND 11
    READ(11,naminput)
    REWIND 11
    READ(11,namparam)
    REWIND 11
    READ(11,namloop)
  CLOSE(11)

  ALLOCATE(time_corr(nn_npt))
  time_corr = (/((DBLE(ii-201)/DBLE(nn_Fs)),ii=1,nn_npt)/)

  ALLOCATE(lenteur_pos(nn_taille_reseau),lenteur_neg(nn_taille_reseau))
  CALL linspace(lenteur_pos,1.d0/4000.d0,1.d0/400.d0,nn_taille_reseau)
  CALL linspace(lenteur_neg,-1.d0/4000.d0,-1.d0/400.d0,nn_taille_reseau)

  bandwidth=Freq_interval

  surech=1
  DO WHILE ( DBLE(surech*nn_Fs)/Freq_centrale/20.d0 < 1.d0 )
    surech=surech+1
  ENDDO

  compte=1

  CALL h5open_f(error)
  write(cn_taille_reseau,'(I0.2)') nn_taille_reseau
  CALL h5fopen_f("reseau_"//TRIM(cn_taille_reseau)//".h5", H5F_ACC_RDONLY_F, infile_id, error)

  !ALLOCATE(corr(nn_npt),corr_sp(nn_npt))
  ALLOCATE(corr_sp(nn_npt))
  ALLOCATE(reseau_maxi_pos(nn_npt,nn_taille_reseau),reseau_maxi_neg(nn_npt,nn_taille_reseau),signal_moyen(nn_npt))
  index_length=NINT(DBLE(nn_Fs)/DBLE((bandwidth(2)-bandwidth(1))))
  ALLOCATE(index(2*index_length+1))
  index = (/( ii , ii=-index_length,index_length )/)
  ALLOCATE(reseauxii(nn_taille_reseau),reseauyii(nn_taille_reseau),reseauxjj(nn_taille_reseau),reseauyjj(nn_taille_reseau))
  ALLOCATE(name1(nn_taille_reseau),name2(nn_taille_reseau))
  ALLOCATE(reseau_xcorr(nn_npt,nn_taille_reseau,nn_taille_reseau))
  ALLOCATE(distance_elements(nn_taille_reseau,nn_taille_reseau))
  ALLOCATE(temps_extrait(nn_max_compte),vitesse_extrait(nn_max_compte),distance_reseaux(nn_max_compte), &
           maximum_extrait(nn_max_compte),lenteur_extrait(nn_max_compte))
  ALLOCATE(position_reseaux(4,nn_max_compte))

  ! CALL date_and_time(values=values)
  t0=0.d0

  countin=(/nn_taille_reseau,1/)
  dims=(/nn_npt/)

  !DO ii=778,778
  DO ii=nn_ibeg,nn_iend,nn_istp

    ! Open input file reseau_64.h5 and read one column of reseaux and reseauy  !! CHECK that memory contiguous
    startin=(/0,ii-1/)
    CALL read_one_trace(infile_id,'reseaux',2,startin,countin,nn_taille_reseau,reseauxii)
    pos_center_lon1(1)=SUM(reseauxii)/DBLE(nn_taille_reseau)
    CALL read_one_trace(infile_id,'reseauy',2,startin,countin,nn_taille_reseau,reseauyii)
    pos_center_lat1(1)=SUM(reseauyii)/DBLE(nn_taille_reseau)

    !DO jj=1054,1054
    !DO jj=nn_jbeg,nn_jend,nn_jstp
    DO jj=ii,nn_jend,nn_jstp

      startin=(/0,jj-1/)
      CALL read_one_trace(infile_id,'reseaux',2,startin,countin,nn_taille_reseau,reseauxjj)
      pos_center_lon2(1)=SUM(reseauxjj)/DBLE(nn_taille_reseau)
      CALL read_one_trace(infile_id,'reseauy',2,startin,countin,nn_taille_reseau,reseauyjj)
      pos_center_lat2(1)=SUM(reseauyjj)/DBLE(nn_taille_reseau)

      CALL dist_wh(distance,pos_center_lat1(1),pos_center_lat2(1),pos_center_lon1(1),pos_center_lon2(1))

      CALL azimuth(AF,pos_center_lat1(1),pos_center_lat2(1),pos_center_lon1(1),pos_center_lon2(1))

!write (*,'(A5,I4,A3,I4,A8,F7.3,A5,F8.3)'),'Sp = ',ii,' - ',jj,' , AF = ',AF,' d = ',distance

! not(A or B or C) = not(A) and not(B) and not(C)
      IF ( (distance>=interval_distance(1)) .AND. & 
           (distance<=interval_distance(2)) .AND. &
           (ABS(AF-angle_main)<=interval_angle) ) THEN

        print *,'Station pair = ',ii,' - ',jj, ' , AF = ',AF   
        CALL date_and_time(values=values)
        t1 = DBLE(values(5))*3600.d0+DBLE(values(6))*60.d0+DBLE(values(7))+0.001d0*DBLE(values(8))
        print *,'elapsed time = ',t1-t0
        t0 = t1

        open(11, file="reseauname.txt", form="formatted",access="direct", recl=6,action="read")  ! un mot = 6 caractere (5car+1blanc) 
        read(11,'(A5)',rec=1+(ii-1)*nn_taille_reseau) name1  ! on lit la ii-ieme ligne
        read(11,'(A5)',rec=1+(jj-1)*nn_taille_reseau) name2  ! on lit la jj-ieme ligne
        close(11)

        CALL h5fopen_f('/media/luke4_scratch_md1200/lecointre/SAN_JACINTO/CORRELATION/'//cn_Freq_name//'/DAILYMEAN/SG.DAILYMEAN.2014150.000000.h5', H5F_ACC_RDONLY_F, file_id, error)

!        reseau_xcorr(:,:,:)=0.d0  ! cette init est inutile ?
        cpt_valid_corr=0
        DO kk=1,nn_taille_reseau
          DO ll=1,nn_taille_reseau

            CALL dist_wh(distance_elements(kk,ll),reseauyii(kk),reseauyjj(ll),reseauxii(kk),reseauxjj(ll))

            CALL sort_string(name1(kk),name2(ll),nom_paire)

            CALL h5lexists_f(file_id, "/"//TRIM(nom_paire), link_exists, error, H5P_DEFAULT_F)
            IF ( link_exists ) THEN
              CALL h5ltread_dataset_float_f(file_id, '/'//TRIM(nom_paire)//'/EPZ_EPZ/corr', corr_sp, dims, error)
              reseau_xcorr(:,kk,ll)=DBLE(corr_sp(:))/MAXVAL(ABS(DBLE(corr_sp(:))))
              cpt_valid_corr=cpt_valid_corr+1
            ELSE
              reseau_xcorr(:,kk,ll)=9999.d0 ! bad value
              distance_elements(kk,ll)=-1.d0 ! bad value
              !! un des deux masks est inutile
            ENDIF

          ENDDO
        ENDDO
        CALL h5fclose_f(file_id, error)
  
        ALLOCATE(reseau_xcorr_2d(nn_npt,cpt_valid_corr))
        ALLOCATE(distance_elements_1d(cpt_valid_corr))
        distance_elements_1d = pack(distance_elements, mask = distance_elements>=0.d0)
        DO ic=1,nn_npt
          reseau_xcorr_2d(ic,:) = pack(reseau_xcorr(ic,:,:), mask = reseau_xcorr(ic,:,:) < 9998.d0)
        ENDDO
        !! ici on pourrait utiliser le m�me mask pour les deux !


      ! Ranking from ORDERPACK2.0
!!! Note: Taken from ORDERPACK 2.0
!!! http://www.fortran-2000.com/ downloaded 2015-06-25
        ALLOCATE(idx_sort(cpt_valid_corr))
        CALL mrgrnk (distance_elements_1d, idx_sort)
        ALLOCATE(distance_elements_trie(cpt_valid_corr))
        ALLOCATE(reseau_xcorr_trie(nn_npt,cpt_valid_corr))
        DO ic=1,cpt_valid_corr
          distance_elements_trie(ic)=distance_elements_1d(idx_sort(ic))
          reseau_xcorr_trie(:,ic)=reseau_xcorr_2d(:,idx_sort(ic))
        ENDDO
        DEALLOCATE(reseau_xcorr_2d,distance_elements_1d,idx_sort) 

        ALLOCATE(diff_distance(cpt_valid_corr))
        DO ic=1,cpt_valid_corr
          diff_distance(ic)=distance_elements_trie(ic)-distance
        ENDDO
        DEALLOCATE(distance_elements_trie)
        compteur=0
write(*,'(A99)'),'compte temps   vitesse                position_reseaux               distance    maximum    lenteur'
        DO while ( compteur<nn_maxi_compteur )

!===!             %%%Lenteur positive

          CALL lenteur(nn_taille_reseau,nn_npt,cpt_valid_corr,nn_Fs,surech,diff_distance,lenteur_pos,reseau_xcorr_trie,  &
                        signal_moyen,IIIJJJpos,maximum_extrait(compte))
 ! en fait pour reseau_maxi_pos on ne veut que le IIIJJJpos2
 ! en fait pour reseau_maxi_pos_env on en veut qu'un

          compteur=compteur+1

!===!             %%%%extraction positive

          CALL extraction(2*index_length+1,nn_npt,nn_Fs,surech,cpt_valid_corr, & 
                          IIIJJJpos,index,diff_distance,lenteur_pos(IIIJJJpos(2)),signal_moyen, & 
                          reseau_xcorr_trie)

          temps_extrait(compte)=REAL(time_corr(IIIJJJpos(1)))
          vitesse_extrait(compte)=REAL(distance/time_corr(IIIJJJpos(1)))
          position_reseaux(:,compte)=REAL((/pos_center_lon1(1),pos_center_lon2(1),pos_center_lat1(1),pos_center_lat2(1)/))
          distance_reseaux(compte)=REAL(distance)
          lenteur_extrait(compte)=REAL(lenteur_pos(IIIJJJpos(2)))

write (*,130),compte,temps_extrait(compte),vitesse_extrait(compte),position_reseaux(:,compte),distance_reseaux(compte),maximum_extrait(compte),lenteur_extrait(compte)
130 format (I6,1X,F5.2,2X,F8.1,2X,6(F9.4,2X),F9.5)

          compte=compte+1

!===!             %%%Lenteur negative

          CALL lenteur(nn_taille_reseau,nn_npt,cpt_valid_corr,nn_Fs,surech,diff_distance,lenteur_neg,reseau_xcorr_trie,  &
                        signal_moyen,IIIJJJneg,maximum_extrait(compte))

          compteur=compteur+1
  
!===!             %%%%extraction negative
  
          CALL extraction(2*index_length+1,nn_npt,nn_Fs,surech,cpt_valid_corr, & 
                          IIIJJJneg,index,diff_distance,lenteur_neg(IIIJJJneg(2)),signal_moyen, & 
                          reseau_xcorr_trie)

          temps_extrait(compte)=REAL(time_corr(IIIJJJneg(1)))
          vitesse_extrait(compte)=REAL(distance/time_corr(IIIJJJneg(1)))
          position_reseaux(:,compte)=REAL((/pos_center_lon1(1),pos_center_lon2(1),pos_center_lat1(1),pos_center_lat2(1)/))
          distance_reseaux(compte)=REAL(distance)
          lenteur_extrait(compte)=REAL(lenteur_neg(IIIJJJneg(2)))

write (*,130),compte,temps_extrait(compte),vitesse_extrait(compte),position_reseaux(:,compte),distance_reseaux(compte),maximum_extrait(compte),lenteur_extrait(compte)

          compte=compte+1

        ENDDO  ! while compteur
        DEALLOCATE(reseau_xcorr_trie,diff_distance)
      ENDIF
    ENDDO
  ENDDO

  CALL h5fclose_f(infile_id, error)

  CALL h5fcreate_f(cn_freq_name//"_JD150_processed.h5",H5F_ACC_TRUNC_F,file_id,error)
  dims=(/1/)
  CALL h5ltmake_dataset_int_f(file_id,"compte",1,dims,(/compte/),error)
  CALL h5ltmake_dataset_int_f(file_id,"number",1,dims,(/nn_taille_reseau/),error)
  dims = (/compte-1/)
  CALL h5ltmake_dataset_float_f(file_id,"vitesse_extrait",1,dims,vitesse_extrait(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"temps_extrait",1,dims,temps_extrait(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"lenteur_extrait",1,dims,lenteur_extrait(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"distance_reseaux",1,dims,distance_reseaux(1:compte-1),error)
  CALL h5ltmake_dataset_float_f(file_id,"maximum_extrait",1,dims,maximum_extrait(1:compte-1),error)
  dims2=(/4,compte-1/)
  CALL h5ltmake_dataset_float_f(file_id,"position_reseaux",2,dims2,position_reseaux(:,1:compte-1),error)
  CALL h5fclose_f(file_id,error)

  CALL h5close_f(error)

END PROGRAM DBF
!  WRITE(rowfmt,'(A,I4,A)') '(',cpt_valid_corr,'(1X,F10.4))'
!  write(ccompteur,'(I0.1)') compteur
!  write(ckk,'(I0.2)') kk
!  OPEN(UNIT=12, FILE=TRIM(ccompteur)//"_"//TRIM(ckk)//"_reseau_xcorr_svd_DBF_pos.txt", ACTION="write", STATUS="replace")
!  DO iii=1,nn_npt
!    WRITE(12,FMT=rowfmt) (reseau_xcorr_svd_DBF_pos(iii,jjj), jjj=1,cpt_valid_corr)
!  END DO
!  CLOSE(12)
