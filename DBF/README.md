[Double beamforming](https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Double_beamforming)

---


```
> source /applis/ciment/guix-start.bash
```

We use the [GUIX manifest file manifest_dbf.scm](./manifest_dbf.scm)

```
> guix package -m manifest_dbf.scm -p $GUIX_USER_PROFILE_DIR/dbf
```

And then for each session:
```
> source /applis/ciment/guix-start.bash
> refresh_guix dbf
```

Install in your $HOME directory brent and asa047 libraries:


```
> gfortran -v
Using built-in specs.
COLLECT_GCC=gfortran
COLLECT_LTO_WRAPPER=/gnu/store/9dfwr7gh59iwg2wary3w853rnjzzk3r7-gfortran-10.3.0/libexec/gcc/x86_64-unknown-linux-gnu/10.3.0/lto-wrapper
Target: x86_64-unknown-linux-gnu
Configured with: 
Thread model: posix
Supported LTO compression algorithms: zlib
gcc version 10.3.0 (GCC) 

> wget https://people.math.sc.edu/Burkardt/f_src/f90split/f90split.f90
> gfortran -o f90split f90split.f90
> rm f90split.f90

> wget https://people.math.sc.edu/Burkardt/f_src/brent/brent.f90
> ./f90split brent.f90
> rm brent.f90 f90split timestamp.f90   # to avoid multiple defintions of timestamp (already in asa047)

> wget https://people.sc.fsu.edu/~jburkardt/f77_src/asa047/asa047.f

> for f in $(ls *.f *.f90) ; do gfortran -c $f ; done
> ar qc $HOME/lib/gnu-devel-10.3.0/libbrentasa047.a *.o
> rm *.o 
```


And then compile:
```
> make
```
