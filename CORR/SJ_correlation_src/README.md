[Correlations](https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Correlations)

---

```
> source /applis/site/guix-start.sh
```

We use the [GUIX manifest file manifest_corr.scm](./manifest_corr.scm)
Edit this manifest and choose if you wan't build with single precision FFTW (fftwf guix package) or double precision FFTW (fftw guix package)

```
> guix package -m manifest_corr.scm -p $GUIX_USER_PROFILE_DIR/corr
```

And then for each session:
```
> source /applis/site/guix-start.sh
> refresh_guix corr
```

Edit the [Makefile](./Makefile) and choose for single or double precision FFTW.
You have to edit the line:
```
CPPK      = -Dlk_scaleoffset -Dlk_float    # single precision computation (recommended)
# or
CPPK      = -Dlk_scaleoffset               # double precision computation
```
And the line:
```
LIBFFTW  = -lfftw3f_threads -lfftw3f -lm   # single precision computation (recommended)
# or
LIBFFTW  = -lfftw3_threads -lfftw3 -lm     # double precision computation
```

And then compile:
```
> make
```
