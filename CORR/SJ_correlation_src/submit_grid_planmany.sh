#!/bin/bash
#OAR --project imagin
#OAR -l /nodes=1/cpu=1/core=1,walltime=96:00:00
#OAR -n cMC_planmany
#OAR -p network_address='luke4'

#  =======================================================================================
#   ***  Shell wrapper to submit corr_planmany correlation code in the CIMENT grid ***
#    
#  =======================================================================================
#               
#  ---------------------------------------------------------------------------------------

set -e

source /applis/ciment/guix-start.bash
# Suppose that GUIX profile "corr" was created with :
# guix install -p $GUIX_USER_PROFILE_DIR/corr gfortran-toolchain fftwf hdf5 hdf5:fortran zlib
# choose fftwf or fftw (single or double precision computation)
# Or with a GUIX manifest file
# guix package -m manifest_corr.scm -p $GUIX_USER_PROFILE_DIR/corr

refresh_guix corr

cat $OAR_NODE_FILE
NBCORE=$(cat $OAR_NODE_FILE | wc -l)
echo "NBCORE : $NBCORE"

HERE=$(pwd)

case "$CLUSTER_NAME" in
  luke)
    if [[ "$HOSTNAME" == "luke4" ]] ; then
      LDIR="/scratch_md1200/$USER/"
    else
      LDIR="/nfs_scratch2/$USER/"
    fi
    ;;
esac

TMPDIR="/$LDIR/CID_$CIGRI_CAMPAIGN_ID/oar.$OAR_JOB_ID"

mkdir -p $TMPDIR
cd $TMPDIR

YEAR=$2 # 2014
JDAY=$3 # 150
EXP=$4 # "EXP001"
nEXP=$5

F90EXE=corr_planmany

cp $HERE/$F90EXE $TMPDIR

NN_IO=1  # if only one daily corr...

sed -e "s/<<YEAR>>/$YEAR/" -e "s/<<JDAY>>/$JDAY/" \
    -e "s/<<NN_IO>>/$NN_IO/" -e "s/nn_nbthread = 0/nn_nbthread = $NBCORE/" $HERE/namelist.correlation.skel > $TMPDIR/namelist

ln -sf $LDIR/SAN_JACINTO/FINAL_PREPROCESS_${EXP}/SG_${YEAR}_${JDAY}.trace.int8.h5 . 

echo "CORRELATION: Correlations:                      $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"

$SHARED_SCRATCH_DIR/$USER/bin/time -f "\t%M Maximum resident set size (KB)"  \
    ./$F90EXE

mkdir $nEXP
mv c.h5 $nEXP/SG.${YEAR}${JDAY}.000000.h5

echo "CORRELATION: End job:                           $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"

