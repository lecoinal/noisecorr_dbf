#!/bin/bash

#  =======================================================================================
#   ***  Shell wrapper to submit correlation code in the CIMENT grid ***
#    
#  =======================================================================================
#               
#  ---------------------------------------------------------------------------------------

set -e

source /applis/ciment/guix-start.bash
# Suppose that GUIX profile "corr" was created with :
# guix install -p $GUIX_USER_PROFILE_DIR/corr gfortran-toolchain fftwf hdf5 hdf5:fortran zlib
# choose fftwf or fftw (single or double precision computation)
# Or with a GUIX manifest file
# guix package -m manifest_corr.scm -p $GUIX_USER_PROFILE_DIR/corr

refresh_guix corr

YEAR=$2   # 2014   # $1
JDAY=$3   # 145    # $2
EXP=$4    # Experiment name : for example EXPA02
#FPCID=$4    # Final preprocessing cigri campaign id

F90EXE=corr_planmany

cat $OAR_NODE_FILE
NBCORE=$(cat $OAR_NODE_FILE | wc -l)

case "$CLUSTER_NAME" in
  luke)
    if [[ "$HOSTNAME" == "luke4" ]] ; then 
      echo "$HOSTNAME ; working in MD1200 ; reading input from MD1200 "
      IDIR="/scratch_md1200/$USER/" # 40TB
      #echo "$HOSTNAME ; working in R720 ; reading input from R720 "
      #IDIR="/scratch_r720/$USER/"
      NN_IO=4
    else
      echo "$HOSTNAME ; working in NFS ; reading input from NFS "
      #IDIR="${SHARED_SCRATCH_DIR}/$USER/"; #/nfs_scratch : 40TB
      IDIR="/beegfs_scratch/$USER/";   # 40TB
      NN_IO=2
    fi
    TMPDIR="${IDIR}/oar.SJcorrgrid.$OAR_JOB_ID";
    ;;
esac

mkdir -p $TMPDIR
cp $F90EXE $TMPDIR

sed -e "s/<<YEAR>>/$YEAR/" -e "s/<<JDAY>>/$JDAY/" \
    -e "s/<<NN_IO>>/$NN_IO/" namelist.skel > $TMPDIR/namelist

cd $TMPDIR

ln -sf $IDIR/SAN_JACINTO/FINAL_PREPROCESS_${EXP}/SG_${YEAR}_${JDAY}.trace.int8.h5 .

echo "Correlations:                      $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"

./$F90EXE

echo "Move Results:                      $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"

#mkdir -p ${IDIR}/SAN_JACINTO/CORRELATION/${EXP}/
#mv c.h5 ${IDIR}/SAN_JACINTO/CORRELATION/${EXP}/SG.${YEAR}${JDAY}.000000.h5

echo "End job:                           $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"
