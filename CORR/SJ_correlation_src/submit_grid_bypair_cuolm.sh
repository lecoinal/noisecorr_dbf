#!/bin/bash
#OAR --project resolve
#OAR -l /nodes=1/cpu=1/core=1,walltime=96:00:00
#OAR -n cMC_bypair
##OAR -p network_address='luke4'

#  =======================================================================================
#   ***  Shell wrapper to submit corr_bypair correlation code in the CIMENT grid ***
#    
#  =======================================================================================
#               
#  ---------------------------------------------------------------------------------------

set -e

source /applis/site/guix-start.sh
# Suppose that GUIX profile "corr" was created with :
# guix install -p $GUIX_USER_PROFILE_DIR/corr gfortran-toolchain fftwf hdf5 hdf5:fortran zlib
# choose fftwf or fftw (single or double precision computation)
# Or with a GUIX manifest file
# guix package -m manifest_corr.scm -p $GUIX_USER_PROFILE_DIR/corr

refresh_guix corr

cat $OAR_NODE_FILE
NBCORE=$(cat $OAR_NODE_FILE | wc -l)
echo "NBCORE : $NBCORE"

HERE=$(pwd)

LDIR="$SHARED_SCRATCH_DIR/$USER/"

TMPDIR="/$LDIR/CID_$CIGRI_CAMPAIGN_ID/oar.$OAR_JOB_ID"

mkdir -p $TMPDIR
cd $TMPDIR

YEAR=2022 # $2 # 2014
JDAY=175 # $3 # 150
H=15 # $4

#EXP=$4 # "EXP001"
#nEXP=$5

HH=$(printf "%02d" $H)   # 2-digit hour

F90EXE=corr_bypair

cp $HERE/$F90EXE $TMPDIR

NN_IO=1  # not used # if only one daily corr...

sed -e "s/<<NN_IO>>/$NN_IO/" $HERE/namelist.skel > $TMPDIR/namelist

# full input file
#ln -sf /summer/resolve/Cuolm_da_Vi/Preprocessed_for_Corr/CdV_Stryde_${YEAR}_${JDAY}_${HH}_preprocessed.h5 in.h5

# only 4 nodes
ln -sf /bettik/lecoinal/tmp/extract_CdV_Stryde_${YEAR}_${JDAY}_${HH}_preprocessed.h5 in.h5

echo "CORRELATION: Correlations:                      $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"

$SHARED_SCRATCH_DIR/$USER/bin/time -f "\t%M Maximum resident set size (KB)"  \
    ./$F90EXE

#mkdir $nEXP
mv c.h5 CdV_Stryde.${YEAR}${JDAY}.${HH}0000.h5

echo "CORRELATION: End job:                           $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"

