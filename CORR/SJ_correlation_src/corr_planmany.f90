!###############################################################################
!#    This file is part of noisecorr_dbf : a Fortran code for computing        #
!#    cross-correlations on the dataset from San Jacinto Fault zone            #
!#    dense array of geophones                                                 #
!#                                                                             #
!#    Copyright (C) 2015 Albanne Lecointre
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################
!
!#  https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Correlations

#ifdef lk_float
#  define MYKIND C_FLOAT
#  define MYKINDCPLX C_FLOAT_COMPLEX
#  define ZERO 0.0
#  define UN 1.0
#else
#  define MYKIND C_DOUBLE
#  define MYKINDCPLX C_DOUBLE_COMPLEX
#  define ZERO 0.d0
#  define UN 1.d0
#endif


PROGRAM CORR_PLANMANY

  USE HDF5 ! HDF5 module

  USE, intrinsic :: iso_c_binding

  IMPLICIT NONE

  include 'fftw3.f03'

  LOGICAL :: l_exist_namelist

  ! Output management

  ! Input parameters
  INTEGER :: nn_samprate   ! Input dataset frequency
  INTEGER :: nn_unitseg    ! length in sec of a unitary segment 1200 sec (24000 points at 20Hz)
  INTEGER :: nn_nbunitseg   ! number of unitary segments in one hour (5) 
  INTEGER :: nn_lag        ! the lag in sec for the correlation
  INTEGER :: nn_nextpow2   ! npad
  INTEGER :: maxlag      ! in nb of samples (5min*60*sampling rate) : 6000

  INTEGER :: npc         ! = 2*maxlag+1 : total nb of points for the output correlation : 12001
  INTEGER :: nph         ! the total number of samples in 20min segment

  CHARACTER(LEN=128) :: cn_filein
  CHARACTER(LEN=9), DIMENSION(:), ALLOCATABLE :: cldname
  CHARACTER(LEN=9) :: cd1,cd2
  CHARACTER(LEN=7), DIMENSION(:), ALLOCATABLE :: clchname
  CHARACTER(LEN=11), DIMENSION(:), ALLOCATABLE :: clstname
  CHARACTER(LEN=20) :: cbuf
  CHARACTER(LEN=2) :: chour,cmin

  ! Manipulation des fichiers et des datasets HDF5
  ! Input
  INTEGER(HID_T) :: infile_id      ! Input File identifier: this HDF5 file contains all the 1H (=5 x 20min) traces
  INTEGER(HID_T) :: dset_id        ! Input and Output Dataset identifiers (released immediately)
  INTEGER(HID_T) :: space_id       ! Input and Output Dataset's dataspace identifier (released immediately)
  INTEGER(HID_T) :: memspace_id    ! Input and Output Dataset's memspace identifier in the memory (released immediately)
  INTEGER(HSIZE_T), DIMENSION(1) :: indims     ! The hyperslab (memory) current dimension for input dataset : 20min at 20Hz = 24000
  INTEGER(HSIZE_T), DIMENSION(1) :: startin    ! The starting point to read 20min input dataset from input file 
  INTEGER(HSIZE_T), DIMENSION(1) :: countin    ! Into 2Ddataset from HDF5 file: nb of points when read 20min input data from input file : (/1,24000/)
  INTEGER(HSIZE_T), DIMENSION(1) :: memcountin ! Into memory: nb of points when read 20min input data from input file : (/24000/)
  INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: memstartin = (/0/) ! Input Dataset hyperslab memory starting point : always (/0/)
  ! Output
  INTEGER(HID_T) :: outfile_id     ! Output File identifier: the HDF5 File containing all the correlations for all the pairs
  INTEGER(HSIZE_T), DIMENSION(2) :: dimsfi ! The total current dimension for output dataset
  INTEGER(HSIZE_T), DIMENSION(2) :: outdims ! The total current dimension for output dataset
  INTEGER(HID_T) :: grpsta_id
  INTEGER(HID_T) :: grpchan_id

  INTEGER(HSIZE_T), DIMENSION(2) :: count  
  INTEGER(HSSIZE_T), DIMENSION(2) :: offset 

  ! Iterators
  INTEGER   :: jglomin       ! iterator on the global min (from 0 to 1440)
  INTEGER   :: js            ! iterator on the 20min unitary segment
  INTEGER   :: jk,jtr1,jtr2  ! iterator on the station traces
  INTEGER   :: it            ! iterator on the time

  INTEGER   :: nn_glomin_beg,nn_glomin_end,nn_glomin_stp

  INTEGER   :: error ! Error flag

  ! Instrumentation du code pour analyse de performance
  INTEGER,DIMENSION(8) :: values
  REAL(C_FLOAT) :: elps           ! in sec

  INTEGER :: obj_type            ! Type of the object
  INTEGER :: nmembers            ! Number of group members
  INTEGER :: nbpair
  INTEGER :: npad                              ! =30000 ! 32768  ! 2^15 (nextpow2(dishape+maxlag=24000+6000))

  ! Buffers for input and output datasets
  INTEGER, DIMENSION(:), ALLOCATABLE :: ntr            ! nph=24000
  INTEGER(KIND=1), DIMENSION(:,:), ALLOCATABLE :: normtr ! one unitary segment (here 20min long), for all traces
  COMPLEX(MYKINDCPLX), pointer :: yout(:,:)
  type(C_PTR) :: pyout
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE :: dcorr    ! Output correlation (when manyplan=F)
  REAL(MYKIND), pointer :: din(:)                    ! final result of IFFT (when manyplan=F)
  COMPLEX(MYKINDCPLX), pointer :: youtpair(:)
  COMPLEX(MYKINDCPLX), DIMENSION(:,:,:), ALLOCATABLE :: yout15 , yout25
  type(C_PTR) :: pyoutpair
  type(C_PTR) :: pdin
  REAL(MYKIND), pointer :: dinpad(:,:)  ! all station traces but only one 20min chunk, padded with zeros before or after
  type(C_PTR) :: pdinpad

  INTEGER(KIND=8) :: plan_forward,plan_backward

  ! Use the advanced interface for FFTW (plan_many)
  INTEGER :: rank
  INTEGER :: howmany
  INTEGER :: istride, idist
  INTEGER :: ostride, odist
  INTEGER, DIMENSION(1) :: n_array, inembed, onembed
 
  ! Use the multithreaded FFTW
  INTEGER :: iret
  INTEGER :: nn_nbthread

  INTEGER :: nn_scaleoffset
  INTEGER(HID_T)  :: dcpl ! Handles
  INTEGER(HSIZE_T), DIMENSION(2)   :: chunk

  INTEGER :: nn_io
  REAL(MYKIND), DIMENSION(:,:,:), ALLOCATABLE :: dcorrdump

  ! Declare NAMELIST
  NAMELIST /namthread/ nn_nbthread
  NAMELIST /namio/ nn_scaleoffset,nn_io 
  NAMELIST /namparam/ cn_filein,nn_samprate,nn_lag,nn_unitseg,nn_nbunitseg,nn_nextpow2
  NAMELIST /namtime/ nn_glomin_beg,nn_glomin_end,nn_glomin_stp

  ! Read namelist
  INQUIRE(file='namelist', exist=l_exist_namelist)
  IF (.NOT. l_exist_namelist) THEN
     PRINT *,'The namelist does not exist'
     STOP
  ENDIF
  OPEN(11,file='namelist')
  PRINT *,'Reading namelist namthread'
    REWIND 11
    READ(11,namthread)
    PRINT *,'nn_nbthread = ',nn_nbthread
  PRINT *,'Reading namelist namio'
    REWIND 11
    READ(11,namio)
    PRINT *,'nn_io = ',nn_io
    PRINT *,'nn_scaleoffset = ',nn_scaleoffset
  PRINT *,'Reading namelist namparam'
    REWIND 11
    READ(11,namparam)
    PRINT *,'cn_filein: ',TRIM(cn_filein)
    PRINT *,'nn_samprate: ',nn_samprate,' Hz'
    PRINT *,'nn_unitseg: ',nn_unitseg,' sec'
    PRINT *,'nn_nbunitseg: ',nn_nbunitseg,' unitary segment(s)'
    PRINT *,'nn_lag: ',nn_lag,' sec'
    PRINT *,'nn_nextpow2: ',nn_nextpow2
  PRINT *,'Reading namelist namtime'
    REWIND 11
    READ(11,namtime)
    PRINT *,'nn_glomin_beg: ',nn_glomin_beg
    PRINT *,'nn_glomin_end: ',nn_glomin_end
    PRINT *,'nn_glomin_stp: ',nn_glomin_stp
  CLOSE(11)

  ! Compute some useful parameters
  maxlag = nn_lag*nn_samprate  ! as a number of samples : 200
  npc = 2*maxlag+1             ! final length of the correlation vector : 401
  nph = nn_unitseg*nn_samprate ! length of a unitary 10min segment : 60000
  IF ( nn_nextpow2 == 0 ) THEN
    npad = nph + maxlag          ! 60200 ( or 65536 = 2^16 (nextpow2(nph+maxlag)) FFTW3.3.4 "Arbitrary-size transforms" )
  ELSE
    npad = nn_nextpow2
  ENDIF
  dimsfi = (/npc,nn_io/)                      ! 401,4
  outdims = (/npc,86400/(nn_unitseg*nn_nbunitseg)/)    ! 86400/600=144  86400/(600*6)=24
  indims = (/nph/)                      ! 60000
  memcountin = (/nph/)                  ! 10min a 100Hz = 60000 pts
  countin = (/nph/)                     ! 60000
  count = (/npc,nn_io/)                     ! 401,1
  chunk = (/npc,nn_io/)

  ! Read one time the entire file to get the total number of traces
  CALL h5open_f(error)
  CALL h5fopen_f(TRIM(cn_filein), H5F_ACC_RDONLY_F, infile_id, error)
  CALL h5gn_members_f(infile_id, "/", nmembers, error)
  nbpair=nmembers*(nmembers+1)/2

  ! In and Out for the FFT forward (advanced interface: manyplan)
#ifdef lk_float
  pdinpad = fftwf_alloc_real(int(npad * nmembers, C_SIZE_T))
#else
  pdinpad = fftw_alloc_real(int(npad * nmembers, C_SIZE_T))
#endif
  call c_f_pointer(pdinpad, dinpad, [npad,nmembers])
#ifdef lk_float
  pyout = fftwf_alloc_complex(int((npad/2+1) * nmembers, C_SIZE_T))
#else
  pyout = fftw_alloc_complex(int((npad/2+1) * nmembers, C_SIZE_T))
#endif
  call c_f_pointer(pyout, yout, [npad/2+1,nmembers])


#ifdef lk_threads
  ! Multithreaded FFTW
  if ( nn_nbthread /= 0 ) then
#ifdef lk_float
    call sfftw_init_threads(iret)
    call sfftw_plan_with_nthreads(nn_nbthread)
#else
    call dfftw_init_threads(iret)
    call dfftw_plan_with_nthreads(nn_nbthread)
#endif
    print *,'iret:',iret
  endif
#endif


  ! Create the 2 plans for FFT forward and FFT backward
  CALL date_and_time(VALUES=values)
  elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)

  rank = 1
  n_array(1) = npad
  howmany = nmembers
  istride = 1 ; ostride = 1
  dinpad(:,:)=ZERO
  yout(:,:)=(ZERO,ZERO)
  inembed(1) = npad ; onembed(1) = npad/2 + 1
  idist = npad ; odist = npad/2 + 1
#ifdef lk_float
  CALL sfftw_plan_many_dft_r2c(plan_forward, rank, n_array, howmany, &
                                 dinpad, inembed, istride, idist, &
                                 yout, onembed, ostride, odist, &
                                 FFTW_ESTIMATE)

  pyoutpair = fftwf_alloc_complex(int((npad/2+1), C_SIZE_T))
  call c_f_pointer(pyoutpair, youtpair, [npad/2+1])
  pdin = fftwf_alloc_real(int(npad, C_SIZE_T))
  call c_f_pointer(pdin, din, [npad])
  youtpair(:)=(ZERO,ZERO)
  CALL sfftw_plan_dft_c2r(plan_backward, rank, n_array, &
                              youtpair, &
                              din, &
                              FFTW_ESTIMATE)
#else
  CALL dfftw_plan_many_dft_r2c(plan_forward, rank, n_array, howmany, &
                                 dinpad, inembed, istride, idist, &
                                 yout, onembed, ostride, odist, &
                                 FFTW_ESTIMATE)

  pyoutpair = fftw_alloc_complex(int((npad/2+1), C_SIZE_T))
  call c_f_pointer(pyoutpair, youtpair, [npad/2+1])
  pdin = fftw_alloc_real(int(npad, C_SIZE_T))
  call c_f_pointer(pdin, din, [npad])
  youtpair(:)=(ZERO,ZERO)
  CALL dfftw_plan_dft_c2r(plan_backward, rank, n_array, &
                              youtpair, &
                              din, &
                              FFTW_ESTIMATE)
#endif

  CALL date_and_time(VALUES=values)
  elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-elps
  print *,'Init plans:',elps,'sec'

  ! Allocate array
  ALLOCATE(ntr(nph))
  ALLOCATE(normtr(nph,nmembers)) ! Fortran order : first dim is contiguous
  ALLOCATE(cldname(nmembers)) 
  ALLOCATE(clchname(nbpair),clstname(nbpair))

  ALLOCATE(yout15(npad/2+1,nmembers,nn_nbunitseg))
  ALLOCATE(yout25(npad/2+1,nmembers,nn_nbunitseg))

  ! Browse the input file to know the station and channel names
  DO jtr1 = 1, nmembers ! Loop on station traces
    CALL h5gget_obj_info_idx_f(infile_id, "/", jtr1-1, cbuf, obj_type, error)
    cldname(jtr1)=TRIM(cbuf)
  ENDDO
  jk=0
  DO jtr1=1,nmembers
    cd1=cldname(jtr1)
    DO jtr2=jtr1,nmembers
      jk=jk+1
      cd2=cldname(jtr2)
      CALL sort_string(cd1(1:5),cd2(1:5),clstname(jk))
      clchname(jk)=cd1(7:9)//'_'//cd2(7:9)
    ENDDO
  ENDDO

  ! Create output file
  CALL h5fcreate_f('c.h5', H5F_ACC_TRUNC_F, outfile_id, error)
  ! Create the 2D dataset npc x 144 : 401 (lag) x 144 (144 10min windows)
  ! Create the groups for sta and chan
  DO jk=1,nbpair
    CALL create_or_open_group(outfile_id, TRIM(clstname(jk)), grpsta_id )
    CALL create_or_open_group(grpsta_id,  TRIM(clchname(jk)), grpchan_id)
    CALL h5gclose_f( grpsta_id, error)
    ! Create the data space for the 2D output dataset
    CALL h5screate_simple_f(2, outdims, space_id, error)
    ! Create the dataset creation property list, and set the chunk size.
    CALL h5pcreate_f(H5P_DATASET_CREATE_F, dcpl, error)
#ifdef lk_scaleoffset
    CALL h5pset_scaleoffset_f(dcpl, 0, nn_scaleoffset, error)
#endif
    CALL h5pset_chunk_f(dcpl, 2, chunk, error)

    ! Create the dataset with default properties
    CALL h5dcreate_f(grpchan_id, 'corr', H5T_NATIVE_REAL, space_id, dset_id, error, dcpl)
    !CALL h5dcreate_f(grpchan_id, 'corr', H5T_NATIVE_DOUBLE, space_id, dset_id, error, dcpl)
    CALL h5pclose_f(dcpl, error)
    CALL h5sclose_f(space_id, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5gclose_f(grpchan_id, error)
  ENDDO

  ALLOCATE(dcorr(npc))
  ALLOCATE(dcorrdump(npc,nn_io,nbpair))

  it=0
  ! Loop on global mins
  DO jglomin=nn_glomin_beg, nn_glomin_end,nn_glomin_stp
      CALL date_and_time(VALUES=values)
      elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)

      write(chour,'(I0.2)') (jglomin-MOD(jglomin,60))/60
      write(cmin,'(I0.2)') MOD(jglomin,60)

      youtpair(:)=(ZERO,ZERO)

      ! Loop on the 5 x 20min segments
      DO js=1, nn_nbunitseg
        ! Read all the input traces for the considered 20min segment (read as int8bit)
        ! require 55MB/hour if 2400 traces int8bit(kind=1)
!        ALLOCATE(normtr(nph,nmembers)) ! Fortran order : first dim is contiguous
        normtr(:,:)=0
        DO jtr1 = 1, nmembers ! Loop on station traces
          CALL h5dopen_f(infile_id, "/"//cldname(jtr1), dset_id, error)
          CALL h5dget_space_f(dset_id, space_id, error)
          startin = (/(jglomin)*60*nn_samprate+(js-1)*nn_unitseg*nn_samprate/)
!print *,'startin,countin',startin,countin,jglomin
          ntr(:)=0
          CALL h5sselect_hyperslab_f (space_id, H5S_SELECT_SET_F, startin, countin, error)
          CALL h5screate_simple_f(1, memcountin, memspace_id, error)
          CALL h5sselect_hyperslab_f(memspace_id, H5S_SELECT_SET_F, memstartin, memcountin, error)
          CALL H5dread_f(dset_id, H5T_NATIVE_INTEGER, ntr, indims, error, memspace_id, space_id)
          CALL h5sclose_f(memspace_id, error)
          normtr(:,jtr1)=ntr(:)
          CALL h5sclose_f(space_id, error)
          CALL h5dclose_f(dset_id, error)
        ENDDO ! End loop on station traces
  
        ! Compute the FFT forward first when padding with zero after, and take the conjugate
        dinpad(nph+1:npad,:)=ZERO
#ifdef lk_float
        DO jtr1=1,nmembers
          dinpad(1:nph,jtr1) = REAL(normtr(:,jtr1))/SQRT(SUM((REAL(normtr(:,jtr1))**2)))
        ENDDO
        CALL sfftw_execute_dft_r2c(plan_forward, dinpad, yout)  
  
        ! Save the output of FFT+1 in memory (2.5GB)   ! 2400traces * 5 segments * 2 = 4800 * 1.14MB files) (if FFTW-1 plan many = F)
        yout15(:,:,js) = conjg(yout(:,:))
#else
        DO jtr1=1,nmembers
          dinpad(1:nph,jtr1) = DBLE(normtr(:,jtr1))/SQRT(SUM((DBLE(normtr(:,jtr1))**2)))
        ENDDO
        CALL dfftw_execute_dft_r2c(plan_forward, dinpad, yout)  
  
        ! Save the output of FFT+1 in memory (2.5GB)   ! 2400traces * 5 segments * 2 = 4800 * 1.14MB files) (if FFTW-1 plan many = F)
        yout15(:,:,js) = dconjg(yout(:,:))
#endif
  
        ! Compute again the FFT forward when padding with zero before
        !dinpad = CSHIFT (dinpad, SHIFT = -maxlag, DIM = 1)
        dinpad(1:maxlag,:)=ZERO
        dinpad(maxlag+nph+1:npad,:)=ZERO
#ifdef lk_float
        DO jtr1=1,nmembers
          dinpad(maxlag+1:maxlag+nph,jtr1) = REAL(normtr(:,jtr1))/SQRT(SUM((REAL(normtr(:,jtr1))**2)))
        ENDDO
!        DEALLOCATE(normtr)
        CALL sfftw_execute_dft_r2c(plan_forward, dinpad, yout)
#else
        DO jtr1=1,nmembers
          dinpad(maxlag+1:maxlag+nph,jtr1) = DBLE(normtr(:,jtr1))/SQRT(SUM((DBLE(normtr(:,jtr1))**2)))
        ENDDO
!        DEALLOCATE(normtr)

        CALL dfftw_execute_dft_r2c(plan_forward, dinpad, yout)
#endif
  
        !    Compute all the multiplication (if FFTW-1 plan many = T)
        ! Save the output into memory (2.5GB)
        yout25(:,:,js) = yout(:,:)
      ENDDO  ! End loop on js
     
      ! Compute the FFTW-1 and then corr and write corr into HDF5 file
      jk=0
      DO jtr1=1,nmembers
        DO jtr2=jtr1,nmembers
          jk=jk+1
          youtpair(:) = (ZERO,ZERO)
          DO js=1,nn_nbunitseg
            youtpair(:) = youtpair(:) + yout15(:,jtr1,js)*yout25(:,jtr2,js) 
          ENDDO
#ifdef lk_float
          call sfftw_execute_dft_c2r(plan_backward,youtpair,din)
#else
          call dfftw_execute_dft_c2r(plan_backward,youtpair,din)
#endif
          dcorr(:) = (UN / nn_nbunitseg) * din(1:2*maxlag+1) / npad

          ! Depending on modulo it, store the current 10min into memory or write the last n x 10min into file
          dcorrdump(:,1+MOD(it,nn_io),jk)=dcorr(:)
          if (MOD(it+1,nn_io)==0) THEN ! on ecrit dans le fichier
            ! Each process (each jk pair) defines dataset in memory and writes it to the hyperslab in the file.
            offset = (/0,it-nn_io+1/)   ! 0,0-143
            CALL h5screate_simple_f(2, dimsfi, memspace_id, error)   ! rank=2, dimsfi=(/npc,nn_io/)=(/401,4/)
            CALL h5dopen_f(outfile_id,"/"//TRIM(clstname(jk))//"/"//TRIM(clchname(jk))//"/corr", dset_id, error)
            ! Select hyperslab in the output file.
            CALL h5dget_space_f(dset_id, space_id, error)
            CALL h5sselect_hyperslab_f (space_id, H5S_SELECT_SET_F, offset, count, error)
            ! Write the dataset independantly
            CALL h5dwrite_f(dset_id, H5T_NATIVE_REAL, REAL(dcorrdump(:,:,jk)), dimsfi, &
                    error, file_space_id = space_id,mem_space_id = memspace_id)
            !CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, dcorrdump(:,:,jk), dimsfi, error, file_space_id = space_id,mem_space_id = memspace_id)
            CALL h5sclose_f(space_id, error)
            CALL h5sclose_f(memspace_id, error)
            CALL h5dclose_f(dset_id, error)
          endif 
        ENDDO
      ENDDO ! End double loop on stations

      CALL date_and_time(VALUES=values)
      elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-elps
      PRINT '("Total elapsed time to compute ",I8," correlations for HH ",A2," MM ",A2," ( "I2" thr) : ",f20.6," s")', &
              nbpair,chour,cmin,nn_nbthread,elps
    it=it+1
  ENDDO ! End loop on global minutes

  ! Close the input daily file
  CALL h5fclose_f(infile_id, error)

  ! Close the 1day output file
  CALL h5fclose_f(outfile_id, error)

#ifdef lk_float
  call fftwf_free(pdinpad)
  call fftwf_free(pyout)
  call fftwf_free(pdin)
  call fftwf_free(pyoutpair)

  CALL sfftw_destroy_plan(plan_forward)
  CALL sfftw_destroy_plan(plan_backward)
#ifdef lk_threads
  if ( nn_nbthread /= 0 ) then
    CALL sfftw_cleanup_threads()
  endif
#endif
#else
  call fftw_free(pdinpad)
  call fftw_free(pyout)
  call fftw_free(pdin)
  call fftw_free(pyoutpair)

  CALL dfftw_destroy_plan(plan_forward)
  CALL dfftw_destroy_plan(plan_backward)
#ifdef lk_threads
  if ( nn_nbthread /= 0 ) then
    CALL dfftw_cleanup_threads()
  endif
#endif
#endif
  CALL h5close_f(error)

END PROGRAM CORR_PLANMANY

SUBROUTINE create_or_open_group(parent_id,gr_name,gr_id)

  USE HDF5 ! HDF5 module

  IMPLICIT NONE

  INTEGER(HID_T), INTENT(in)     :: parent_id
  CHARACTER(LEN=*), INTENT(in)   :: gr_name
  INTEGER(HID_T), INTENT(out)    :: gr_id
  LOGICAL                        :: link_exists
  INTEGER                        :: error

  ! Output file : create group if necessary, or simply open it
  CALL h5lexists_f(parent_id, gr_name, link_exists, error, H5P_DEFAULT_F)
  IF ( link_exists ) THEN
    CALL h5gopen_f(parent_id, gr_name, gr_id, error)
  ELSE
    CALL h5gcreate_f(parent_id, gr_name, gr_id, error)
  ENDIF

END SUBROUTINE create_or_open_group

SUBROUTINE sort_string(str1,str2,strsort)

  IMPLICIT NONE

  CHARACTER(LEN = *), INTENT(in)  :: str1,str2
  CHARACTER(LEN = *), INTENT(out) :: strsort

  IF ( LGT(str1,str2) ) THEN
    strsort=str2//'_'//str1
  ELSE
    strsort=str1//'_'//str2
  ENDIF

END SUBROUTINE sort_string
