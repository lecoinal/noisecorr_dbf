[Initial preprocessing](https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Preprocessing)

 * gapfilling : fill remaning gap with zero
 * bandpass filter : 0.5 - 25Hz
 * decimation factor 5 using spline interpolation : -> 100Hz
 * Float32 encoding and rename output data channel as EPZ (with respect to channel naming SEED convention)

---

```
> source /applis/ciment/guix-start.bash
```
Option 1: We suppose that GUIX profile "preprocess" was created with :
```
> guix install -p $GUIX_USER_PROFILE_DIR/preprocess python@3 python-obspy python-h5py
```
Option 2: We use the [GUIX manifest file manifest_preprocess.scm](./manifest_preprocess.scm)
```
> guix package -m manifest_preprocess.scm -p $GUIX_USER_PROFILE_DIR/preprocess
```

And then for each session:
```
> source /applis/ciment/guix-start.bash
> refresh_guix preprocess
```
