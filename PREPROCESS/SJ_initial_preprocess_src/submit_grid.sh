#!/bin/bash

#  =======================================================================================
#   ***  Shell wrapper to submit read_SanJacinto_2014_skel.py in the CIMENT grid ***
#    
#  =======================================================================================
#   History :  1.0  : 09/2015  : A. Lecointre : Initial version
#                     11/2015  : A. Lecointre : pytables->h5py
#               
#  ---------------------------------------------------------------------------------------

set -e

# only on Luke4 !

source /applis/ciment/guix-start.bash
# Suppose that GUIX profile "preprocess" was created with :
# guix install -p $GUIX_USER_PROFILE_DIR/preprocess python@3 python-obspy python-h5py
# Or with a GUIX manifest file
# guix package -m manifest_preprocess.scm -p $GUIX_USER_PROFILE_DIR/preprocess

refresh_guix preprocess

############################################################################################

# args
if [ $# -ne 3 ]; then
    echo "ERROR: Bad number of parameters"
    echo "USAGE: $0 <input mseed file> <bandpass_freqmin> <bandpass_freqmax>"
    exit 1
fi

INPUTFILE=$1
FB1=$2  # 1.0
FB2=$3  # 12.0

cat $OAR_NODE_FILE

case "$CLUSTER_NAME" in
  luke)
    TMPDIR="$LOCAL_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID"        # /var/tmp
    ;;
esac
#TMPDIR="/bettik/$USER/oar.$OAR_JOB_ID"

## Example of return status that triggers an automatic re-submission
## The special code "66" tells to cigri to re-submit.
## if machine is not luke4 (problem with networkadress='luke4' submission rule)
#if [[ "$HOSTNAME" != "luke4" ]] ; then echo "$HOSTNAME ; only works for luke4 ; cigri re-submit ; exit 66" ; exit 66 ; fi

echo $TMPDIR

mkdir -p $TMPDIR

sed -e "s/<<FB1>>/$FB1/" -e "s/<<FB2>>/$FB2/" read_SanJacinto_2014_skel.py > $TMPDIR/read_SanJacinto_2014.py

cd $TMPDIR

echo "Preprocess                    : Date and Time: $(date +%F" "%T"."%N)"

mkdir -p /scratch_md1200/$USER/PREPROCESS_SANJACINTO/${CIGRI_CAMPAIGN_ID}
#mkdir -p /bettik/$USER/${CIGRI_CAMPAIGN_ID}

python3 read_SanJacinto_2014.py $INPUTFILE
mv $TMPDIR /scratch_md1200/$USER/PREPROCESS_SANJACINTO/${CIGRI_CAMPAIGN_ID}/.
#mv $TMPDIR /bettik/$USER/${CIGRI_CAMPAIGN_ID}/.

echo "End job                       : Date and Time: $(date +%F" "%T"."%N)"
