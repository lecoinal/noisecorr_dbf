#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of noisecorr_dbf : a python code for applying          #
#    the initial pre-processing on the dataset from San Jacinto Fault zone    #
#    dense array of geophones                                                 #
#                                                                             #
#    Copyright (C) 2015 Albanne Lecointre
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################

#  =======================================================================================
#                       ***  Python script read_SanJacinto_2014_skel.py  ***
#   This is the main Python script to apply the initial preprocessing on raw 
#   San Jacinto MSEED data
#    
#  =======================================================================================
#   History :  1.0  : 09/2015  : A. Lecointre : Initial Python version
#                     11/2015  : A. Lecointre : pytables->h5py
#	        
#  ---------------------------------------------------------------------------------------
#   methodology   : gapfilling : fill remaning gap with zero
#                 : bandpass filter : 0.5 - 25Hz
#                 : decimation factor 5 using spline interpolation : from 500 to 100Hz
#                 : Float32 encoding and rename output data channel as EPZ (with respect to 
#                   channel naming SEED convention)
#                 : output writing as : 1 dataset au format miniseed, 1 dataset au format 
#                   HDF5 (pour test)
#
#  https://ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Preprocessing
#  https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Preprocessing
#
#  This sample script must be updated by wrapper submit_script.sh and TAGS <<CID>> and 
#  <<OID>> must be updated by current Cigri Campaign Id and Oar Job Id
#  ---------------------------------------------------------------------------------------

from obspy.core import read,UTCDateTime,Trace
import numpy as np
import os
import sys
import h5py
from scipy import interpolate

lnplt=0
#lnplt=1

#if lnplt:
#	from scipy import fft, arange
#	import matplotlib.pyplot as plt
#	def plotSpectrum(y,Fs):
#	        """
#	        Plots a Single-Sided Amplitude Spectrum of y(t)
#	        """
#	        n = len(y)
#	        k = arange(n)
#	        T = n/Fs
#	        frq = k/T
#	        frq = frq[range(n/2)]
#	        Y = fft(y)/n
#	        Y = Y[range(n/2)]
#	        plt.plot(frq,abs(Y),'r')
#	        plt.xlabel('Freq (Hz)')
#	        plt.ylabel('|Y(freq)|')
#	def mkPlt(z,sr,pltname):
#		plt.clf()
#		plt.subplot(2,1,1)
#		plt.plot(z)
#		plt.xlabel('Time')
#		plt.ylabel('Amplitude')
#		plt.subplot(2,1,2)
#		plotSpectrum(z,sr)
#		#plt.savefig('/scratch/lecoinal/SAN_JACINTO/'+pltname, bbox_inches=0)
#		plt.savefig('./'+pltname, bbox_inches=0)

def readSJ(df):
	
	# Read
	
	s=read(df)
	
	# Gap filling : fill with zero
	
	net = s[0].stats.network
	sta = s[0].stats.station
	loc = s[0].stats.location
	channel = s[0].stats.channel
	year = s[0].stats.starttime.year
	jd = s[0].stats.starttime.julday
	
	datatype = s[0].data.dtype

#	if lnplt:
#		if ( len(s)>1 ):
#			splt=s.copy()
#			splt.merge(method=0,fill_value=None)
#			mkPlt(splt[0].data,splt[0].stats.sampling_rate, 'p0.'+net+'.'+sta+'.'+loc+'.'+channel+'.D.'+"{:04d}".format(year)+'.'+"{:03d}".format(jd)+'.png')
#		else:
#			mkPlt(s[0].data,s[0].stats.sampling_rate, 'p0.'+net+'.'+sta+'.'+loc+'.'+channel+'.D.'+"{:04d}".format(year)+'.'+"{:03d}".format(jd)+'.png')
	
	meta = {'station': sta, 'location': loc, 'network': net, 'channel': channel}
	
	stend = s[-1].stats.endtime
	
	trb = Trace(data=np.zeros(1,dtype=datatype),header=meta)
	trb.stats.delta = s[0].stats.delta
	trb.stats.starttime = UTCDateTime(str(year)+"{:03d}".format(jd)+"T000000.000000Z")
	if s[0].stats.starttime != trb.stats.starttime:
		s.append(trb)
	
	tre = Trace(data=np.zeros(1,dtype=datatype),header=meta)
	tre.stats.delta = s[0].stats.delta
	tre.stats.starttime = UTCDateTime(str(year)+"{:03d}".format(jd+1))-s[0].stats.delta
	if stend != tre.stats.endtime:
		s.append(tre)
	
	s.merge(method=0, fill_value=0)
	
	# Bandpass filter : 0.5 25 Hz

#        if lnplt:
#                mkPlt(s[0].data,s[0].stats.sampling_rate, 'p1.'+net+'.'+sta+'.'+loc+'.'+channel+'.D.'+"{:04d}".format(year)+'.'+"{:03d}".format(jd)+'.png')
	
	fb1=<<FB1>> # 0.5
	fb2=<<FB2>> # 25.0
	s.filter("bandpass", freqmin=fb1, freqmax=fb2, corners=4, zerophase=True)
	
#	if lnplt:
#		mkPlt(s[0].data,s[0].stats.sampling_rate,'p2.'+net+'.'+sta+'.'+loc+'.'+channel+'.D.'+"{:04d}".format(year)+'.'+"{:03d}".format(jd)+'.png')
	
	# Decimation factor 5 using spline interpolation, update the channel name
	
	tck = interpolate.splrep(np.arange(0, s[0].stats.npts, 1), s[0].data, s=0)
	s[0].data = interpolate.splev(np.arange(0, s[0].stats.npts , 5), tck, der=0)
	s[0].stats.sampling_rate=s[0].stats.sampling_rate/5
	channel = 'EPZ'   # extremely short period = 100Hz and corner period of the geophone = 0.1sec
	s[0].stats.channel = channel
	
#	if lnplt:
#		mkPlt(s[0].data,s[0].stats.sampling_rate,'p3.'+net+'.'+sta+'.'+loc+'.'+channel+'.D.'+"{:04d}".format(year)+'.'+"{:03d}".format(jd)+'.png')
	
	# Write outputs
	
	#SDSdir = '/scratch_md1200/lecointre/PREPROCESS_SANJACINTO/<<CID>>/MSEED/'+"{:04d}".format(year)+'/'+net+'/'+sta+'/'+channel+'.D/'
	SDSdir = '.'
	SDSfile = net+'.'+sta+'.'+loc+'.'+channel+'.D.'+"{:04d}".format(year)+'.'+"{:03d}".format(jd)
	
	try:
	    os.stat(SDSdir)
	except:
	    os.makedirs(SDSdir)
	
	s[0].data=s[0].data.astype('float32')
	
#	s.write(SDSdir+'/'+SDSfile, format='MSEED' , reclen=4096 , encoding='FLOAT32' )
	
	h5fo = h5py.File(SDSdir+'/'+net+'_'+"{:04d}".format(year)+'_'+"{:03d}".format(jd)+'.h5','a')
	h5fo.create_dataset(SDSfile,shape=(s[0].stats.npts,), dtype='float32',data=s[0].data[:],compression='gzip', compression_opts=1, fletcher32='True')
	h5fo.close()

if __name__ == '__main__':
	import sys,time
	t=time.time()
	readSJ(sys.argv[1])
	print sys.argv[1],time.time()-t," sec"
