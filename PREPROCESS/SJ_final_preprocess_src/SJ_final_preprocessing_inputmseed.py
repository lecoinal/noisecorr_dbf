#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of noisecorr_dbf : a python code for applying          #
#    the final pre-processing on the dataset from San Jacinto Fault zone      #
#    dense array of geophones                                                 #
#                                                                             #
#    Copyright (C) 2015 Albanne Lecointre
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################

#  =======================================================================================
#                       ***  Python script SJ_final_preprocessing_inputmseed.py  ***
#   This is the main Python script to compute the final preprocessing on gap-filled and 
#   decimated San Jacinto MSEED data
#   This script reads input as MSEED files
#    
#  =======================================================================================
#   History :  1.0  : 10/2015  : A. Lecointre : Initial Python version
#               
#  ---------------------------------------------------------------------------------------
#
#  ---------------------------------------------------------------------------------------
#   methodology   : whitening 6-12Hz
#                 : 1-Bit
#
#  https://ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Final_preprocessing
#  https://ciment-grid.univ-grenoble-alpes.fr/legacy-wiki/ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto#Final_preprocessing
#
#  ---------------------------------------------------------------------------------------

import numpy as np
import time
import sys
import h5py
from obspy.core import read
import glob,os

t0=time.time()

year=int(sys.argv[1])
jd=int(sys.argv[2])

option_white=int(sys.argv[3]) # 1 # 1: whitening / other: bandpass
option_sig=int(sys.argv[4])   # 1 # 1: 1Bit / 2: rms clip (4xrms) / other: do nothin

frequences_corr=np.array([float(sys.argv[5]),float(sys.argv[6])],np.float64)  # 6 12 : frequence for whitening or bandpass
deltaf=float(sys.argv[7])       # 0.2 : for whitening

Fsnew=int(sys.argv[8])        # 100 : current sampling frequency

##############################################################
# code for Final preprocessing debore correlation

ltf=86400*Fsnew

idir='./'
odir='./'

freq=np.arange(0,Fsnew,np.float64(Fsnew)/ltf)

J=np.where((freq > frequences_corr[0]) & (freq < frequences_corr[1]))
Jdebut=np.where((freq > frequences_corr[0]) & (freq<(frequences_corr[0]+deltaf)))
Jfin=np.where((freq>(frequences_corr[1]-deltaf)) & (freq<frequences_corr[1]))

# Read from MSEED dataset and write into HDF5 dataset
h5fo = h5py.File(odir+'/SG_'+"{:04d}".format(year)+'_'+"{:03}".format(jd)+'.trace.int8.h5','w')

print "Init time: ",time.time()-t0,' sec'

# Loop over the stations for the final preprocessing
inode=0
for df in glob.glob(idir+'/MSEED/'+"{:04d}".format(year)+'/SG/*/EPZ.D/SG.*..EPZ.D.'+"{:04d}".format(year)+'.'+"{:03}".format(jd)):
	t0=time.time()

	inode+=1

	st=np.empty((ltf,), dtype=float)         # 8640000,nbtrace  -> 8640000
	st_preproc=np.zeros_like(st)

	s=read(df)

	sta = s[0].stats.station
	channel = s[0].stats.channel
	st[:] = s[0].data

	if option_white==1:
		import scipy.fftpack
		st_preproc=np.zeros((ltf,),np.complex128)
		fftst=scipy.fftpack.fft(st[:])
		st_preproc[J]=fftst[J]/abs(fftst[J])
		if np.size(Jdebut)>1:
			st_preproc[Jdebut]=st_preproc[Jdebut]*(np.sin(np.pi/2*np.arange(0,np.size(Jdebut))/(np.size(Jdebut)-1))**2)
		if np.size(Jfin)>1:
			st_preproc[Jfin]=st_preproc[Jfin]*(np.cos(np.pi/2*np.arange(0,np.size(Jfin))/(np.size(Jfin)-1))**2)
		st_preproc=2*scipy.fftpack.ifft(st_preproc).real
	else:
		from scipy.signal import filtfilt, butter
		# Butterworth digital filter design
		BB, AA = butter(3,frequences_corr/Fsnew*2,btype='bandpass')
		st_preproc = filtfilt(BB, AA, st[:])

	if option_sig==1:
		st_preproc=np.sign(st_preproc)
		h5fo.create_dataset(sta+'_'+channel,shape=(ltf,), dtype='i1',data=st_preproc[:].astype('int8'),compression='gzip', compression_opts=1, fletcher32='True')
	elif option_sig==2:
                rms4=4*np.std(st_preproc)
                np.clip(st_preproc, -rms4, rms4, out=st_preproc)
		h5fo.create_dataset(sta+'_'+channel,shape=(ltf,), dtype='float32',data=st_preproc[:].astype('float32'),compression='gzip', compression_opts=1, fletcher32='True')

	print inode,os.path.basename(df),time.time()-t0," sec"

h5fo.close()
