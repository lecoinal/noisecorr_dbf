#!/bin/bash

#  =======================================================================================
#   ***  Shell wrapper to submit SJ_final_preprocessing.py in the CIMENT grid ***
#    
#  =======================================================================================
#   History :  1.0  : 09/2015  : A. Lecointre : Initial version
#                     10/2015  : A. Lecointre : pytables -> h5py
#               
#  ---------------------------------------------------------------------------------------

set -e

source /applis/ciment/guix-start.bash
# Suppose that GUIX profile "preprocess" was created with :
# guix install -p $GUIX_USER_PROFILE_DIR/preprocess python@3 python-obspy python-h5py
# Or with a GUIX manifest file
# guix package -m manifest_preprocess.scm -p $GUIX_USER_PROFILE_DIR/preprocess

refresh_guix preprocess

############################################################################################

EXP=$1    # # the first parameter for cigri MUST be unique
YEAR=$3   # 2014
JDAY=$2   # 135 
PTIM=$4   # 144
LWHITE=$5 # 1
LSIG=$6   # 1
FMIN=$7      # 6
FMAX=$8      # 12
DF=$9        # 0.2
FREQ=${10}   # 100

PYEXE=SJ_final_preprocessing.py
#PYEXE=SJ_final_preprocessing_inputmseed.py

cat $OAR_NODE_FILE

case "$CLUSTER_NAME" in
  luke)
    #TMPDIR="${LOCAL_SCRATCH_DIR}/$USER/oar.SJcorr.$OAR_JOB_ID";  # /var/tmp : 155GB
    #TMPDIR="${SHARED_SCRATCH_DIR}/$USER/oar.SJcorr.$OAR_JOB_ID"; #/nfs_scratch : 40TB
    TMPDIR="/scratch_md1200/$USER/oar.SJcorr.$OAR_JOB_ID";        # 40TB
    ;;
esac

mkdir -p $TMPDIR
cp $PYEXE $TMPDIR

cd $TMPDIR

echo "Link inputs"

ln -sf /scratch_md1200/lecointre/SAN_JACINTO/PREPROCESS_BP_0.5_25_DEC100Hz/HDF5/SG_${YEAR}_${JDAY}.h5 .
# Or if we are using the SJ_final_preprocessing_inputmseed.py python script with MSEED input reading as a standard SDS (Seismic Data Structure) of MSEED files
# ln -sf /scratch_md1200/lecointre/SAN_JACINTO/PREPROCESS_BP_0.5_25_DEC100Hz/MSEED .

echo "Preprocess:                        $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"

$SHARED_SCRATCH_DIR/$USER/bin/time -f "\t%M Maximum resident set size (KB)"  \
    python3 $PYEXE $YEAR $JDAY $PTIM $LWHITE $LSIG $FMIN $FMAX $DF $FREQ

echo "Move outputs:                      $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"

mkdir -p /scratch_md1200/$USER/SAN_JACINTO/FINAL_PREPROCESS_EXP${EXP}
mv SG_${YEAR}_${JDAY}.trace.int8.h5 /scratch_md1200/$USER/SAN_JACINTO/FINAL_PREPROCESS_EXP${EXP}/.

echo "End job:                           $YEAR $JDAY      Date and Time: $(date +%F" "%T"."%N)"
